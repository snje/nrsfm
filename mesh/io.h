#ifndef __IO_H__
#define __IO_H__

#include <string>
#include <vector>
#include <pcl/PolygonMesh.h>


int read_trace_csv(
    std::string path, std::vector<float> * output_array, int * frames,
    int * points
);

int save_trace_csv(
    std::string path, std::vector<float> & output_array, int frames, int points
);

int save_triangle_mesh(std::string path, std::vector<pcl::Vertices> triangles);

#endif // __IO_H__
