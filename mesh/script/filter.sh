#!/bin/bash

input_file=$1
output_file=$2

bindir=$(dirname $0)/../build
filter_prog=$bindir/filter

tmp1=/tmp/$RANDOM.pcd

$filter_prog $input_file $output_file --threshold 1.0
$filter_prog $output_file $output_file --threshold 3.0 --percent 0.3 --size 2

rm -f $tmp1
