#!/usr/bin/env python
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib.tri import Triangulation
from scipy.spatial import ConvexHull
from scipy.spatial import Delaunay
import numpy as np
import plyfile
import sys

if len(sys.argv) < 2:
    print "Please provide trace path"
    sys.exit(1)

W = np.loadtxt(sys.argv[1])
triangles = np.loadtxt(sys.argv[2]).T.astype("uint32")

X = W[-3:, :].T

xmin = np.min(W[0::3, :])
xmax = np.max(W[0::3, :])
ymin = np.min(W[1::3, :])
ymax = np.max(W[1::3, :])
zmin = np.min(W[2::3, :])
zmax = np.max(W[2::3, :])
size = max(xmax - xmin, ymax - ymin, zmax - zmin)

x, y, z = X.T
# cvx.simplices contains an (nfacets, 3) array specifying the indices of
# the vertices for each simplical facet
tri = Triangulation(x, y, triangles=np.array(triangles))

fig = plt.figure(dpi = 180)
ax = fig.gca(projection='3d')
ax.set_xlim3d(xmin, xmin + size)
ax.set_ylim3d(ymin, ymin + size)
ax.set_zlim3d(zmin, zmin + size)
ax.hold(True)
ax.plot_trisurf(tri, z)
#ax.plot_wireframe(x, y, z, color='r')
#ax.scatter(x, y, z, color='r')

plt.draw()
plt.show()
