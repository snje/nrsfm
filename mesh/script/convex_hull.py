#!/usr/bin/env python
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib.tri import Triangulation
from scipy.spatial import ConvexHull
from scipy.spatial import Delaunay
import numpy as np
import plyfile
import sys

if len(sys.argv) < 3:
    print "Please provide trace path and output path"
    sys.exit(1)

W = np.loadtxt(sys.argv[1])
X = W[-3:, :].T

X -= np.mean(X, axis = 0)

# compute the convex hull of the points
cvx = ConvexHull(X)

print cvx

x, y, z = X.T

print cvx.simplices.shape

if False:
    # cvx.simplices contains an (nfacets, 3) array specifying the indices of
    # the vertices for each simplical facet
    tri = Triangulation(x, y, triangles= cvx.simplices)

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.hold(True)
    ax.plot_trisurf(tri, z)
    #ax.plot_wireframe(x, y, z, color='r')
    ax.scatter(x, y, z, color='r')

    plt.draw()
    plt.show()

np.savetxt(sys.argv[2], cvx.simplices.T)
