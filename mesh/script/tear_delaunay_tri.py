#!/usr/bin/env python
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib.tri import Triangulation
from scipy.spatial import ConvexHull
from scipy.spatial import Delaunay
import numpy as np
import plyfile
import sys

if len(sys.argv) < 2:
    print "Please provide trace path and output path"
    sys.exit(1)

W = np.loadtxt(sys.argv[1])
x, y, z = W[-3, :], W[-2, :], W[-1, :]

left_indices = x < 20
right_indices = ~left_indices

def create_mesh(indices):
    points = W[-3:-1, indices].T
    tri = Delaunay(points)
    print indices.shape, tri.simplices.shape
    #plt.triplot(points[:,0], points[:,1], tri.simplices.copy())
    #plt.plot(points[:,0], points[:,1], 'o')
    indices = np.where(indices)
    to_global_i = np.vectorize(lambda i: indices[0][i])
    return to_global_i(tri.simplices)

left_tri = create_mesh(left_indices)
right_tri = create_mesh(right_indices)
np.savetxt(sys.argv[2], np.vstack([left_tri, right_tri]).T)
#plt.triplot(x, y, np.vstack([left_tri, right_tri]))
#plt.plot(x, y, 'o')
#plt.show()
