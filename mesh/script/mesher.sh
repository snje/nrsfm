#!/bin/bash

input_file=$1
output_file=$2

bindir=$(dirname $0)/../build
filter_prog=$bindir/filter
mesh_prog=$bindir/scan_mesh

tmp1=/tmp/$RANDOM.pcd
tmp2=/tmp/$RANDOM.pcd

$filter_prog $input_file $tmp1 --threshold 1.0
$filter_prog $tmp1 $tmp2 --threshold 3.0 --percent 0.3 --size 2
$mesh_prog $tmp2 $output_file

rm -f $tmp1
rm -f $tmp2
