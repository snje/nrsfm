#!/usr/bin/env python
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib.tri import Triangulation
from scipy.spatial import ConvexHull
from scipy.spatial import Delaunay
import numpy as np
import plyfile
import sys

if len(sys.argv) < 2:
    print "Please provide trace path"
    sys.exit(1)

W = np.loadtxt(sys.argv[1])
f = 3
points = W[f*3:f*3 + 2, :].T

tri = Delaunay(points)
#plt.triplot(points[:,0], points[:,1], tri.simplices.copy())
#plt.plot(points[:,0], points[:,1], 'o')
#plt.show()
np.savetxt(sys.argv[2], tri.simplices.T)
