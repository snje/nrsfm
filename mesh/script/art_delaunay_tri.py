#!/usr/bin/env python
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib.tri import Triangulation
from scipy.spatial import ConvexHull
from scipy.spatial import Delaunay
import numpy as np
import plyfile
import sys

if len(sys.argv) < 2:
    print "Please provide trace path and output path"
    sys.exit(1)

W = np.loadtxt(sys.argv[1])
frame = 43
x, y, z = W[frame * 3, :], W[frame * 3 + 1, :], W[frame * 3 + 2, :]

left = y < 35
middle = np.bitwise_and(y >= 35, y < 250)
right = y >= 250

print np.where(left)
print np.where(middle)
print np.where(right)

def create_mesh(indices):
    points = W[frame * 3:frame * 3 + 2, indices].T
    tri = Delaunay(points)
    print indices.shape, tri.simplices.shape
    #plt.triplot(points[:,0], points[:,1], tri.simplices.copy())
    #plt.plot(points[:,0], points[:,1], 'o')
    indices = np.where(indices)
    to_global_i = np.vectorize(lambda i: indices[0][i])
    return to_global_i(tri.simplices)

left_tri = create_mesh(left)
middle_tri = create_mesh(middle)
right_tri = create_mesh(right)

plt.triplot(x, y, np.vstack([left_tri, middle_tri, right_tri]))
plt.plot(x, y, 'o')
plt.axis('equal')
plt.show()

np.savetxt(sys.argv[2], np.vstack([left_tri, middle_tri, right_tri]).T)
