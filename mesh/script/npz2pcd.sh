#!/bin/bash

root_dir=$(dirname $0)

in_dir=$1
out_dir=$2

convert_proc=$root_dir/../../im3d2pcd.py #/repositories/nrsfm/im3d2pcd.py
filter_proc=$root_dir/filter.sh #/repositories/nrsfm/mesh/script/filter.sh
normal_prog=$root_dir/../build/normal

do_move() {
  path=$1
  fname_ext=$(basename $path)
  fname="${fname_ext%.*}"
  out_path=$out_dir/$fname.pcd
  echo $path
  echo $out_path
  echo $convert_proc
  $convert_proc $path $out_path
  $filter_proc $out_path $out_path
  $normal_prog $out_path $out_path
}

mkdir -p $out_dir

export cmd
export out_dir
export convert_proc
export filter_proc
export normal_prog
export -f do_move

in_files=$(ls $in_dir/*.npz)
echo $in_files | xargs --max-procs=3 -n 1 bash -c 'do_move "$@"' _
