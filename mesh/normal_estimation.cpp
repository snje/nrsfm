#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
//#include <pcl/io/obj_io.h>
//#include <pcl/io/ply_io.h>
#include <pcl/features/normal_3d.h>
#include <pcl/console/time.h>
#include <pcl/features/integral_image_normal.h>
#include <pcl/surface/organized_fast_mesh.h>
#include <pcl/PCLHeader.h>

#include <algorithm>
#include <vector>
#include <map>
#include <string>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <getopt.h>


#include <math.h>

typedef pcl::PointXYZRGB PointType;
typedef pcl::Normal NormalType;
typedef pcl::PointXYZRGBNormal FullType;

int main(int argc, char ** argv) {
    int c;
    int digit_optind = 0;
    std::map<std::string, std::string> options;
    while (1) {
        int this_option_optind = optind ? optind : 1;
        int option_index = 0;
        static struct option long_options[] = {
            {"x", required_argument, 0,  0 },
            {"y", required_argument, 0,  0 },
            {"z",  required_argument, 0, 0},
            {"depth_factor",    required_argument, 0,  0 },
            {"normal_smoothing",    required_argument, 0,  0 },
            {"size",    required_argument, 0,  0 },
            {"depth_smoothing",    no_argument, 0,  0 },
            {0,         0,                 0,  0 }
        };
        c = getopt_long(argc, argv, "abc:d:012",
        long_options, &option_index);
        if (c == -1) break;

        switch (c) {
            case 0:
            {
                std::string key(long_options[option_index].name);
                std::string val = optarg ? std::string(optarg): std::string();
                options[key] = val;
                break;
            }
            case '?':
                break;
            default:
                printf("?? getopt returned character code 0%o ??\n", c);
        }
    }
    std::vector<std::string> paths;

    for (int i = optind; i < argc; i++) {
        paths.push_back(std::string(argv[i]));
    }

    if (paths.size() < 2) {
        printf("Provide input and output paths\n");
        return -1;
    }

    bool depth_smoothing = false;
    float depth_factor = 50.5f;
    float normal_smoothing = 50.5f;
    int rect_size = 203;
    float x  = 0;//1;
    float y = 0;//0.55;
    float z = 0;//-0.05;

    if (options.find("x") != options.end()) x = atof(options["x"].c_str());
    if (options.find("y") != options.end()) y = atof(options["y"].c_str());
    if (options.find("z") != options.end()) z = atof(options["z"].c_str());
    if (options.find("size") != options.end()) {
        rect_size = atof(options["size"].c_str());
    }
    if (options.find("normal_smoothing") != options.end()) {
        normal_smoothing = atof(options["normal_smoothing"].c_str());
    }
    if (options.find("depth_smoothing") != options.end()) {
        depth_smoothing = true;
    }
    if (options.find("depth_factor") != options.end()) {
        depth_factor = atof(options["depth_factor"].c_str());
    }



  pcl::PointCloud<PointType>::Ptr cloud (new pcl::PointCloud<PointType>);
  if ( pcl::io::loadPCDFile <PointType> (paths[0], *cloud) == -1) {
    std::cout << "Cloud reading failed." << std::endl;
    return (-1);
  }

  pcl::PointCloud<NormalType>::Ptr normals (new pcl::PointCloud<NormalType>);
  // Estimate normals and curvature
  pcl::IntegralImageNormalEstimation<PointType, NormalType> ne;
  ne.setNormalEstimationMethod (ne.AVERAGE_3D_GRADIENT);
  ne.setBorderPolicy(ne.BORDER_POLICY_IGNORE);
  ne.setDepthDependentSmoothing(depth_smoothing);
  ne.setMaxDepthChangeFactor(depth_factor);
  ne.setNormalSmoothingSize(normal_smoothing);
  ne.setRectSize(rect_size, rect_size);
  //ne.setViewPoint(1, 0.55, -0.05);
  ne.setViewPoint(x, y, z);
  ne.setInputCloud(cloud);
  ne.compute(*normals);


  pcl::PointCloud<FullType>::Ptr full_cloud(new pcl::PointCloud<FullType>());
  full_cloud->resize(cloud->size());

  for (int i = 0; i < full_cloud->size(); i++) {
    PointType xyzrgb = (*cloud)[i];
    NormalType n = (*normals)[i];

    FullType p;
    p.x = xyzrgb.x;
    p.y = xyzrgb.y;
    p.z = xyzrgb.z;
    p.rgb = xyzrgb.rgb;
    p.normal_x = n.normal_x;
    p.normal_y = n.normal_y;
    p.normal_z = n.normal_z;
    p.curvature = n.curvature;

    (*full_cloud)[i] = p;
  }

  full_cloud->width = cloud->width;
  full_cloud->height = cloud->height;

  pcl::io::savePCDFileBinaryCompressed(paths[1], *full_cloud);

  return 0;
}
