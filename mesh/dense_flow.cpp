#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <stdio.h>

#include <map>
#include <string>
#include <future>
#include <algorithm>
#include <vector>
#include <numeric>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/video/video.hpp>

#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/io/vtk_lib_io.h>
#include <pcl/conversions.h>

#include "io.h"

typedef pcl::PointXYZRGBNormal PointType;
typedef pcl::PointCloud<PointType> CloudType;
typedef std::pair<cv::Mat, CloudType::Ptr> LoadPair;

LoadPair load_pcd_file(std::string path) {
    cv::Mat im;
    pcl::PointCloud<PointType>::Ptr cloud (new pcl::PointCloud<PointType>);
    if ( pcl::io::loadPCDFile <PointType> (path, *cloud) == -1) {
        return LoadPair(im, cloud);
    }
    int width = cloud->width;
    int height = cloud->height;

    im = cv::Mat(height, width, CV_8UC3);
    for (int h = 0; h < height; h++) {
        for (int w = 0; w < width; w++) {
            int index = w + h * width;
            PointType p = (*cloud)[index];
            uint32_t rgb = *reinterpret_cast<int*>(&p.rgb);
            int cindex = index * 3;
            im.data[cindex++] = (rgb >> 16) & 0x0000ff;
            im.data[cindex++] = (rgb >> 8)  & 0x0000ff;
            im.data[cindex++] = (rgb)       & 0x0000ff;
        }
    }
    //cv::cvtColor(im, im, CV_RGB2GRAY);
    return LoadPair(im, cloud);
}

bool valid_point(PointType p) {
    return !isnan(p.x) && !isnan(p.normal_x);
}

PointType search_point(CloudType::Ptr cloud, int x, int y) {
    int width = cloud->width;
    int height = cloud->height;

    PointType p_i = (*cloud)[x + y * width];
    if (valid_point(p_i)) return p_i;

    std::vector<PointType> inter_point;
    std::vector<int> inter_x, inter_y;

    for (int _x = x + 1; _x < width; _x++) {
        PointType p = (*cloud)[_x + y * width];
        if (valid_point(p)) {
            inter_point.push_back(p);
            inter_x.push_back(_x);
            inter_y.push_back(y);
            break;
        }
    }
    for (int _x = x - 1; _x >= 0; _x--) {
        PointType p = (*cloud)[_x + y * width];
        if (valid_point(p)) {
            inter_point.push_back(p);
            inter_x.push_back(_x);
            inter_y.push_back(y);
            break;
        }
    }
    for (int _y = y - 1; _y >= 0; _y--) {
        PointType p = (*cloud)[x + _y * width];
        if (valid_point(p)) {
            inter_point.push_back(p);
            inter_x.push_back(x);
            inter_y.push_back(_y);
            break;
        }
    }
    for (int _y = y + 1; _y < height; _y++) {
        PointType p = (*cloud)[x + _y * width];
        if (valid_point(p)) {
            inter_point.push_back(p);
            inter_x.push_back(x);
            inter_y.push_back(_y);
            break;
        }
    }

    PointType out_p;
    std::vector<float> weight;
    float normalizer = 0;
    for (int i = 0; i < inter_x.size(); i++) {
        float dx = x - inter_x[i];
        float dy = y - inter_y[i];
        float w = 1.0 / sqrt(dx * dx + dy * dy);
        normalizer += w;
        weight.push_back(w);
    }

    for (int i = 0; i < inter_point.size(); i++) {
        PointType p = inter_point[i];
        float w = weight[i] / normalizer;
        out_p.x += p.x * w;
        out_p.y += p.y * w;
        out_p.z += p.z * w;

        out_p.normal_x += p.normal_x * w;
        out_p.normal_y += p.normal_y * w;
        out_p.normal_z += p.normal_z * w;
    }
    /*
    float max = 0;
    for (int i = 0; i < inter_point.size(); i++) {
        if (weight[i] > max) {
            max = weight[i];
            out_p = inter_point[i];
        }
    }
    printf(
        "normalizer = %f %f %f %f %i\n", normalizer, out_p.normal_x,
        out_p.normal_y, out_p.normal_z, inter_point.size()
    );
    */

    float l = sqrt(
        out_p.normal_x * out_p.normal_x + out_p.normal_y * out_p.normal_y
        + out_p.normal_z * out_p.normal_z
    );
    if (l < 1e-10) {
        out_p.normal_x = 0;
        out_p.normal_y = 0;
        out_p.normal_z = -1;
    } else {
        out_p.normal_x /= l;
        out_p.normal_y /= l;
        out_p.normal_z /= l;
    }

    return out_p;
}

cv::Mat illustrate_flow(cv::Mat flow) {
    std::vector<cv::Mat> xy;
    cv::split(flow, xy);
    cv::Mat mag, angle, hsv;
    cv::cartToPolar(xy[0], xy[1], mag, angle, true);
    angle.convertTo(angle, CV_8UC1, 0.5);
    std::vector<cv::Mat> hsv_vec = {
        angle, 255 * cv::Mat::ones(angle.rows, angle.cols, CV_8UC1),
        255 * cv::Mat::ones(angle.rows, angle.cols, CV_8UC1)
    };
    cv::merge(hsv_vec, hsv);
    cvtColor(hsv, hsv, CV_HSV2BGR);
    return hsv;
}

// COCiter == Container of Containers Iterator
// Oiter == Output Iterator
template <class COCiter, class Oiter>
void flatten (COCiter start, COCiter end, Oiter dest) {
    while (start != end) {
        dest = std::copy(start->begin(), start->end(), dest);
        ++start;
    }
}

int main(int argc, char ** argv) {
    std::vector<cv::Mat> test_vec = {
        cv::Mat::ones(255, 255, CV_32F), cv::Mat::ones(255, 255, CV_32F)
    };
    cv::Mat test;
    cv::merge(test_vec, test);
    cv::Mat rgb_flow = illustrate_flow(test);
    cv::imshow("flow", rgb_flow);
    while (cv::waitKey(16) != 27) {}
    return 0;


    int c;
    int digit_optind = 0;
    std::map<std::string, std::string> options;
    while (1) {
        int this_option_optind = optind ? optind : 1;
        int option_index = 0;
        static struct option long_options[] = {
            {"trace", required_argument, 0,  0 },
            {"normal", required_argument, 0,  0 },
            {"mask",  required_argument, 0, 0},
            {"debug",    required_argument, 0,  0 },
            {"verbose",    no_argument, 0,  0 },
            {0,         0,                 0,  0 }
        };
        c = getopt_long(argc, argv, "abc:d:012",
        long_options, &option_index);
        if (c == -1) break;

        switch (c) {
            case 0:
            {
                std::string key(long_options[option_index].name);
                std::string val = optarg ? std::string(optarg): std::string();
                options[key] = val;
                break;
            }
            case '?':
                break;
            default:
                printf("?? getopt returned character code 0%o ??\n", c);
        }
    }
    std::vector<std::string> paths;

    for (int i = optind; i < argc; i++) {
        paths.push_back(std::string(argv[i]));
    }

    if (paths.size() == 0) {
        printf("Provide paths to input PCD files\n");
        return -1;
    }

    std::string output_path = "./trace3d.txt";
    std::string mask_path;
    std::string debug_path;
    bool verbose = false;

    if (options.find("output") != options.end()) {
        output_path = options["output"];
    }
    if (options.find("mask") != options.end()) {
        mask_path = options["mask"];
    } else {
        printf("Please provide mask path\n");
        return -2;
    }
    if (options.find("debug") != options.end()) {
        debug_path = options["debug"];
    }
    if (options.find("verbose") != options.end()) {
        verbose = true;
    }

    cv::Mat mask = cv::imread(mask_path, 0);
    LoadPair prev_frame = load_pcd_file(paths[0]);

    //cv::Mat prev_points;
    std::vector<std::vector<PointType> > trace_3d;
    std::vector<cv::Point2f> prev_points;
    int maxCorners = 1000;
    double qualityLevel = 0.1;
    double minDistance = 25;
    int blockSize = 15;
    bool useHarrisDetector = false;
    double k = 0.04;

    cv::resize(
        mask, mask, prev_frame.first.size(), 0, 0, cv::INTER_NEAREST
    );


    std::vector<PointType> points;
    /*
    std::transform(
        prev_points.begin(), prev_points.end(), std::back_inserter(points),
        pick_init_point
    );
    */
    for (int i = 0; i < prev_points.size(); i++) {
        cv::Point2f p = prev_points[i];
        points.push_back(search_point(prev_frame.second, (int)p.x, (int)p.y));
    }
    trace_3d.push_back(points);
    /*
    cv::imshow("image", prev_frame.first);
    while (cv::waitKey(16) != 27) {}
    */

    std::future<LoadPair> load_task = std::async(
        std::launch::async, load_pcd_file, paths[1]
    );
    for (int i = 1; i < paths.size(); i++) {
        LoadPair frame = load_task.get();
        if (i < paths.size() - 1) {
            load_task = std::async(
                std::launch::async, load_pcd_file, paths[i + 1]
            );
        }
        cv::Mat flow;
        cv::calcOpticalFlowSF(
            prev_frame.first, frame.first, flow,
            3, 2, 4, 4.1, 25.5, 18, 55.0, 25.5, 0.35, 18, 55.0, 25.5, 10
        );
        cv::Mat rgb_flow = illustrate_flow(flow);
        cv::imshow("flow", rgb_flow);
        while (cv::waitKey(16) == 27) {}
        prev_frame = frame;
    }

    return 0;
}
