#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/obj_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/console/time.h>
#include <pcl/features/integral_image_normal.h>
#include <pcl/surface/organized_fast_mesh.h>
#include <pcl/PCLHeader.h>

#include <algorithm>
#include <vector>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <math.h>

typedef pcl::PointXYZRGB PointType;
typedef pcl::Normal NormalType;
typedef pcl::PointXYZRGBNormal FullType;

int main(int argc, char ** argv) {
  if (argc < 3) {
    printf("Please provide input PCD and output OBJ paths\n");
    exit(-2);
  }

  pcl::PointCloud<PointType>::Ptr cloud (new pcl::PointCloud<PointType>);
  if ( pcl::io::loadPCDFile <PointType> (argv[1], *cloud) == -1) {
    std::cout << "Cloud reading failed." << std::endl;
    return (-1);
  }

  pcl::PointCloud<NormalType>::Ptr normals (new pcl::PointCloud<NormalType>);
  // Estimate normals and curvature
  pcl::IntegralImageNormalEstimation<PointType, NormalType> ne;
  ne.setNormalEstimationMethod (ne.COVARIANCE_MATRIX);
  ne.setMaxDepthChangeFactor(0.02f);
  ne.setNormalSmoothingSize(10.0f);
  ne.setInputCloud(cloud);
  ne.compute(*normals);

  pcl::PointCloud<FullType>::Ptr full_cloud(new pcl::PointCloud<FullType>());
  full_cloud->resize(cloud->size());

  for (int i = 0; i < full_cloud->size(); i++) {
    PointType xyzrgb = (*cloud)[i];
    NormalType n = (*normals)[i];

    FullType p;
    p.x = xyzrgb.x;
    p.y = xyzrgb.y;
    p.z = xyzrgb.z;
    p.rgb = xyzrgb.rgb;
    p.normal_x = n.normal_x;
    p.normal_y = n.normal_y;
    p.normal_z = n.normal_z;

    (*full_cloud)[i] = p;
  }
  printf("sizes %i %i\n", cloud->size(), full_cloud->size());

  pcl::OrganizedFastMesh<PointType> mesher;
  mesher.setTriangulationType(
      mesher.TriangulationType::TRIANGLE_ADAPTIVE_CUT
  );
  //mesher.setMaxEdgeLength(100);
  //mesher.setTrianglePixelSize (10);
  //mesher.setTrianglePixelSizeRows (10);
  mesher.setInputCloud(cloud);
  std::vector<pcl::Vertices> polygons;
  mesher.reconstruct(polygons);

  printf("length %i\n", polygons[0].vertices.size());

  // Convert to PointCloud2
  pcl::PCLPointCloud2 export_cloud;
  pcl::toPCLPointCloud2(*cloud, export_cloud);

  pcl::PolygonMesh export_mesh;
  pcl::PCLHeader header;
  header.seq = 0;
  header.stamp = 0;
  header.frame_id = "yo";
  export_mesh.header = header;
  export_mesh.cloud = export_cloud;
  export_mesh.polygons = polygons;

  //pcl::io::saveOBJFile(argv[2], export_mesh);
  pcl::io::savePLYFile(argv[2], export_mesh);

  return 0;
}
