#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/photo/photo.hpp"
#include <opencv2/contrib/contrib.hpp>

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/visualization/pcl_visualizer.h>

#include <vector>
#include <algorithm>

typedef pcl::PointXYZRGB PointType;

bool visualize = false;

void show_float_image(cv::Mat map, std::string name = "out") {
  double min;
  double max;
  cv::minMaxIdx(map, &min, &max);
  cv::Mat adjMap = map.clone();

  adjMap -= min;
  adjMap *= 255.0 / (max - min);
  adjMap.convertTo(adjMap, CV_8UC1);
  // expand your range to 0..255. Similar to histEq();
  //map.convertTo(adjMap,CV_8UC1, 255 / (max-min), -min);

  //printf("image %s %f %f\n", name.c_str(), min, max);
  // this is great. It converts your grayscale image into a tone-mapped one,
  // much more pleasing for the eye
  // function is found in contrib module, so include contrib.hpp
  // and link accordingly
  cv::Mat falseColorsMap;
  applyColorMap(adjMap, falseColorsMap, cv::COLORMAP_JET);

  cv::imshow(name, falseColorsMap);
}

cv::Mat fill_missing(cv::Mat data, cv::Mat hole, cv::Mat valid) {
  data = data.clone();
  if (visualize && data.rows * data.cols > 12000) show_float_image(data, "org");

  //double min, max;
  //cv::minMaxLoc(data, &min, &max, 0, 0, valid);
  //data.setTo((max + min) * 0.5, valid);
  std::vector<double> mean, stddev;
  cv::meanStdDev(data, mean, stddev, valid);

  double min = mean[0] - 2 * stddev[0];
  double max = mean[0] + 2 * stddev[0];

  data -= min;
  data *= 255.0 / (max - min);
  data.convertTo(data, CV_8UC1);
  if (visualize && data.rows * data.cols > 12000) {
    //printf("yo %f %f\n", min, max);
    //cv::imshow("stuff", data);
    show_float_image(data, "previous");
    //while (cv::waitKey(16) != 27) {}
    //cv::destroyWindow("stuff");
  }
  cv::inpaint(data, hole, data, 10, cv::INPAINT_TELEA);
  if ( visualize && data.rows * data.cols > 12000) {
    show_float_image(data, "uint8");
    //while (cv::waitKey(16) != 27) {}
    //cv::destroyWindow("stuff");
  }
  data.convertTo(data, CV_32F);
  if (visualize && data.rows * data.cols > 12000) {
    show_float_image(data, "float32");
    //while (cv::waitKey(16) != 27) {}
    //cv::destroyWindow("stuff");
  }
  //printf("%f %f\n", min, max);
  data *= (max - min) / 255.0;
  data += min;
  cv::minMaxLoc(data, &min, &max);
  //printf("repair %f %f\n", min, max);
  if (visualize && data.rows * data.cols > 12000) {
    show_float_image(data, "scaled");
    while (cv::waitKey(16) != 27) {}
    //cv::destroyWindow("stuff");
  }
  return data;
}



int main(int argc, char ** argv) {
  pcl::PointCloud<PointType>::Ptr cloud (new pcl::PointCloud<PointType>);
  pcl::PointCloud<PointType>::Ptr cloud_filtered (new pcl::PointCloud<PointType>);

  if (argc < 3) {
    printf("Please provide input and output file\n");
    return -1;
  }

  // Fill in the cloud data
  pcl::PCDReader reader;
  // Replace the path below with the path where you saved your file
  reader.read<PointType> (argv[1], *cloud);

  int width = cloud->width;
  int height = cloud->height;

  cv::Mat valid_mask(height, width, CV_8UC1);
  cv::Mat hole_mask(height, width, CV_8UC1);
  valid_mask.setTo(0);
  hole_mask.setTo(0);

  cv::Mat im_x(height, width, CV_32F);
  cv::Mat im_y(height, width, CV_32F);
  cv::Mat im_z(height, width, CV_32F);

  for (int h = 0; h < height; h++) {
    for (int w = 0; w < width; w++) {
      int index = w + h * width;
      PointType p = (*cloud)[index];
      if (!isnan(p.x)) {
        valid_mask.data[index] = 255;
        im_x.at<float>(h, w) = p.x;
        im_y.at<float>(h, w) = p.y;
        im_z.at<float>(h, w) = p.z;
      }
    }
  }

  std::vector<std::vector<cv::Point> > contours;
  std::vector<cv::Vec4i> hierarchy;
  cv::findContours(
    valid_mask.clone(), contours, hierarchy, CV_RETR_TREE,
    CV_CHAIN_APPROX_NONE, cv::Point(0, 0)
  );

  for (int i = 0; i < hierarchy.size(); i++) {
    if (hierarchy[i][3] != -1) {
      cv::drawContours(hole_mask, contours, i, cv::Scalar(255, 255, 255), -1);
    }
  }

  for (int i = 0; i < hierarchy.size(); i++) {
    if (hierarchy[i][3] != -1) {
      cv::Rect roi = cv::boundingRect(contours[i]);
      int margin = 3;
      roi.x = std::max(0, roi.x - margin);
      roi.y = std::max(0, roi.y - margin);
      roi.width = std::min(width, roi.width + margin * 2);
      roi.height = std::min(height, roi.height+ margin * 2);

      cv::Mat x = im_x(roi);
      cv::Mat y = im_y(roi);
      cv::Mat z = im_z(roi);
      cv::Mat val = valid_mask(roi);
      cv::Mat paint = hole_mask(roi);

      //fill_missing(x, paint, val);

      fill_missing(x, paint, val).copyTo(x);
      fill_missing(y, paint, val).copyTo(y);
      fill_missing(z, paint, val).copyTo(z);


      // Do masked copying afterwards
    }
  }


  for (int h = 0; h < hole_mask.rows; h++) {
    for (int w = 0; w < hole_mask.cols; w++) {
      int index = w + h * hole_mask.cols;
      if (hole_mask.data[index]) {
        PointType p = (*cloud)[index];
        //printf("pre %f\n", p.x);
        p.x = im_x.at<float>(h, w);
        p.y = im_y.at<float>(h, w);
        p.z = im_z.at<float>(h, w);
        //printf("post %i %i %f %f %f\n", w, h, p.x, p.y, p.z);
        (*cloud)[index] = p;
      }
    }
  }

  show_float_image(im_x, "x_image");
  show_float_image(im_y, "y_image");
  show_float_image(im_z, "z_image");
  while(cv::waitKey(16) != 27) {}

/*
  pcl::visualization::PCLVisualizer viewer("cluser viewer");
  //viewer.showCloud(colored_cloud);
  //viewer.addPointCloud(applyUniformColor(*cloud, 255, 255, 255), "segmented");
  viewer.addPointCloud(cloud, "segmented");
  viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "segmented");
  viewer.addCoordinateSystem (1.0);
  viewer.initCameraParameters ();
  //viewer.setSize(1920, 1080);
  //viewer.setCameraPosition(0, 0, 0, 0, 0, 1);
  while (!viewer.wasStopped ())
  {
    viewer.spinOnce (100);
    boost::this_thread::sleep (boost::posix_time::microseconds (100000));
  }
  */
  //cv::imshow("valid", valid_mask);
  //cv::imshow("hole", hole_mask);
  //cv::imshow("x", im_x);
  //while(cv::waitKey(16) != 27) {}

  pcl::io::savePCDFile(argv[2], *cloud);

  return 0;
}
