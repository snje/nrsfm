# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.2

# compile CXX with /usr/bin/c++
CXX_FLAGS =   -Wno-deprecated -I/usr/include/vtk-5.8 -I/usr/local/include/pcl-1.7 -I/usr/include/eigen3 -I/usr/local/include/opencv -I/usr/local/include -I/usr/local/cuda-7.5/include    -std=gnu++11

CXX_DEFINES = -DDISABLE_OPENNI -DDISABLE_OPENNI2 -DDISABLE_PCAP -DDISABLE_PNG -DDISABLE_QHULL -DEIGEN_USE_NEW_STDVECTOR -DEIGEN_YES_I_KNOW_SPARSE_MODULE_IS_NOT_STABLE_YET -DFLANN_STATIC

