# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.2

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /repositories/nrsfm/mesh

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /repositories/nrsfm/mesh/build

# Include any dependencies generated for this target.
include CMakeFiles/proj_mesh.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/proj_mesh.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/proj_mesh.dir/flags.make

CMakeFiles/proj_mesh.dir/proj_mesh.cpp.o: CMakeFiles/proj_mesh.dir/flags.make
CMakeFiles/proj_mesh.dir/proj_mesh.cpp.o: ../proj_mesh.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /repositories/nrsfm/mesh/build/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/proj_mesh.dir/proj_mesh.cpp.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/proj_mesh.dir/proj_mesh.cpp.o -c /repositories/nrsfm/mesh/proj_mesh.cpp

CMakeFiles/proj_mesh.dir/proj_mesh.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/proj_mesh.dir/proj_mesh.cpp.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /repositories/nrsfm/mesh/proj_mesh.cpp > CMakeFiles/proj_mesh.dir/proj_mesh.cpp.i

CMakeFiles/proj_mesh.dir/proj_mesh.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/proj_mesh.dir/proj_mesh.cpp.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /repositories/nrsfm/mesh/proj_mesh.cpp -o CMakeFiles/proj_mesh.dir/proj_mesh.cpp.s

CMakeFiles/proj_mesh.dir/proj_mesh.cpp.o.requires:
.PHONY : CMakeFiles/proj_mesh.dir/proj_mesh.cpp.o.requires

CMakeFiles/proj_mesh.dir/proj_mesh.cpp.o.provides: CMakeFiles/proj_mesh.dir/proj_mesh.cpp.o.requires
	$(MAKE) -f CMakeFiles/proj_mesh.dir/build.make CMakeFiles/proj_mesh.dir/proj_mesh.cpp.o.provides.build
.PHONY : CMakeFiles/proj_mesh.dir/proj_mesh.cpp.o.provides

CMakeFiles/proj_mesh.dir/proj_mesh.cpp.o.provides.build: CMakeFiles/proj_mesh.dir/proj_mesh.cpp.o

CMakeFiles/proj_mesh.dir/io.cpp.o: CMakeFiles/proj_mesh.dir/flags.make
CMakeFiles/proj_mesh.dir/io.cpp.o: ../io.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /repositories/nrsfm/mesh/build/CMakeFiles $(CMAKE_PROGRESS_2)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/proj_mesh.dir/io.cpp.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/proj_mesh.dir/io.cpp.o -c /repositories/nrsfm/mesh/io.cpp

CMakeFiles/proj_mesh.dir/io.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/proj_mesh.dir/io.cpp.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /repositories/nrsfm/mesh/io.cpp > CMakeFiles/proj_mesh.dir/io.cpp.i

CMakeFiles/proj_mesh.dir/io.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/proj_mesh.dir/io.cpp.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /repositories/nrsfm/mesh/io.cpp -o CMakeFiles/proj_mesh.dir/io.cpp.s

CMakeFiles/proj_mesh.dir/io.cpp.o.requires:
.PHONY : CMakeFiles/proj_mesh.dir/io.cpp.o.requires

CMakeFiles/proj_mesh.dir/io.cpp.o.provides: CMakeFiles/proj_mesh.dir/io.cpp.o.requires
	$(MAKE) -f CMakeFiles/proj_mesh.dir/build.make CMakeFiles/proj_mesh.dir/io.cpp.o.provides.build
.PHONY : CMakeFiles/proj_mesh.dir/io.cpp.o.provides

CMakeFiles/proj_mesh.dir/io.cpp.o.provides.build: CMakeFiles/proj_mesh.dir/io.cpp.o

# Object files for target proj_mesh
proj_mesh_OBJECTS = \
"CMakeFiles/proj_mesh.dir/proj_mesh.cpp.o" \
"CMakeFiles/proj_mesh.dir/io.cpp.o"

# External object files for target proj_mesh
proj_mesh_EXTERNAL_OBJECTS =

proj_mesh: CMakeFiles/proj_mesh.dir/proj_mesh.cpp.o
proj_mesh: CMakeFiles/proj_mesh.dir/io.cpp.o
proj_mesh: CMakeFiles/proj_mesh.dir/build.make
proj_mesh: /usr/lib/x86_64-linux-gnu/libboost_system.so
proj_mesh: /usr/lib/x86_64-linux-gnu/libboost_filesystem.so
proj_mesh: /usr/lib/x86_64-linux-gnu/libboost_thread.so
proj_mesh: /usr/lib/x86_64-linux-gnu/libboost_date_time.so
proj_mesh: /usr/lib/x86_64-linux-gnu/libboost_iostreams.so
proj_mesh: /usr/lib/x86_64-linux-gnu/libboost_serialization.so
proj_mesh: /usr/lib/x86_64-linux-gnu/libboost_chrono.so
proj_mesh: /usr/lib/x86_64-linux-gnu/libpthread.so
proj_mesh: /usr/local/lib/libpcl_common.so
proj_mesh: /usr/lib/x86_64-linux-gnu/libflann_cpp_s.a
proj_mesh: /usr/local/lib/libpcl_kdtree.so
proj_mesh: /usr/local/lib/libpcl_octree.so
proj_mesh: /usr/local/lib/libpcl_search.so
proj_mesh: /usr/local/lib/libpcl_surface.so
proj_mesh: /usr/local/lib/libpcl_io.so
proj_mesh: /usr/local/lib/libpcl_sample_consensus.so
proj_mesh: /usr/local/lib/libpcl_filters.so
proj_mesh: /usr/local/lib/libpcl_visualization.so
proj_mesh: /usr/local/lib/libpcl_outofcore.so
proj_mesh: /usr/local/lib/libpcl_features.so
proj_mesh: /usr/local/lib/libpcl_segmentation.so
proj_mesh: /usr/local/lib/libpcl_people.so
proj_mesh: /usr/local/lib/libpcl_tracking.so
proj_mesh: /usr/local/lib/libpcl_keypoints.so
proj_mesh: /usr/local/lib/libpcl_registration.so
proj_mesh: /usr/local/lib/libpcl_recognition.so
proj_mesh: /usr/lib/x86_64-linux-gnu/libboost_system.so
proj_mesh: /usr/lib/x86_64-linux-gnu/libboost_filesystem.so
proj_mesh: /usr/lib/x86_64-linux-gnu/libboost_thread.so
proj_mesh: /usr/lib/x86_64-linux-gnu/libboost_date_time.so
proj_mesh: /usr/lib/x86_64-linux-gnu/libboost_iostreams.so
proj_mesh: /usr/lib/x86_64-linux-gnu/libboost_serialization.so
proj_mesh: /usr/lib/x86_64-linux-gnu/libboost_chrono.so
proj_mesh: /usr/lib/x86_64-linux-gnu/libpthread.so
proj_mesh: /usr/lib/x86_64-linux-gnu/libflann_cpp_s.a
proj_mesh: /usr/lib/libvtkGenericFiltering.so.5.8.0
proj_mesh: /usr/lib/libvtkGeovis.so.5.8.0
proj_mesh: /usr/lib/libvtkCharts.so.5.8.0
proj_mesh: /usr/local/lib/libpcl_common.so
proj_mesh: /usr/local/lib/libpcl_kdtree.so
proj_mesh: /usr/local/lib/libpcl_octree.so
proj_mesh: /usr/local/lib/libpcl_search.so
proj_mesh: /usr/local/lib/libpcl_surface.so
proj_mesh: /usr/local/lib/libpcl_io.so
proj_mesh: /usr/local/lib/libpcl_sample_consensus.so
proj_mesh: /usr/local/lib/libpcl_filters.so
proj_mesh: /usr/local/lib/libpcl_visualization.so
proj_mesh: /usr/local/lib/libpcl_outofcore.so
proj_mesh: /usr/local/lib/libpcl_features.so
proj_mesh: /usr/local/lib/libpcl_segmentation.so
proj_mesh: /usr/local/lib/libpcl_people.so
proj_mesh: /usr/local/lib/libpcl_tracking.so
proj_mesh: /usr/local/lib/libpcl_keypoints.so
proj_mesh: /usr/local/lib/libpcl_registration.so
proj_mesh: /usr/local/lib/libpcl_recognition.so
proj_mesh: /usr/lib/libvtkViews.so.5.8.0
proj_mesh: /usr/lib/libvtkInfovis.so.5.8.0
proj_mesh: /usr/lib/libvtkWidgets.so.5.8.0
proj_mesh: /usr/lib/libvtkVolumeRendering.so.5.8.0
proj_mesh: /usr/lib/libvtkHybrid.so.5.8.0
proj_mesh: /usr/lib/libvtkParallel.so.5.8.0
proj_mesh: /usr/lib/libvtkRendering.so.5.8.0
proj_mesh: /usr/lib/libvtkImaging.so.5.8.0
proj_mesh: /usr/lib/libvtkGraphics.so.5.8.0
proj_mesh: /usr/lib/libvtkIO.so.5.8.0
proj_mesh: /usr/lib/libvtkFiltering.so.5.8.0
proj_mesh: /usr/lib/libvtkCommon.so.5.8.0
proj_mesh: /usr/lib/libvtksys.so.5.8.0
proj_mesh: CMakeFiles/proj_mesh.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX executable proj_mesh"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/proj_mesh.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/proj_mesh.dir/build: proj_mesh
.PHONY : CMakeFiles/proj_mesh.dir/build

CMakeFiles/proj_mesh.dir/requires: CMakeFiles/proj_mesh.dir/proj_mesh.cpp.o.requires
CMakeFiles/proj_mesh.dir/requires: CMakeFiles/proj_mesh.dir/io.cpp.o.requires
.PHONY : CMakeFiles/proj_mesh.dir/requires

CMakeFiles/proj_mesh.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/proj_mesh.dir/cmake_clean.cmake
.PHONY : CMakeFiles/proj_mesh.dir/clean

CMakeFiles/proj_mesh.dir/depend:
	cd /repositories/nrsfm/mesh/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /repositories/nrsfm/mesh /repositories/nrsfm/mesh /repositories/nrsfm/mesh/build /repositories/nrsfm/mesh/build /repositories/nrsfm/mesh/build/CMakeFiles/proj_mesh.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/proj_mesh.dir/depend

