#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/io/vtk_lib_io.h>
#include <pcl/conversions.h>

#include <vector>
#include <algorithm>


typedef pcl::PointXYZRGB PointType;
typedef pcl::PointCloud<PointType> CloudType;

int main(int argc, char ** argv) {
    std::vector<int> input = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    std::vector<int> output;

    std::remove_copy_if(
        input.begin(), input.end(), std::back_inserter(output),
        [] (int i) {return i%2;}
    );

    for (int i = 0; i < output.size(); i++) {
        printf("%i ", output[i]);
    }
    printf("\n");

    pcl::PointCloud<PointType>::Ptr cloud (new pcl::PointCloud<PointType>);
    if ( pcl::io::loadPCDFile <PointType> (argv[1], *cloud) == -1) {
        printf("Failed to load file %s\n", argv[1]);
        return -1;
    }

    pcl::io::savePCDFileBinaryCompressed(argv[2], *cloud);

    return 0;
}
