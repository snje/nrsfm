#include <math.h>

#include <iostream>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/statistical_outlier_removal.h>

#include <vector>
#include <algorithm>
#include <map>

#include <unistd.h>
#include <getopt.h>

typedef pcl::PointXYZRGB PointType;

inline float distance(PointType p1, PointType p2) {
  float dx = p1.x - p2.x;
  float dy = p1.y - p2.y;
  float dz = p1.z - p2.z;
  return sqrt(dx * dx + dy * dy + dz * dz);
}

int main (int argc, char** argv)
{
  pcl::PointCloud<PointType>::Ptr cloud (new pcl::PointCloud<PointType>);
  pcl::PointCloud<PointType>::Ptr cloud_filtered (new pcl::PointCloud<PointType>);

  int c;
  int digit_optind = 0;
  std::map<std::string, std::string> options;
  while (1) {
      int this_option_optind = optind ? optind : 1;
      int option_index = 0;
      static struct option long_options[] = {
          {"threshold", required_argument, 0,  0 },
          {"size", required_argument, 0,  0 },
          {"percent", required_argument, 0,  0 },
          {0,         0,                 0,  0 }
      };
      c = getopt_long(argc, argv, "abc:d:012",
      long_options, &option_index);
      if (c == -1) break;

      switch (c) {
          case 0:
          {
              std::string key(long_options[option_index].name);
              std::string val = optarg ? std::string(optarg): std::string();
              options[key] = val;
              break;
          }
          case '?':
              break;
          default:
              printf("?? getopt returned character code 0%o ??\n", c);
      }
  }
  std::vector<std::string> paths;
  if (argc - optind < 2) {
      printf("Please provide input and output path");
      return 1;
  }
  std::string input_path(argv[optind]);
  std::string output_path(argv[optind + 1]);

  // Fill in the cloud data
  pcl::PCDReader reader;
  // Replace the path below with the path where you saved your file
  reader.read<PointType> (input_path, *cloud);


  int height = cloud->height;
  int width = cloud->width;
  int k = 5;
  if (options.find("size") != options.end()) {
      k = atoi(options["size"].c_str());
  }
  int min_count = 1;
  if (options.find("percent") != options.end()) {
      float p = atof(options["percent"].c_str());
      float count = 2 * k + 1;
      count *= count;
      min_count = std::max((int)(p * count), 1);
  }

  std::vector<float> mean_distance(height * width);

  for (int h = 0; h < height; h++) {
    for (int w = 0; w < width; w++) {
      int index = w + width * h;
      PointType p1 = (*cloud)[index];
      if (isnan(p1.x)) {
        mean_distance[index] = -1;
        continue;
      }

      float dist_sum = 0;
      float count = 0;
      for (int y = std::max(0, h - k); y < std::min(height, h + 1 + k); y++) {
        for (int x = std::max(0, w - k); x < std::min(width, w + 1 + k); x++) {
          int index2 = x + y * width;
          PointType p2 = (*cloud)[index2];
          if (isnan(p2.x)) continue;
          dist_sum += distance(p1, p2);
          count++;
        }
      }
      if (count <= min_count) {
        mean_distance[index] = -1;
      } else {
        mean_distance[index] = dist_sum / count;
      }
    }
  }

  float mean_mean_dist = 0;
  float count = 0;

  for (int i = 0; i < mean_distance.size(); i++) {
    float md = mean_distance[i];
    if (md >= 0) {
      mean_mean_dist += md;
      count++;
    }
  }

  mean_mean_dist /= count;

  float var_mean_dist = 0;

  for (int i = 0; i < mean_distance.size(); i++) {
    float md = mean_distance[i];
    if (md >= 0) {
      float d = md - mean_mean_dist;
      var_mean_dist += d * d;
    }
  }

  var_mean_dist /= count;

  float factor = 1.0;
  if (options.find("threshold") != options.end()) {
      factor = atof(options["threshold"].c_str());
  }
  printf("runnign with %f\n", factor);

  for (int i = 0; i < mean_distance.size(); i++) {
    float md = mean_distance[i];
    float diff = md - mean_mean_dist;
    diff *= diff;
    diff /= var_mean_dist;
    if (md < 0 || diff > factor) {
      PointType p = (*cloud)[i];
      p.x = NAN;
      p.y = NAN;
      p.z = NAN;
      (*cloud)[i] = p;
    }
  }

  printf("dense %i %f\n", cloud->is_dense, NAN);
  pcl::io::savePCDFileBinaryCompressed(output_path, *cloud);

  return (0);
}
