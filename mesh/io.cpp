#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <pcl/io/ply_io.h>

#include "io.h"


int read_trace_csv(
    std::string path, std::vector<float> * output_array, int * frames,
    int * points
) {
    char buffer[1024 * 1024] ;
    char *record,*line;
    int i=0,j=0;
    FILE *fstream = fopen(path.c_str(), "r");
    if(fstream == NULL) {
       printf("\n file opening failed ");
       return -1 ;
    }
    output_array->clear();
    int _prev_points = -1;
    int _frames = 0;
    while((line = fgets(buffer,sizeof(buffer), fstream)) != NULL) {
        record = strtok(line, " ");
        int _points = 0;
        while(record != NULL) {
            _points++;
            output_array->push_back(atof(record));
            record = strtok(NULL, " ");
        }
        if (_prev_points != -1 && _prev_points != _points) {
            printf("Point count mismatched in rows\n");
            return -1;
        }
        _frames++;
        _prev_points = _points;
    }
    if (_frames % 3 != 0) {
        printf("Line count was a divisor of 3\n");
        return -2;
    }

    *points = _prev_points;
    *frames = _frames / 3;

    return 0;
}

int save_trace_csv(
    std::string path, std::vector<float> & array, int frames, int points
) {
    if (array.size() != 3 * frames * points) return 1;

    FILE * file = fopen(path.c_str(), "w");
    for (int f = 0; f < frames * 3; f++) {
        for (int p = 0; p < points; p++) {
            fprintf(file, "%.6f ", array[f * points + p]);
        }
        fprintf(file, "\n");
    }

    fclose(file);
    return 0;
}

int save_triangle_mesh(std::string path, std::vector<pcl::Vertices> triangles) {
    FILE * f = fopen(path.c_str(), "w");
    if (!f) return 1;
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < triangles.size(); j++) {
            pcl::Vertices t = triangles[j];
            fprintf(f, "%i ", t.vertices[i]);
        }
        fprintf(f, "\n");
    }
    fclose(f);
    return 0;
}
