#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/features/normal_3d.h>
#include <pcl/surface/gp3.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/io/vtk_lib_io.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>

#include <map>

#include "io.h"

int extract_frames(
    std::vector<float> point_array, int frame, int points, int frames,
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud
) {
    if (frame >= frames) return 1;
    cloud->resize(points);

    int index = frame * points * 3;

    for (int i = 0; i < points; i++) {
        pcl::PointXYZ p;
        p.x = point_array[index + i];
        p.y = point_array[index + points + i];
        p.z = point_array[index + 2 * points + i];
        (*cloud)[i] = p;
    }
    return 0;
}

pcl::PointXYZ mean_center(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud) {
    pcl::PointXYZ m;
    m.x, m.y, m.z = 0, 0, 0;
    for (int i = 0; i < cloud->size(); i++) {
        pcl::PointXYZ p = (*cloud)[i];
        m.x += p.x;
        m.y += p.y;
        m.z += p.z;
    }

    double s = 1.0 / cloud->size();
    m.x *= s;
    m.y *= s;
    m.z *= s;
    return m;
}

int main(int argc, char ** argv) {
    int c;
    int digit_optind = 0;
    std::map<std::string, std::string> options;
    while (1) {
        int this_option_optind = optind ? optind : 1;
        int option_index = 0;
        static struct option long_options[] = {
            {"normal", required_argument, 0,  0 },
            {"mesh",  required_argument, 0, 0},
            {"frame",  required_argument, 0, 0},
            {"visualize",  no_argument, 0, 0},
            {0,         0,                 0,  0 }
        };
        c = getopt_long(argc, argv, "abc:d:012",
        long_options, &option_index);
        if (c == -1) break;

        switch (c) {
            case 0:
            {
                std::string key(long_options[option_index].name);
                std::string val = optarg ? std::string(optarg): std::string();
                options[key] = val;
                break;
            }
            case '?':
                break;
            default:
                printf("?? getopt returned character code 0%o ??\n", c);
        }
    }

    if (optind >= argc) return -1;


    std::vector<float> output_array;
    int frames, points;
    read_trace_csv(argv[optind], &output_array, &frames, &points);
    bool viz = false;
    if (options.find("visualize") != options.end()) {
        viz = true;
    }

  int ex_frame = 0;
  if (options.find("frame") != options.end()) {
      ex_frame = atoi(options["frame"].c_str());
  }
  // Load input file into a PointCloud<T> with an appropriate type
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
  extract_frames(output_array, ex_frame, points, frames, cloud);

  /*
  for (int i = 0; i < cloud->size(); i++) {
      if ((*cloud)[i].x > 15) {
          (*cloud)[i].x += 400;
      }
  }
  */

  //printf("size %i\n", cloud->size());
  //* the data should be available in cloud

  // Normal estimation*
  pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> n;
  pcl::PointCloud<pcl::Normal>::Ptr normals (new pcl::PointCloud<pcl::Normal>);
  pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
  tree->setInputCloud (cloud);
  n.setInputCloud (cloud);
  n.setSearchMethod (tree);
  n.setKSearch (15);
  n.compute (*normals);
  //* normals should not contain the point normals + surface curvatures

  // Concatenate the XYZ and normal fields*
  pcl::PointCloud<pcl::PointNormal>::Ptr cloud_with_normals (new pcl::PointCloud<pcl::PointNormal>);
  pcl::concatenateFields (*cloud, *normals, *cloud_with_normals);
  //* cloud_with_normals = cloud + normals

  // Create search tree*
  pcl::search::KdTree<pcl::PointNormal>::Ptr tree2 (new pcl::search::KdTree<pcl::PointNormal>);
  tree2->setInputCloud (cloud_with_normals);

  // Initialize objects
  pcl::GreedyProjectionTriangulation<pcl::PointNormal> gp3;
  pcl::PolygonMesh triangles;

  // Set the maximum distance between connected points (maximum edge length)
  gp3.setSearchRadius (50);

  // Set typical values for the parameters
  gp3.setMu (3.0);
  //gp3.setMu (3.75);
  //gp3.setMu (2.0);
  gp3.setMaximumNearestNeighbors (150);
  gp3.setMaximumSurfaceAngle(M_PI/4); // 45 degrees
  gp3.setMinimumAngle(M_PI/18); // 10 degrees
  gp3.setMaximumAngle(3*M_PI/3); // 120 degrees
  gp3.setMaximumAngle(2.0*M_PI/3); // 120 degrees
  gp3.setNormalConsistency(false);

  // Get result
  gp3.setInputCloud (cloud_with_normals);
  gp3.setSearchMethod (tree2);
  gp3.reconstruct (triangles);

  // Additional vertex information
  //std::vector<int> parts = gp3.getPartIDs();
  //std::vector<int> states = gp3.getPointStates();
  //printf("yo\n");

  if (viz) {
      pcl::visualization::PCLVisualizer viewer("Mesh viewer");
      viewer.addPolygonMesh(triangles);
      viewer.addPointCloud(cloud, "point");
      viewer.setPointCloudRenderingProperties (
          pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 10, "point"
      );
      viewer.addPointCloudNormals<pcl::PointXYZ, pcl::Normal> (
          cloud, normals, 1, 5, "normal"
      );
      viewer.addCoordinateSystem (1.0);
      viewer.initCameraParameters ();
      pcl::PointXYZ center = mean_center(cloud);
      printf("center %f %f %f\n", center.x, center.y, center.z);
      //viewer.setCameraPosition(-center.x, -center.y, -center.z, 0, 0, 1);
      while (!viewer.wasStopped ())
      {
        viewer.spinOnce (100);
        boost::this_thread::sleep (boost::posix_time::microseconds (100000));
      }
 }

  std::vector<float> normal_array;

  for (int f = 0; f < frames; f++) {
       extract_frames(output_array, f, points, frames, cloud);
       tree->setInputCloud (cloud);
       n.setInputCloud (cloud);
       n.setSearchMethod (tree);
       n.setKSearch (15);
       n.compute (*normals);

       for (int i = 0; i < normals->size(); i++) {
           pcl::Normal norms = (*normals)[i];
           normal_array.push_back(norms.normal_x);
       }
       for (int i = 0; i < normals->size(); i++) {
           pcl::Normal norms = (*normals)[i];
           normal_array.push_back(norms.normal_y);
       }
       for (int i = 0; i < normals->size(); i++) {
           pcl::Normal norms = (*normals)[i];
           normal_array.push_back(norms.normal_z);
       }
  }



  std::string normal_path = "./normals.txt";
  if (options.find("normal") != options.end()) {
      normal_path = options["normal"];
  }
  std::string mesh_path = "./mesh.txt";
  if (options.find("mesh") != options.end()) {
      mesh_path = options["mesh"];
  }

  //save_trace_csv(normal_path, normal_array, frames, points);
  save_triangle_mesh(mesh_path, triangles.polygons);
  // Finish
  return (0);
}
