#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/obj_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/console/time.h>
#include <pcl/features/integral_image_normal.h>
#include <pcl/surface/organized_fast_mesh.h>
#include <pcl/PCLHeader.h>
#include <pcl/io/vtk_lib_io.h>

#include <algorithm>
#include <vector>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <math.h>

typedef pcl::PointXYZRGB PointType;
typedef pcl::Normal NormalType;
typedef pcl::PointXYZRGBNormal FullType;

inline int find_non_nan(pcl::PointCloud<PointType>::Ptr cloud, int h, int w_init) {
    for (int  w = w_init; w < cloud->width; w++) {
        int index = w + h * cloud->width;
        PointType p = (*cloud)[index];
        if (!isnan(p.x)) return w;
    }
    return cloud->width;
}

struct TriangleValidParam {
    float pixel_max_side;
    float pixel_max_area;
    float real_max_side;
};

inline float distance(float x1, float y1, float x2, float y2) {
    float dx = x1 - x2;
    float dy = y1 - y2;
    return sqrt(dx * dx + dy * dy);
}

inline float distanceP(PointType p1, PointType p2) {
    float dx = p1.x - p2.x;
    float dy = p1.y - p2.y;
    float dz = p1.z - p2.z;
    return sqrt(dx * dx + dy * dy + dz * dz);
}

bool valid_triangle(
    pcl::PointCloud<PointType>::Ptr cloud, int w1, int h1, int w2, int h2,
    int w3, int h3, TriangleValidParam param
) {
    PointType p1 = (*cloud)[w1 + h1 * cloud->width];
    PointType p2 = (*cloud)[w2 + h2 * cloud->width];
    PointType p3 = (*cloud)[w3 + h3 * cloud->width];
    return (
        distance(w1, h1, w2, h2) < param.pixel_max_side &&
        distance(w1, h1, w3, h3) < param.pixel_max_side &&
        distance(w3, h3, w2, h2) < param.pixel_max_side &&
        distanceP(p1, p2) < param.real_max_side &&
        distanceP(p1, p3) < param.real_max_side &&
        distanceP(p3, p2) < param.real_max_side
    );
}

int main(int argc, char ** argv) {
  if (argc < 3) {
    printf("Please provide input PCD and output OBJ paths\n");
    exit(-2);
  }

  pcl::PointCloud<PointType>::Ptr cloud (new pcl::PointCloud<PointType>);
  if ( pcl::io::loadPCDFile <PointType> (argv[1], *cloud) == -1) {
    std::cout << "Cloud reading failed." << std::endl;
    return (-1);
  }

  pcl::PointCloud<NormalType>::Ptr normals (new pcl::PointCloud<NormalType>);
  // Estimate normals and curvature
  pcl::IntegralImageNormalEstimation<PointType, NormalType> ne;
  ne.setNormalEstimationMethod (ne.COVARIANCE_MATRIX);
  ne.setMaxDepthChangeFactor(0.02f);
  ne.setNormalSmoothingSize(10.0f);
  ne.setInputCloud(cloud);
  ne.compute(*normals);

  pcl::PointCloud<FullType>::Ptr full_cloud(new pcl::PointCloud<FullType>());
  full_cloud->resize(cloud->size());

  for (int i = 0; i < full_cloud->size(); i++) {
    PointType xyzrgb = (*cloud)[i];
    NormalType n = (*normals)[i];

    FullType p;
    p.x = xyzrgb.x;
    p.y = xyzrgb.y;
    p.z = xyzrgb.z;
    p.rgb = xyzrgb.rgb;
    p.normal_x = n.normal_x;
    p.normal_y = n.normal_y;
    p.normal_z = n.normal_z;

    (*full_cloud)[i] = p;
  }

  std::vector<pcl::Vertices> polygons;

  int width = cloud->width;
  int height = cloud->height;

  TriangleValidParam param;
  param.pixel_max_side = 100;
  param.real_max_side = 0.05;

  for (int h = 0; h < height - 1; h++) {
      uint32_t w1 = find_non_nan(cloud, h, 0);
      uint32_t w2 = find_non_nan(cloud, h + 1, 0);
      while (w1 < width && w2 < width) {
          uint32_t w1_next = find_non_nan(cloud, h, w1 + 1);
          uint32_t w2_next = find_non_nan(cloud, h + 1, w2 + 1);
          if (w1_next >= width && w2_next >= width) break;
          pcl::Vertices vert;
          if (w1_next < w2_next) {
              if (
                  valid_triangle(cloud, w1, h, w2, h + 1, w1_next, h, param)
              ) {
                  /*
                  vert.vertices = {
                      w1 + h * width,
                      w2 + (h + 1) * width,
                      w1_next + h * width
                  };
                  */
                  vert.vertices.push_back(w1 + h * width);
                  vert.vertices.push_back(w2 + (h + 1) * width);
                  vert.vertices.push_back(w1_next + h * width);
              }
              w1 = w1_next;
          } else {
              if (
                  valid_triangle(cloud, w1, h, w2, h + 1, w2_next, h + 1, param)
              ) {
                  /*
                  vert.vertices = {
                      w1 + h * width,
                      w2 + (h + 1) * width,
                      w2_next + (h + 1) * width
                  };
                  */
                  vert.vertices.push_back(w1 + h * width);
                  vert.vertices.push_back(w2 + (h + 1) * width);
                  vert.vertices.push_back(w2_next + (h + 1) * width);
              }
              w2 = w2_next;
          }
          if (vert.vertices.size() == 3) polygons.push_back(vert);
      }
  }

  // Convert to PointCloud2
  pcl::PCLPointCloud2 export_cloud;
  pcl::toPCLPointCloud2(*full_cloud, export_cloud);

  pcl::PolygonMesh export_mesh;
  pcl::PCLHeader header;
  header.seq = 0;
  header.stamp = 0;
  header.frame_id = "yo";
  export_mesh.header = header;
  export_mesh.cloud = export_cloud;
  export_mesh.polygons = polygons;

  //pcl::io::saveOBJFile(argv[2], export_mesh);
  pcl::io::savePLYFileBinary(argv[2], export_mesh);

  return 0;
}
