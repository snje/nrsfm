#include <iostream>
#include <vector>
#include <pcl/io/ply_io.h>
#include <pcl/io/vtk_lib_io.h>
#include <pcl/point_types.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/filters/passthrough.h>
#include <pcl/segmentation/min_cut_segmentation.h>
#include <stdio.h>

#include <boost/thread/thread.hpp>
#include <pcl/common/common_headers.h>
#include <pcl/features/normal_3d.h>
#include <pcl/visualization/pcl_visualizer.h>

typedef pcl::PointXYZRGB CloudType;

int main (int argc, char** argv)
{
  if (argc < 2) {
    printf("please provide a ply file");
    return -1;
  }
  pcl::PolygonMesh mesh;
  if ( pcl::io::loadPolygonFilePLY (argv[1], mesh) == -1 )
  {
    std::cout << "Mesh reading failed." << std::endl;
    return (-1);
  }

  pcl::visualization::PCLVisualizer viewer("Mesh viewer");
  //viewer.showCloud(colored_cloud);
  //viewer.addPointCloud(applyUniformColor(*cloud, 255, 255, 255), "segmented");
  //viewer.addPointCloud(cloud, "segmented");
  //viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "segmented");
  viewer.setBackgroundColor (0.2, 0.8, 1.0);
  viewer.addPolygonMesh(mesh);
  viewer.addCoordinateSystem (1.0);
  viewer.initCameraParameters ();
  //viewer.setSize(1920, 1080);
  //viewer.setCameraPosition(0, 0, 0, 0, 0, 1);
  while (!viewer.wasStopped ())
  {
    viewer.spinOnce (100);
    boost::this_thread::sleep (boost::posix_time::microseconds (100000));
  }

  return (0);
}
