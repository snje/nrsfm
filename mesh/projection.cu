#include <pcl/point_types.h>
//#include <pcl/io/vtk_lib_io.h>
#include <pcl/PolygonMesh.h>
//#include <pcl/io/ply_io.h>

#include <cuda.h>


#define EPSILON 1e-6

__device__ float3 vadd(const float3 v1, const float3 v2) {
    return make_float3(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
}

__device__ float3 vsub(const float3 v1, const float3 v2) {
    return make_float3(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
}

__device__ float3 vcross(const float3 v1, const float3 v2) {
    return make_float3(
        v1.y * v2.z - v2.y * v1.z,
        v1.z * v2.x - v2.z * v1.x,
        v1.x * v2.y - v2.x * v1.y
    );
}

__device__ float vdot(const float3 v1, const float3 v2) {
    return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}

__device__ float vlength(const float3 v) {
    return sqrt(vdot(v, v));
}

__device__ float3 vnormalize(const float3 v) {
    float l = vlength(v);
    if (l < EPSILON) return v;

    return make_float3(v.x / l, v.y / l, v.z / l);
}

__device__ float3 vnegate(const float3 v) {
    return make_float3(-v.x, -v.y, -v.z);
}

// https://en.wikipedia.org/wiki/M%C3%B6ller%E2%80%93Trumbore_intersection_algorithm
__device__  int ray_triangle(
    float3 v1, float3 v2, float3 v3, float3 o, float3 d, float * s
) {
    float3 e1 = vsub(v2, v1);
    float3 e2 = vsub(v3, v1);
    float3 p = vcross(d, e2);
    float det = vdot(e1, p);
    if (det > -EPSILON && det < EPSILON) return 0;
    float inv_det = 1.0f / det;

    float3 t = vsub(o, v1);
    float u = vdot(t, p) * inv_det;
    if (u < 0.f || u > 1.f) return 0;

    float3 q = vcross(t, e1);

    float v = vdot(d, q) * inv_det;

    if (v < 0.f || u + v > 1.f) return 0;

    float dt = vdot(e2, q) * inv_det;

    if(dt > EPSILON) { //ray intersection
        *s = dt;
        return 1;
    }

    // No hit, no win
    return 0;
}

__global__ void collision_detection(
    int n_triangles, int n_points, float3 * points, float3 * vertices,
    int3 * tri_indices, int * collision
) {
    int i_triangle = blockIdx.x * blockDim.x + threadIdx.x;
    int i_point = blockIdx.y * blockDim.y + threadIdx.y;

    if (i_triangle >= n_triangles || i_point >= n_points) return;

    int3 indices = tri_indices[i_triangle];
    float3 v1 = points[indices.x];
    float3 v2 = points[indices.y];
    float3 v3 = points[indices.z];
    float3 o = points[i_point];
    float3 d = vnegate(vnormalize(o));

    int c_index = i_triangle + i_point * n_triangles;
    float s;
    collision[c_index] = ray_triangle(v1, v2, v3, o, d, &s);
}

__global__ void visibility_test(
    int n_triangles, int n_points, int * collision, int * visibility
) {
    int i_point = blockIdx.x * blockDim.x + threadIdx.x;

    if (i_point >= n_points) return;

    int col = 0;
    for (int j = 0; j < n_triangles; j++) {
        int index = j + n_triangles * i_point;
        col |= collision[index];
    }
    visibility[i_point] = col;
}

int main(int argc, char ** argv) {
    for (int i = 1; i < argc; i++) {
        pcl::PolygonMesh mesh;
        //pcl::io::loadPLYFile (argv[i], &mesh);
    }

    return 0;
}
