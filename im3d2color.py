#! /usr/bin/env python

import pypcd
import cv2
import numpy as np
import structuredlight as stl
import sys
import util

if len(sys.argv) < 3:
    print "Please supply input and output path"
    sys.exit(1)

p, _, c = stl.im3dread(sys.argv[1])

def _balance(im):
    v = im.reshape(-1)
    v_sort = np.sort(v)
    p4 = int(0.04 * len(v))
    vmin = v_sort[p4]
    vmax = v_sort[-p4]
    im = np.clip(im, vmin, vmax)
    return cv2.normalize(im, alpha = 0, beta = 255, norm_type = cv2.NORM_MINMAX)

def balance(im):
    bim = np.copy(im)
    for i in xrange(3):
        bim[:, :, i] = _balance(bim[:, :, i])
    return bim

#c = balance(c)


#c = cv2.resize(c, (0, 0), fx = 0.1, fy = 0.1)
cv2.imwrite(sys.argv[2], c)
