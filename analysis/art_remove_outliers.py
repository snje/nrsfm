#!/usr/bin/env python
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib.tri import Triangulation
from scipy.spatial import ConvexHull
from scipy.spatial import Delaunay
import numpy as np
import plyfile
import sys

if len(sys.argv) < 2:
    print "Please provide trace path and output path"
    sys.exit(1)
    
W = np.loadtxt(sys.argv[1])
N = np.loadtxt(sys.argv[2])
x, y, z = W[::3, :], W[1::3, :], W[2::3, :]
invalid = z < -37
invalid = np.sum(invalid, axis = 0) > 0
np.savetxt(sys.argv[3], W[:, ~invalid])
np.savetxt(sys.argv[4], N[:, ~invalid])
