This dataset structure contains two main directories:
  * sequence    ::    Contains the projected 2D paths
  * visibility  ::    Contains the visibility corresponding to the given projected paths
  
The former contains a collection of files, generated via projecting the ground truth sequence using either perspective or orthogonal projection (a subfolder for each). Each projected track file has been produced using a different type of camera pathing, given by its name. Track files are formatted in a CSV style with white-spacing as separator. Each is given as a matrix where each column defines a points coordinates over the sequence with x, y and z arriving in alternating order. This is illustrated below:

        __                  __
        | x_00 x_01 ... x_0P |
        | y_00 y_01 ... y_0P |
        | x_10 x_11 ... x_1P |
S   =   | y_10 y_11 ... y_1P |
        | :     :   :     :  |
        | x_F0 x_F1 ... x_FP |
        | y_F0 y_F1 ... y_FP |
        --                  --

Visibility data is given as a similar structure. Each entry in the matrix has a boolean value, 0 if the point is occluded and 1 if it is visible. Notice that the visibility variables are replicated in pairs in order to facilite easy boolean lookup e.g. with Numpy or Matlab.

        __                  __
        | v_00 v_01 ... v_0P |
        | v_00 v_01 ... v_0P |
        | v_10 v_11 ... v_1P |
V  =    | v_10 v_11 ... v_1P |
        | :     :   :     :  |
        | v_F0 v_F1 ... v_FP |
        | v_F0 v_F1 ... v_FP |
        --                  --

The visibility corresponding for a given sequence can be found by simple file name matching. For example:
  * sequence/perspective/circle.txt
  * visibility/perspective/circle.txt

Additionally we provide a single ground truth frame, which you can use to compare to your 3D reconstruction. The frame index is given in the filename with 0-indexing. For example 'gt_frame_103.txt' would be the groundtruth for frame 103. This is given in the following format:

        __                  __
        | x_f0 x_f1 ... x_fP |
G   =   | y_f0 y_f1 ... y_fP |
        | z_f0 z_f1 ... z_fP |
        --                  --

If you notice any problems or have any suggestions, feel free to let us know using the following email: snje@dtu.dk.
