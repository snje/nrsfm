#!/usr/bin/env python
import numpy as np
import sys

from pykalman import KalmanFilter

if len(sys.argv) < 3:
    print "Please provide input and output path"
    sys.exit(-1)

data = np.loadtxt(sys.argv[1])

for i in range(data.shape[1]):
    for j in range(3):
        d = data[j::3, i]
        init = d[0]
        observations = d - init
        kf = KalmanFilter(
            transition_matrices=np.array([[1, 1], [0, 1]]),
            transition_covariance=0.01 * np.eye(2)
        )
        states_pred = kf.em(observations).smooth(observations)[0]
        data[j::3, i] = states_pred[:, 0] + init

np.savetxt(sys.argv[2], data)
