#!/usr/bin/env python
import numpy as np
import sys

if len(sys.argv) < 3:
    print "Please provide input and output path"
    sys.exit(-1)

in_path = sys.argv[1]
out_path = sys.argv[2]

A = np.loadtxt(in_path)
A = A.T
B = A.reshape(A.shape[0], -1, 3)
C = np.fliplr(B)
D = C.reshape(C.shape[0], -1)
D = D.T
np.savetxt(out_path, D)
