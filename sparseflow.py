#!/usr/bin/env python
from optparse import OptionParser
import numpy as np
import cv2
import os
import concurrent.futures as futures
import sys
import random

import structuredlight as stl
import util

parser = OptionParser()

parser.add_option(
    "-v", "--verbose", action = "store_true", dest="verbose",
    default = False, help="Makes program more talkative"
)
parser.add_option(
    "-o", "--output", dest = "output", help = "Sets output directory",
    metavar = "DIR"
)
parser.add_option(
    "-m", "--mask", dest = "mask",
    help = "file path for initialization mask in first frame", metavar = "FILE"
)
parser.add_option(
    "-d", "--debug", dest = "debug",
    help = "path to dump debug images", metavar = "FILE"
)

(options, args) = parser.parse_args()

if len(args) < 2:
    print "Please supply 2 images or more"
    sys.exit(1)

output = options.output or "."

def vprint(*farg):
  if options.verbose:
    for a in farg:
      print a,
    print ""

def update_progress(end_val, bar_length=20):
    if options.verbose:
        for i in xrange(end_val / 5 + 1):
            sys.stdout.write('\r')
        # the exact output you're looking for:
            sys.stdout.write("[%-20s] %d%%" % ('='*i, end_val))
            sys.stdout.flush()

# Now init the threadpool which is to handle threaded processing
loader = futures.ThreadPoolExecutor(max_workers = 1)


lk_params = dict( winSize  = (51, 51),
                  maxLevel = 21,
                  criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.001))

maxcorners = 1000
feature_params = dict( maxCorners = maxcorners,
                       qualityLevel = 0.1,
                       minDistance = 25,
                       blockSize = 15 )

# Repairs 3D images by filling NaNs using linear interpolation
# Interpolation is done on a pr. row basis
# This is due to the assumption of stereo rectification
def fillnan(im3d):
    mask = ~np.isnan(im3d[:, :, 0]).astype("uint8")
    kernel = np.ones((11,11), dtype = "uint8")
    mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)
    mask = mask == 255
    im3d[~mask, 0] = np.nan
    im3d[~mask, 1] = np.nan
    im3d[~mask, 2] = np.nan

    def fill(data):
        mask = np.isnan(data)
        if np.sum(~mask) > 0:
            data[mask] = np.interp(
                np.flatnonzero(mask), np.flatnonzero(~mask), data[~mask]
            )
        return data
    for r in xrange(im3d.shape[0]):
        x = im3d[r, :, 0]
        y = im3d[r, :, 1]
        z = im3d[r, :, 2]

        im3d[r, :, 0] = fill(x)
        im3d[r, :, 1] = fill(y)
        im3d[r, :, 2] = fill(z)
    for c in xrange(im3d.shape[1]):
        x = im3d[:, c, 0]
        y = im3d[:, c, 1]
        z = im3d[:, c, 2]

        im3d[:, c, 0] = fill(x)
        im3d[:, c, 1] = fill(y)
        im3d[:, c, 2] = fill(z)
    for i in xrange(20):
        im3d = cv2.medianBlur(im3d, 3)
    return im3d

def loadim(path, color = False):
    im3d, _, im = stl.im3dread(path)
    #util.plot_shared([im, im3d[:, :, 2]])
    #util.show()
    #im[:, :, 0] *= 2
    #im[:, :, 2] *= 2
    if not color:
        im = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
    nim3d = fillnan(im3d)
#    print path, np.sum(np.isnan(nim3d))
    return im, nim3d

#print(args[0])
#args = args[:10]
previm, prev3d = loadim(args[0])
#util.valueplot(im3d[1000:2000, 2000:2500, 2])
#util.plot_shared([previm, prev3d[:, :, 2]])
#util.show()
#frame = cv2.imread(args[-1])
#mask = np.zeros_like(frame)
color = np.random.randint(0,255,(maxcorners,3))
# calculate optical flow
fmask = None
if options.mask is not None:
    fmask = cv2.imread(options.mask, 0)

#util.plot_shared([previm, prev3d[:, :, 2], fmask])
#util.show()
p0 = cv2.goodFeaturesToTrack(previm, mask = fmask, **feature_params)
#print p0.shape
trace = p0
ip0 = p0.reshape(-1, 2).astype("int32")
trace3d = prev3d[ip0[:, 1], ip0[:, 0], :]

impromise = loader.submit(loadim, args[1])
vprint("Generating tracks")
update_progress(0)

for i in xrange(1, len(args)):
    nextim, next3d = impromise.result()
    if i + 1 < len(args):
        impromise = loader.submit(loadim, args[i + 1])
    #frame = cv2.imread(args[i])
    #mask = np.zeros_like(frame)
    p1, st, err = cv2.calcOpticalFlowPyrLK(previm, nextim, p0, None, **lk_params)
    good_new = p1[st==1]
    good_old = p0[st==1]
    #print trace[st == 1].shape, good_new.shape, p0.shape
    trace = np.concatenate((trace[st == 1], good_new), axis = 1)
    trace = trace.reshape(-1, 1, trace.shape[1])
    igood_new = good_new.reshape(-1, 2).astype("int32")
    #print st.shape, trace3d[st[:, 0] == 1, :].shape, next3d[igood_new[:, 1], igood_new[:, 0], :].shape
    trace3d = np.concatenate(
        (trace3d[st[:, 0] == 1], next3d[igood_new[:, 1], igood_new[:, 0], :]),
        axis = 1
    )
    #print trace.shape
    #print trace.shape
    previm = nextim
    prev3d = next3d
    p0 = good_new.reshape(-1,1,2)
    update_progress(i * 100 / (len(args) - 1))

vprint(" ")
trace = trace.reshape(trace.shape[0], trace.shape[2]).T
trace3d = trace3d.T
vprint("Obtained", trace.shape, trace3d.shape, "tracks")
vprint("Writing output to", output)
np.savetxt(output, trace3d, fmt = "%.7e")
#stl.cloud2ply("/tmp/tracks0.ply", trace3d[0:3, :].T)
if options.debug is not None:
    vprint("Writing debug images to", options.debug)
    update_progress(0)
    for i, path in enumerate(args):
        im, im3d = loadim(path, color = True)
        for j in xrange(trace.shape[1]):
            x = trace[i * 2, j]
            y = trace[i * 2 + 1, j]
            cv2.circle(im, (x, y), 8, color[j, :], 2)
        cv2.imwrite(options.debug + "/" + str(i) + ".png", im)
        update_progress(i * 100 / (len(args) - 1))
print ""
#for i, img in enumerate(imgs):
#    cv2.imwrite(output + "/" + str(i) + ".png", img)
