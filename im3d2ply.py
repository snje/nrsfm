#!/usr/bin/env python
import sys
import structuredlight as stl

if len(sys.argv) < 3:
    print "Please provide input and output"
    sys.exit(-1)

im3d, _, im = stl.im3dread(sys.argv[1])
stl.im3d2ply(sys.argv[2], im3d, im)
