#!/usr/bin/env python
import numpy as np
import sys
import structuredlight


arser = OptionParser()

parser.add_option(
    "-v", "--verbose", action = "store_true", dest="verbose",
    default = False, help="Makes program more talkative"
)
parser.add_option(
    "-o", "--output", dest = "output", help = "Sets output directory",
    metavar = "DIR"
)

(options, args) = parser.parse_args()
