import sys
import os
base_path = "./" + os.path.dirname(sys.argv[0])
r50path = base_path + "/../../branch/r50"
trunkpath = base_path + "/.."

sys.path.append(r50path + "/control")
sys.path.append(trunkpath + "/utilities")
sys.path.append(trunkpath + "/analysis")
print trunkpath
print r50path
import abb
import light
import projector as projector_lib
import camera
import camerautils
import GrayCodeGenerator as GCG
import robotpath as robotpath_lib
import transformations as tr
from subprocess import Popen, PIPE
import time
import json
import math
import numpy as np
import cv2
import structuredlight as stl_generator

# Generate GrayCode patterns
patterns = GCG.make_sequence(cols=1024, rows=800)
#for noise_level in [0.1, 0.5, 1, 2, 5]:
tmp_whitenoise = np.random.normal(0.5, 5.0, (800,1280))*255
patterns.append(np.dstack((tmp_whitenoise, tmp_whitenoise, tmp_whitenoise)))


pattern_dir = "{}/patterns".format("/home/mebd/thoso_test")
if not os.path.exists(pattern_dir):
    os.makedirs(pattern_dir)
    print("Created directory: {}".format(pattern_dir))

for index, image in enumerate(patterns):
    cv2.imwrite("{}/pattern_{}.png".format(pattern_dir, index), image)
