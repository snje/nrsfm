#!/usr/bin/env python
import sys
import os
base_path = "./" + os.path.dirname(sys.argv[0])
r50path = base_path + "/../../branch/r50"
trunkpath = base_path + "/.."

sys.path.append(r50path + "/control")
sys.path.append(trunkpath + "/utilities")
sys.path.append(trunkpath + "/analysis")
print trunkpath
print r50path
import abb
import light
import projector as projector_lib
import camera
import camerautils
import GrayCodeGenerator as GCG
import robotpath as robotpath_lib
import transformations as tr
from subprocess import Popen, PIPE
import time
import json
import math
import numpy as np
import cv2
import serial
import structuredlight as stl_generator


camera_left_serial  = 14194507
camera_right_serial = 14253982

positions = [
[[1150.39, -259.53, 636.13], [0.275, 0.59, 0.701, -0.29]],
[[889.55, -421.44, 273.37], [0.121, 0.739, 0.654, -0.111]],
[[479.69, -374.32, 337.44], [0.07, 0.877, 0.446, -0.165]]
]
base_dir = "/home/snje/nrsfm/pind"
if not os.path.exists(base_dir):
    os.makedirs(base_dir)
    print("Test directory has been created at: \n\t{}".format(base_dir))

camera_path = r50path+ "/drivers/CameraDaemon/CameraDaemon"

robot = None
process_cameraL = None
process_cameraR = None
camera_left = None
camera_right = None
# Use try and finally to make proper cleanup.
try:
    #arduino = serial.Serial('/dev/ttyACM0')


    leftlog = open(base_dir + "/leftlog.txt", "w")
    rightlog = open(base_dir + "/rightlog.txt", "w")

    # Hook up to cameras
    print("Starting camera processes.")
    process_cameraL = Popen([camera_path, "1337"], stdout=leftlog, stderr=leftlog)
    process_cameraR = Popen([camera_path, "1338"], stdout=rightlog, stderr=rightlog)
    print("Processes has been started and logs are found in: \n\t{}".format(base_dir))

    ##
    ## ----- Basic Connections ----- ##
    ##
    # Connecting to robot
    robot = abb.Robot(ip='192.168.125.1')
    # Setting speed to avoid any damages.
    robot.set_speed(speed=[100, 50, 50, 50])
    print "Connected to Robot!"
    # [0.6750092, 0.0064189, 0.7377480, -0.00701555]
    #rTool=[[128, -3, 145], tr.quaternion_from_euler(np.deg2rad(0),np.deg2rad(0),np.deg2rad(0))];
    #robot.set_tool(rTool)
    # Connecting to light
    light = light.Light()
    # Turn off lights
    light.all_on()
    print "Connected to Light!"

    # Setting the connection to the daemons
    camera_left = camera.Camera(port=1337, url="tcp://localhost")
    camera_right = camera.Camera(port=1338, url="tcp://localhost")

    # Inform the Daemons which camera to connect to.
    camera_left.connect(camera_left_serial)
    camera_right.connect(camera_right_serial)

    # Initialize the cameras, this puts camera in a "default" setting.
    camera_left.init()
    camera_right.init()

    # Setting gain and shutter
    camera_left.set_gain(0)
    camera_right.set_gain(0)
    camera_left.set_shuttertime(100)
    camera_right.set_shuttertime(100)

    #
    projector = projector_lib.Projector(screen=1)
    print("Connected to projector!")

    # Generate GrayCode patterns
    # patterns = GCG.make_sequence(cols=1024, rows=1)
    patterns = stl_generator.phaseshiftsequence(1280, 16, 9)
    gpat = GCG.make_sequence(cols=1024, rows=1)
    # patterns.append(np.dstack((tmp_whitenoise, tmp_whitenoise, tmp_whitenoise)))
    # 41731
    print("Made phase stuffy")
    for index, image in enumerate(patterns):
        cv2.imwrite(base_dir + "/pattern_{}.png".format(index), image)
    num_of_pos = len(positions)
    for pose_index in range(1):
        print("Going to pos {} of {}".format(pose_index, num_of_pos))
        for index, pos in enumerate(positions):
            # Move Robot to position
            robot.set_cartesian(pos)
            time.sleep(1)

            pos_dir = "{}/pose_{}/pos_{}".format(base_dir, pose_index, index)
            # Make directory if not existing
            if not os.path.exists(pos_dir):
                os.makedirs(pos_dir)
                print("Created directory: {}".format(pos_dir))
            # Make directory if not existing
            left_dir = "{}/left".format(pos_dir)
            if not os.path.exists(left_dir):
                os.makedirs(left_dir)
                print("Created directory: {}".format(left_dir))
            right_dir = "{}/right".format(pos_dir)
            # Make directory if not existing
            if not os.path.exists(right_dir):
                os.makedirs(right_dir)
                print("Created directory: {}".format(right_dir))

            light.all_off()
            projector.black_screen()
            seq = camerautils.shoot_stl(camera_left, camera_right, projector, patterns)
            for index, image in enumerate(seq.left):
                cv2.imwrite("{}/left/IMG_{}_PS.png".format(pos_dir, index), image)
            for index, image in enumerate(seq.right):
                cv2.imwrite("{}/right/IMG_{}_PS.png".format(pos_dir, index), image)
            seq = camerautils.shoot_stl(camera_left, camera_right, projector, gpat)
            for index, image in enumerate(seq.left):
                cv2.imwrite("{}/left/IMG_{}_GRAY.png".format(pos_dir, index), image)
            for index, image in enumerate(seq.right):
                cv2.imwrite("{}/right/IMG_{}_GRAY.png".format(pos_dir, index), image)
            print robot.get_cartesian()
            projector.black_screen()
            light.all_on()
            time.sleep(1)
            cv2.imwrite("{}/right/LIGHT_{}.png".format(pos_dir, index), camera_right.single_shot())
            cv2.imwrite("{}/left/LIGHT_{}.png".format(pos_dir, index), camera_left.single_shot())

        #arduino.write(b'1') # forwards
        #arduino.write(b'1') # forwards
        #arduino.write(b'1') # forwards
        #arduino.write(b'1') # forwards
        #arduino.write(b'1') # forwards
        #arduino.write(b's')
        # arduino.write(b'0) for backwards
        # arduino.write(b's') to remove power over coils.
        # If coils are shorted for too long it will catch fire.

finally:
    # Sleeps to make sure that everything is done with its thing.
    time.sleep(5)
    if(robot is not None):
        robot.close()
    if(camera_left is not None):
        camera_left.close()
        time.sleep(1)
    if(camera_right is not None):
        camera_right.close()
        time.sleep(1)
    if(process_cameraL is not None):
        process_cameraL.kill()
    if(process_cameraR is not None):
        process_cameraR.kill()
    print "Done!\n"
