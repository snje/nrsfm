#!/usr/bin/env python

import numpy as np
import sys

if len(sys.argv) < 4:
    print "Please provide path number, path file and output path"
    sys.exit(-1)

path_num = sys.argv[1]
in_path = sys.argv[2]
out_path = sys.argv[3]

all_path = np.load(in_path)
if in_path.shape[1] <= path_num:
    print "Pathing number exceeded: {0} <= {1}".format(in_path.shape[1], path_num)
    sys.exit(-2)

sub_path = in_path[:, path_num, :]
sub_path = sub_path.reshape(sub_path[0], 1, 7)
np.save(out_path, sub_path)
