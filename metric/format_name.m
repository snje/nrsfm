function [ fname ] = format_name( name )
    if iscell(name)
       [r, c] = size(name);
       fname = cell(r, c);
       for i = 1:r
           for j = 1:c
               fname{i, j} = format_name(name{i, j});
           end
       end
       return
    end
    
    fname = name;
    fname = strrep(fname, 'X1=', '');
    fname = strrep(fname, '.csv', '');
    fname = strrep(fname, 'BALM', '\\cite{balm2010}');
    fname = strrep(fname, 'CSF2', '\\cite{GotardoCVPR2011}');
    fname = strrep(fname, 'CSF', '\\cite{GotardoPAMI2011}');
    fname = strrep(fname, 'Concensus', '\\cite{concensus2016}');
    fname = strrep(fname, 'EM PPCA', '\\cite{TorresaniNIPS:2003}');
    fname = strrep(fname, 'KSTA', '\\cite{GotardoICCV2011}');
    fname = strrep(fname, 'MDH', '\\cite{MDH2016}');
    fname = strrep(fname, 'SPFM', '\\cite{Dai2012}');
    fname = strrep(fname, 'PTA', '\\cite{AkhterPAMI2011}');
    fname = strrep(fname, 'RIKS', '\\cite{gotardo:ECCV2012}');
    fname = strrep(fname, 'SoftInext', '\\cite{Vicente:etal:2012}');
    fname = strrep(fname, 'Bundle', '\\cite{DelBue:etal:IVC2007}');
    fname = strrep(fname, 'Compressible', '\\cite{compressible2016}');
    fname = strrep(fname, 'RigidTriangle', '\\cite{TaylorCVPR2010}');
    fname = strrep(fname, 'MetricProj', '\\cite{PaladiniCVPR2009}');
    fname = strrep(fname, 'MultiBody', '\\cite{MultiBody2017}');
    fname = strrep(fname, 'ScalableSurface', '\\cite{scalesurface2017}');
end

