clear all;
close all;

%load('/media/eco3d/nrsfm/challenge/evaluation.mat');
addpath('./RANSAC-Toolbox/')
SetPathLocal('./RANSAC-Toolbox')

%ort_flag = strcmp(score(:, 4), 'orthogonal') .* ~strcmp(score(:, 3), 'Spatial-temporal-Multi-body-NRSFM.csv');
%ort_flag = logical(ort_flag);
%per_flag = strcmp(score(:, 4), 'perspective');

%scatter_mat = cell2mat([score(ort_flag, 1) score(per_flag, 1)]);
%valid = sum(~isnan(scatter_mat), 2) == 2;

anova_stuff;

key_full = strcat(sf(:, 3), sf(:, 4), sf(:, 5), sf(:, 6));
key_miss = strcat(sm(:, 3), sm(:, 4), sm(:, 5), sm(:, 6));

x = [];
y = [];
algoname = [];
for i = 1:length(key_full)
   kf = key_full{i};
   km = find(strcmp(kf, key_miss));
   key = sf(i, 3);
   %kf
   %key_miss{km}
   if length(km) == 1
       %sf{i, 1} - sm{km, 1}
       x = [x; sf{i, 1}];
       y = [y; sm{km, 1}];
       algoname = [algoname; key];
   end
end

valid = ~isnan(x) & ~isnan(y);

x = x(valid);
y = y(valid);
algoname = algoname(valid);

f = figure;
ua = unique(algoname);
for l = 1:length(ua)
    k = ua{l};
    v = strcmp(algoname, k);
    %hold on;
    scatter(x(v), y(v));
end
xlabel('Full (mm)');
ylabel('Missing Data (mm)');
title('Reconstruction Error');

%x = scatter_mat(valid, 1);
%y = scatter_mat(valid, 2);

p = polyfit(x, y, 1);
save_pdf('build/miss_data.pdf', f);

% set RANSAC options
options.epsilon = 1e-6;
options.P_inlier = 0.95;
options.sigma = 1;
options.est_fun = @estimate_line;
options.man_fun = @error_line;
options.mode = 'MSAC';
options.Ps = [];
options.notify_iters = [];
options.min_iters = 300;
options.fix_seed = false;
options.reestimate = true;
options.stabilize = false;

[results, options] = RANSAC([x y]', options);

ind = results.CS;

f = figure;
ms = 10;
%plot(x(ind), y(ind), '.b', 'MarkerEdgeColor', [0.33, 0.75, 0.41], 'MarkerSize', ms);
%hold on;
%plot(x(~ind), y(~ind), '.r','MarkerEdgeColor', [0.9, 0.31, 0.32], 'MarkerSize', ms);
scatter(x, y);
%p = polyfit(x(ind), y(ind), 1);
p = [-results.Theta(1) / results.Theta(2) -results.Theta(3) / results.Theta(2)];
hold on;
plot(x(ind), polyval(p, x(ind)), 'color', [0.9, 0.31, 0.32]);%0.5 * [0.33, 0.75, 0.41]);

pbaspect([1 1 1])
set(gca, 'box', 'off')
xlabel('Full (mm)');
ylabel('Missing Data (mm)');
title('Reconstruction Error');
legend('Experiments', sprintf('y = %.2fx + %.2f', p(1), p(2)), 'Location', 'northwest');
%savefig(f, '/tmp/test.pdf');
save_pdf('build/miss_linear.pdf', f);

ua = unique(algoname);
ua_fmt = web_format_name(ua);
for l = 1:length(ua)
    f = figure;
    pbaspect([1 1 1])
    plot(x(ind), polyval(p, x(ind)), 'color', 0.5 * [0.33, 0.75, 0.41]);
    k = ua{l};
    v = strcmp(algoname, k);
    hold on;
    scatter(x(v), y(v));
    name = ua_fmt{l};
    title(ua_fmt{l});
    xlabel('Full (mm)');
    ylabel('Missing Data (mm)');
    save_pdf(sprintf('build/md_%s.pdf', name), f);
end

%f = figure;
%boxplot(cell2mat(score(:, 1)), regexprep(score(:, 4),'(\<\w)','${upper($1)}'), 'Notch', 'on');
%[p, tbl, stats] = anova1([x y], {}, 'off');
%save_pdf(f, 'build/proj_boxplot.pdf');