anova_stuff;

[cmpstat, m, h, gnames] = multcompare(stat, 'CType', 'bonferroni', 'Dimension', [1]);
title('Algorithm')
m = m(:, 1);

[m, I] = sort(m);

gnames = gnames(I);
gnames = strrep(gnames, 'Algorithm=', '');

print_table([web_format_name(gnames)' format_name(gnames) num2cell(m)], {'%s', '%s', '%.2fmm'}, {'Method', 'Citation', 'Mean Error'}, 'printMode', 'latex');
