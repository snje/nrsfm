function [d, outliers, Dt] = truncated_similarity(X, Q)
    point_count = size(X, 1) * size(X, 2) / 3;
    dx = X(1:3:end, :) - Q(1:3:end, :);
    dy = X(2:3:end, :) - Q(2:3:end, :);
    dz = X(3:3:end, :) - Q(3:3:end, :);
    D = dx .* dx + dy .* dy + dz .* dz;
    [Dt, outliers] = box_truncate(D);
    d = sqrt(mean(Dt(:)));
    Dt = sqrt(Dt);
end
