%recon_path = '~/Dropbox/DTU_NRSFM_evaluation/reconstruction/';
%occ_path = '~/Dropbox/DTU_NRSFM_evaluation/reconstruction_occlusion/';
recon_path = '/dtu-compute/eco3d/nrsfm/challenge/full';
occ_path = '/dtu-compute/eco3d/nrsfm/challenge/missing_data';
gt_path = '~/repos/nrsfm/analysis/groundtruth/'
threads = 15;

score = do_evaluation(gt_path, recon_path, @metric, threads);
score_uncor = do_evaluation(gt_path, recon_path, @nrsfm_score, threads);
md_score = do_evaluation(gt_path, occ_path, @metric, threads);
md_score_uncor = do_evaluation(gt_path, occ_path, @nrsfm_score, threads);

save('evaluation.mat', 'score', 'score_uncor', 'md_score', 'md_score_uncor');
