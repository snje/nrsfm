function [d, Xa, outliers, Dt] = metric(X, Q)
    if ~all(size(X) == size(Q))
       [d, Xa, outliers, Dt] = nrsfm_score(X, Q);
       return
    end
    Qr = flatten(Q);
    Xr = flatten(X);
    [~, ~, T] = procrustes(Qr', Xr');
    options = optimoptions(@lsqnonlin,'Algorithm','levenberg-marquardt');
    % Lol hack
    Xr = X;
    Qr = Q;
    f = @(p) objective(p, Xr, Qr);
    p0 = [T.b reshape(T.T', 1, []) T.c(1, :)];
    p = lsqnonlin(f, p0, [], [], options);
    [d, outliers, Dt] = f(p);
    Xa = transform(p, Xr);
    %Xa = reformat(transform(p, Xr), F, P);
    %outliers = reshape(outliers, F / 3, P);
    %sum(sum(outliers))
end

function [s, R, t] = procrustes_guess(X, Q)
    Qr = flatten(Q);
    Xr = flatten(X);
    [~, ~, T] = procrustes(Qr', Xr');
    s = T.b;
    R = T.T';
    t = T.c(1, :);
end

function [s, R, t] = icp_guess(X, Q)
    Qr = flatten(Q);
    Xr = flatten(X);
    Qr = Qr - repmat(mean(Qr, 2), 1, size(Qr, 2));
    Xr = Xr - repmat(mean(Xr, 2), 1, size(Xr, 2));
    scale = @(S) sqrt(sum(S(1:3:end).^2 + S(2:3:end).^2 + S(3:3:end).^2)) / size(S, 2);
    s = scale(Qr) / scale(Xr);
    [R, t] = icp(Qr, scale * Xr);
end

function [Xf] = reformat(X, F, P)
    Xf = zeros(F, P);
    for i = 1:3
        Xf(i:3:end, :) = reshape(X(i, :), F / 3, P);
    end
end

function [d, outliers, Dt] = objective(p, Xr, Qr)
    Xt = transform(p, Xr);
    %consider addding |ln(R)| to objective
    [d, outliers, Dt] = truncated_similarity(Xt, Qr);
end
