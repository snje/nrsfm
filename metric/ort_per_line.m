clear all;
close all;


anova_stuff;

ort_sf = sf(strcmp(sf(:, 4), 'orthogonal'), :);
per_sf = sf(strcmp(sf(:, 4), 'perspective'), :);

key_ort = strcat(ort_sf(:, 3), ort_sf(:, 5), ort_sf(:, 6));
key_per = strcat(per_sf(:, 3), per_sf(:, 5), per_sf(:, 6));

x = [];
y = [];
for i = 1:length(key_ort)
   kf = key_ort{i};
   km = find(strcmp(kf, key_per));
   %kf
   %key_miss{km}
   if length(km) == 1
       %sf{i, 1} - sm{km, 1}
       x = [x; ort_sf{i, 1}];
       y = [y; per_sf{km, 1}];
   end
end


valid = ~isnan(x) & ~isnan(y);

x = x(valid);
y = y(valid);

f = figure; 
scatter(x, y);
xlabel('Orthogonal (mm)');
ylabel('Perspective (mm)');
title('Reconstruction Error');
pbaspect([1 1 1])
save_pdf('build/proj_data.pdf', f);

%load('/media/eco3d/nrsfm/challenge/evaluation.mat');
addpath('./RANSAC-Toolbox/')
SetPathLocal('./RANSAC-Toolbox')

%p = polyfit(x, y, 1);

% set RANSAC options
options.epsilon = 1e-6;
options.P_inlier = 0.99;
options.sigma = 1;
options.est_fun = @estimate_line;
options.man_fun = @error_line;
options.mode = 'MSAC';
options.Ps = [];
options.notify_iters = [];
options.min_iters = 100;
options.fix_seed = false;
options.reestimate = true;
options.stabilize = false;

[results, options] = RANSAC([x y]', options);

ind = results.CS;

f = figure;
ms = 10;
%plot(x(ind), y(ind), '.b', 'MarkerEdgeColor', [0.33, 0.75, 0.41], 'MarkerSize', ms);
%hold on;
%plot(x(~ind), y(~ind), '.r','MarkerEdgeColor', [0.9, 0.31, 0.32], 'MarkerSize', ms);
scatter(x, y);
%p = polyfit(x(ind), y(ind), 1);
p = [-results.Theta(1) / results.Theta(2) -results.Theta(3) / results.Theta(2)];
hold on;
plot(x(ind), polyval(p, x(ind)), 'color', [0.9, 0.31, 0.32]);%0.5 * [0.33, 0.75, 0.41]);

pbaspect([1 1 1])
set(gca, 'box', 'off')
xlabel('Orthogonal (mm)');
ylabel('Perspective (mm)');
title('Reconstruction Error');
legend('Reconstruction Error', sprintf('y = %.2fx + %.2f', p(1), p(2)), 'Location', 'northwest');
%savefig(f, '/tmp/test.pdf');
save_pdf('build/proj_linear.pdf', f);

%f = figure;
%boxplot(cell2mat(score(:, 1)), regexprep(score(:, 4),'(\<\w)','${upper($1)}'), 'Notch', 'on');
%[p, tbl, stats] = anova1([x y], {}, 'off');
%save_pdf(f, 'build/proj_boxplot.pdf');