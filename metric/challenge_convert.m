function [] = challenge_convert( input, output )
    %[~, files] = unix('find /home/snje/Data/Spatial-temporal\ Multi-body-NRSFM/ -name "*.txt"');
    [~, files] = unix(sprintf('find %s -name "*.txt"', input));
    files = strsplit(files, '\n');
    for i = 1:numel(files) - 1
        path = strrep(files{i}, ' ', '\ ');
        try
            X = dlmread(files{i});
            p = fullfile(output, convert_path(files{i}));
            d = fileparts(p);
            unix(sprintf('mkdir -p %s', d));
            %sprintf('mkdir -p %s', d)
            unix(sprintf('cp %s %s', path, p));
            %sprintf('cp %s %s', path, p)
        catch
            sprintf('failed to load file: %s', path)
    end

end

