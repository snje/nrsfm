function [ score_table ] = do_evaluation(groundtruth_dir, reconstruction_dir, eval, threads)
    if nargin < 3
        eval = @metric;
    end
    if nargin < 4
        threads = 3;
    end
    % Tearing
    gt_paths = struct(...
        'tearing', fullfile(groundtruth_dir, 'tearing.txt'),...
        'articulated', fullfile(groundtruth_dir, 'articulated.txt'),...
        'balloon', fullfile(groundtruth_dir, 'balloon.txt'),...
        'paper', fullfile(groundtruth_dir, 'paper.txt'),...
        'stretch', fullfile(groundtruth_dir, 'stretch.txt')...
    );
    score_table = cell(0);
    % Search for entries
    pool = parpool(threads);

    names = fieldnames(gt_paths);
    for shape_key = {names{:}}
        Q = dlmread(gt_paths.(shape_key{1}));
        for proj_key = {'orthogonal', 'perspective'}
            shape_path = fullfile(reconstruction_dir, proj_key{1}, shape_key{1});
            cam_path_dirs = dir(fullfile(shape_path, '/*'));
            for i = 3:size(cam_path_dirs, 1)
               path_type = cam_path_dirs(i).name;
               recon_path = fullfile(shape_path, path_type);

               paths = fullfile(recon_path, '/*.csv');
               files = dir(paths);
               file_count = size(files, 1);

               parfor j = 1:file_count
                   X = load_shape(recon_path, files(j).name);
                   valid = ~any(any(isnan(X)));
                   s = NaN;
                   o = NaN;
                   Dt = NaN;
                   if valid
                    [s, ~, o, Dt] = eval(X, Q);
                   end
                   % Should be score :: outliers :: algorithm :: projection :: scene :: camera
                   output = {s, sum(sum(o)), files(j).name, proj_key{1}, shape_key{1}, cam_path_dirs(i).name, Dt};
                   score_table = [score_table; output];
               end
               load(fullfile(recon_path, 'stats.mat'));
               fail_names = fieldnames(failure);
               for j = 1:numel(fail_names)
                  fn = fail_names(i);
                  if failure.(fn{1})
                      output = {NaN, NaN, fn{1}, proj_key{1}, shape_key{1}, cam_path_dirs(i).name};
                      score_table = [score_table; output];
                  end
               end
            end
        end
    end

    delete(pool);
end

function [shape] = load_shape(input_path, name)
path = sprintf('%s/%s', input_path, name);
shape = dlmread(path);
end
