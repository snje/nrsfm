function [ reconstruction, projection_tag, camera_tag, missing_tag ] = nrsfm_dataset_reconstruction(...
    data_path, output_path, reconst, do_missing_data ...
)
    % Utility function that facilitates running your algorithm on the NRSfM
    % data set. It also exports the reconstructions in the file structure
    % specified on the website http://nrsfm2017.compute.dtu.dk/dataset.
    % Arguments are as follows:
    %
    % data_path:     string: Base path for extracted data set.
    % output_path:   string: Path to the root of the output file structure
    %                        If it doesn't exist, it will be created
    %                        recursively
    % reconst:     function: Your function for reconstruction.
    %                        It should accept two matrices as arguments:
    %                        the first being the 2D traces as a 2FxP matrix
    %                        the second being a 2FxP visibility matrix with
    %                        booleans showing whether a point is visible,
    %                        Refer to the included dataset README for
    %                        additional formatting information.
    %                        If your algorithm needs additional arguments,
    %                        you can pass that via a lambda function e.g.
    %                        reconst = @(W, MD) your_algorithm(W, MD, a, b)
    % do_missing_data: bool: Put 1 if script should run your algorithm
    %                        with missing data.
    %
    % reconstruction:  cell: A list of the estimated 3D reconstructions.
    % projection_tag:  cell: A list of strings indicating which type of
    %                        projection the reconstruction was done with.
    %                        E.g. 'orthogonal'
    % camera_tag:      cell: A list of strings indicating which type of
    %                        camera path the reconstruction was done with.
    %                        E.h. 'zigzag'
    % missing_tag:     cell: A boolean list of tags indicating whether a
    %                        reconstruction was done with missing data.
    %
    % So an example of usage would be:
    % a = 1;
    % b = 22;
    % recon = @(W, MD) your_algorithm(W, MD, a, b);
    % shapes = nrsfm_dataset_reconstruction(...
    %       '/path/to/data', '/path/to/output', recon ...
    % )
    %
    % If you have any comments, suggestions or errors please contact snje@dtu.dk

    % Set default parameter for missing data if not specified
    if (~exist('do_missing_data', 'var'))
       do_missing_data = true; 
    end

    % Camera path naming
    camera = {'circle', 'flyby', 'line', 'semi_circle', 'tricky', 'zigzag'};

    % Initialize the output parameters
    reconstruction = {};
    projection_tag = {};
    camera_tag = {};
    missing_tag = {};
    
    % Iterate over the combinations of projection types and camera paths
    for proj = {'perspective', 'orthogonal'} 
        for i = 1:numel(camera)
            % Synthesize paths
            camera_ext = sprintf('%s.txt', camera{i});
            seq_path = fullfile(data_path, 'sequence', proj, camera_ext);
            vis_path = fullfile(data_path, 'visibility', proj, camera_ext);
            % load 2D sequences and visibility matrices
            W = dlmread(seq_path{1});
            MD = dlmread(vis_path{1});
            % Create matrix symbolizing all points being available
            none_MD = ones(size(W));
            %%%%%%%%%%%%%% Your NRSfM function  %%%%%%%%%%%%%%
            none_S = reconst(W, none_MD);
            % Save file to disk
            export_recon(fullfile(output_path, 'full'), proj, camera{i}, none_S);
            % Added entry into output listing
            reconstruction = [reconstruction; none_S];
            projection_tag = [projection_tag; proj{1}];
            camera_tag = [camera_tag; camera{i}];
            missing_tag = [missing_tag; false];
            
            % Execute missing data branch if requested
            if do_missing_data
                %%%%%%%%%%%%%% Your NRSfM function  %%%%%%%%%%%%%%
                md_S = reconst(W, MD);
                % Export to disk
                export_recon(fullfile(output_path, 'missing_data'), proj, camera{i}, md_S);
                % Add to output lists
                reconstruction = [reconstruction; md_S];
                projection_tag = [projection_tag; proj{1}];
                camera_tag = [camera_tag; camera{i}];
                missing_tag = [missing_tag; true];
            end
        end
    end

end

function export_recon(root, proj, camera, S)
    dir_path = fullfile(root, proj);
    mkdir(dir_path{1});
    camera_ext = sprintf('%s.txt', camera);
    path = fullfile(dir_path, camera_ext);
    dlmwrite(path{1}, S, 'delimiter', ' ', 'precision', '%.7f');
end