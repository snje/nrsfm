close all;
clear all;

load('/home/snje/Data/evaluation_v2.mat');
f = error_heatmap(score, 'tearing', 'frame', 300);
save_pdf('./build/err_tear.pdf', f);
save_pdf('./build/err_paper.pdf', error_heatmap(score, 'paper'));
save_pdf('./build/err_balloon.pdf', error_heatmap(score, 'balloon'));
save_pdf('./build/err_art.pdf', error_heatmap(score, 'articulated'));
save_pdf('./build/err_stretch.pdf', error_heatmap(score, 'stretch'));