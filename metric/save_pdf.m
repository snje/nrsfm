function [ ] = save_pdf( path, f )
set(f,'Units','Inches');

pos = get(f,'Position');

set(f,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
print(f, path,'-dpdf','-r2')


end

