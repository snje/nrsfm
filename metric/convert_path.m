function [ path_out ] = convert_path(path_in)
    p = path_in;
    [p, camera] = fileparts(p);
    [p, proj] = fileparts(p);
    [p, scene] = fileparts(p);
    [p, MD] = fileparts(p);
    [p, algo] = fileparts(p);
    
    path_out = fullfile(MD, proj, scene, camera, sprintf('%s.csv', algo));
    path_out = strrep(path_out, ' ', '-');
end

