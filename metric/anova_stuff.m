clear all;
%close all;

load('/home/snje/Data/evaluation.mat');
addpath('/repositories/nrsfm_website/script/');



sf = [score repmat({0}, [length(score), 1])];
sm = md_score;
md = {};

cache = struct();
for i = 1:length(sm)
    path = sprintf('/repositories/nrsfm/analysis/visibility/%s/%s/%s.txt', sm{i, 4}, sm{i, 5}, sm{i, 6});
    key = sprintf('%s_%s_%s', sm{i, 4}, sm{i, 5}, sm{i, 6});
    %key = strrep(strrep(path, '/', '_'), '.', '_');
    if ~isfield(cache, key)
       cache.(key) =  logical(dlmread(path));
    end
    MD = cache.(key);
    %MD = logical(dlmread(path));
    [w, h] = size(MD);
    
    md = [md; 1];
end

fnames = fieldnames(cache);
mddist = [];
for i = 1:length(fnames)
   MD = cache.(fnames{i}); 
   mddist = [mddist; sum(sum(MD)) / (w * h)];
end
f = figure;
histogram(mddist * 100, 0:10:100);
title('Missing Data Distribution');
xlabel('Missing Data Percentile');
ylabel('Sequence Count');
set(gca, 'XTick', 0:10:100)
save_pdf('build/md_histo.pdf', f);

sm = [md_score md];

%s = md_score_uncor;
s = [sf;sm];
%s = sf;
%s = sm;


measure = cell2mat(s(:, 1));

%algo = s(:, 3);
algo = web_format_name(s(:, 3));
proj = s(:, 4);
scene = s(:, 5);
scene = strrep(scene, 'balloon', 'Deflation');
scene = strrep(scene, 'paper', 'Bending');
scene = strrep(scene, 'stretch', 'stretching');
path = strrep(s(:, 6), 'semi_circle', 'Half circle');
miss = s(:, 7);

scene = regexprep(scene,'(\<\w)','${upper($1)}');
path = regexprep(path,'(\<\w)','${upper($1)}');

%[p_algo, tbl_algo, stat_algo] = anova1(measure, algo);
%[p_sc, tbl_sc, stat_sc] = anova1(measure, scene);
%[p_p, tbl_p, stat_p] = anova1(measure, path);
[p, tbl, stat] = anovan(measure, {algo, proj, scene, path, cell2mat(miss)}, 'display', 'on', 'varnames', {'Algorithm', 'Camera Model', 'Scene', 'Camera Path', 'Missing Data'});
%[p, tbl, stat] = anovan(measure, {algo, proj, scene, path}, 'display', 'on', 'varnames', {'Algorithm', 'Camera Model', 'Scene', 'Camera Path'}, 'model', 'interaction');
%[p, tbl, stat] = anovan(...
%    measure, {algo, proj, scene, path, cell2mat(miss)}, 'display', 'on', ...
%   'continuous', [5], 'varnames', {'Algorithm', 'Projection', 'Deformation', 'Camera Path', 'Missing Data'}...
%   );


%figure;
%scatter(x, y);
%[proj, m, h, gnames] = multcompare(stat, 'CType', 'bonferroni', 'Dimension', [1]);
%figure;
%boxplot(cell2mat(score(:, 1)), regexprep(score(:, 5),'(\<\w)','${upper($1)}'), 'Notch', 'on');
%set(gca, 'box', 'off')
%figure;
%boxplot(cell2mat(md_score(:, 1)), regexprep(md_score(:, 5),'(\<\w)','${upper($1)}'), 'Notch', 'on');

%figure;
%boxplot(cell2mat(score(:, 1)), regexprep(score(:, 6),'(\<\w)','${upper($1)}'), 'Notch', 'on');
%set(gca, 'box', 'off')