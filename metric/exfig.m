%% exfig - export a figure with properties
% 
% A modern replacement for the aging exportfig()!
%
% Example: 
%   exfig(gcf, 'file.eps', 'Width', 12, 'Height', 8, 'Units', 'centimeters');
%
%
% Parameters:
%
%   Units:       {'centimeters'} | 'mm' | 'inches'
%                The unit in which the parameters Width and Height are
%                measured.
%
%   Width:       Default: 10
%                The width of the output image.
%
%   Height:      Default: 8
%                The height of the output image.
%
%   FontUnits:   {'points'} | 'normalized' | 'inches' | 'centimeters' | 'pixels'
%                Units in which the parameter FontSize is measured.
%
%   FontSize:    Default: 8
%                If set, Fontsize used on all labels.
%
%   Format:      Default: 'epsc2'
%                Format of the output image. See "MATLAB > Functions >
%                Graphics > Printing > print > Graphics Format Files" for a 
%                list of available formats. 
%
%   Interpreter: 'tex' | 'latex' | 'none'
%                The interpreter used to typeset all labels.
%
%   Tight:       {false} | true
%                If true, most of the white border will be removed by means
%                of shrinking the figures bounding box. The resulting size
%                will differ from what was set using the Width and Height
%                parameters.
%
%   PSfrag:      {false} | true
%                If true, a *.tex file will be written for use with the PSfrag 
%                package to replace all text labels with LaTeX interpreted labels.
%                Sets the Interpreter to 'none'.
%
%   Renderer:    'painters' | 'opengl' | 'zbuffer'
%                Renderer used for the output figure. If unset, a renderer
%                will automatically be selected, which is not necessarily
%                the on-screen renderer.
%
%   Resolution:  Default: '150'
%                Resolution of the output figure.
%
% Jakob Wilm, DTU, 2012

function exfig(h, fName, varargin)

% Terminate if global EXFIG variable is false
global EXFIG
if EXFIG == false
    return;
end

% Use the inputParser class to validate input arguments.
inp = inputParser;

inp.addRequired('h', @ishandle);
inp.addRequired('fName', @ischar);

validUnits = {'centimeters', 'mm', 'inches'};
inp.addOptional('Units', 'centimeters', @(x)any(validatestring(x,validUnits)));

inp.addOptional('Width', 10, @(x)isreal(x) && x > 0);
inp.addOptional('Height', 8, @(x)isreal(x) && x > 0);

validFontUnits = {'points', 'normalized', 'inches', 'centimeters', 'pixels'};
inp.addOptional('FontUnits', 'points', @(x)any(validatestring(x,validFontUnits)));

inp.addOptional('FontSize', 8, @(x)isreal(x) && x > 0);

inp.addOptional('Format', 'epsc2', @ischar);

inp.addOptional('Interpreter', [], @ischar);

inp.addOptional('Tight', false, @islogical);

inp.addOptional('PSfrag', false, @islogical);

validRenderers = {'opengl', 'painters', 'zbuffer'};
inp.addOptional('Renderer', [], @(x)any(validatestring(x,validRenderers)));

inp.addOptional('Resolution', '150', @ischar);


% Parse input parameters
inp.parse(h,fName,varargin{:});
arg = inp.Results;

% Copy the graphics object
h2 = copyobj(h, 0);

% Set figure size properties
set(h2, 'PaperUnits', arg.Units);
set(h2, 'PaperPosition', [0 0 arg.Width arg.Height]);
set(h2, 'PaperSize', [arg.Width arg.Height]);

% Set font sizes
if(not(isempty(arg.FontSize)))
    set(findall(h2, '-property', 'FontUnits'), 'FontUnits', arg.FontUnits);
    set(findall(h2, '-property', 'FontSize'), 'FontSize', arg.FontSize);
end

% Set interpreter
if(not(isempty(arg.Interpreter)))
    set(findall(h2, '-property', 'Interpreter'), 'Interpreter', arg.Interpreter);
end

% Additional Flags
additionalFlags = {['-d' arg.Format], ...
                   ['-r' arg.Resolution]};
if not(isempty(arg.Renderer))
    additionalFlags = [additionalFlags, {['-' arg.Renderer]}];
end
if arg.Tight == false
    additionalFlags = [additionalFlags, {'-loose'}];
end

% Write PSFrag *.tex file
if arg.PSfrag
    % Resize on-screen
    set(h2, 'Units', arg.Units);
    screenPos = get(h2, 'Position');
    set(h2, 'Position', [screenPos(1), screenPos(2), arg.Width, arg.Height]);
    drawnow;
    
    writePSfrag(h2, arg.fName);
end

% Resize on-screen
set(h2, 'Units', arg.Units);
screenPos = get(h2, 'Position');
set(h2, 'Position', [screenPos(1), screenPos(2), arg.Width, arg.Height]);

% Update figure
drawnow;

% Export figure
print(h2, arg.fName, additionalFlags{:});

% Close figure copy
%close(h2);



function writePSfrag(h, fName)

[pathstr, name, ~] = fileparts(fName);
fileID = fopen(fullfile(pathstr, [name '.tex']),'w');
%fprintf(fileID, '\\begin{psfrag}\n');

% Unique PSFrag identifier
uid = 0;

% Replace ticklabels
hStrings = findall(gcf, 'Type', 'axes');
for i=1:length(hStrings)
    for dir={'X','Y','Z'}
        s = cellstr(get(hStrings(i),[dir{:} 'TickLabel']));
        for j=1:length(s)
            sj = s{j};
            if not(isempty(sj))
                repString = num2str(uid, '%03i');
                s{j} = repString;
                fprintf(fileID, '\\psfrag{%s}[][]{%s}\n', repString, sj);
                uid = uid+1;
            end
        end
        set(hStrings(i),[dir{:} 'TickLabel'],s);
    end
end  

% Replace strings
hStrings = findall(h, '-property', 'String');
for i=1:length(hStrings)
    s = get(hStrings(i),'String');
    if not(isempty(s))
        repString = num2str(uid, '%03i');
        set(hStrings(i), 'String', repString);
        fprintf(fileID, '\\psfrag{%s}[][]{%s}\n', repString, s);
        uid = uid+1;
    end
end

%fprintf(fileID, '\\end{psfrag}\n');
fclose(fileID);
    
    
    
    
    
    















