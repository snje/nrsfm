function display_scalar_cloud(shape, gt_shape, varargin)
    [height width] = size(shape);
    [gtheight gtwidth] = size(gt_shape);
    scale_field = gradient_field(shape);
    if width ~= gtwidth || height ~= gtheight
        disp('Mismatched size of shape and groundtruth')
        %return
    end
    if size(scale_field, 1) ~= height / 3 || size(scale_field, 2) ~= width
        disp('Mismatched size of shape and scalar field')
        return
    end
    frame = 1;
    color_field = estimate_color_field(scale_field);
    fig_h = gcf;
    setappdata(fig_h, 'shape', shape);
    setappdata(fig_h, 'color_field', color_field);
    setappdata(fig_h, 'frame', frame);
    setappdata(fig_h, 'gt_shape', gt_shape);
    display_call(fig_h, frame);
end

function [scalars] = gradient_field(shape)
    scalars = 1:(size(shape, 1) * size(shape, 2) / 3);
    scalars = reshape(scalars, size(shape, 1) / 3, size(shape, 2));
end

function display_call(fig_h, f)
    shape = getappdata(fig_h, 'shape');
    gt_shape = getappdata(fig_h, 'gt_shape');
    color_field = getappdata(fig_h, 'color_field');
    [frames, ~] = size(shape);
    frames = frames / 3;
    f = max(1, f);
    f = min(frames, f);
    xyz = shape(f * 3 - 2:f* 3, :);
    gt_xyz = gt_shape(f * 3 - 2:f* 3, :);
    C = color_field(f * 3 - 2:f * 3, :);
    markersize = 200;
    pcshow(xyz', C', 'MarkerSize', markersize );
    hold on
    pcshow(gt_xyz', zeros(size(gt_xyz')), 'MarkerSize', markersize);
    hold off
    setappdata(fig_h, 'frame', f);
    title(sprintf('Frame %i', f));
    set(fig_h, 'KeyPressFcn', {@key_callback, 1});
end

function [color_field] = estimate_color_field(scale_field)
    [width, height] = size(scale_field);
    sort_field = sort(scale_field(:));
    p = 0.01;
    index = round(size(sort_field, 1) * p);
    min_val = sort_field(index);
    max_val = sort_field(end - index);
    %min_val = min(scale_field(:));
    %max_val = max(scale_field(:));
    norm_field = (scale_field - min_val) / (max_val - min_val);
    norm_field(norm_field < 0) = 0;
    norm_field(norm_field > 1) = 1;
    red_val = [0 0 1];
    green_val = [0 1 0];
    blue_val = [1 0 0];
    x = [0 0.5 1.0];
    red_field = interp1(x, red_val, norm_field(:));
    green_field = interp1(x, green_val, norm_field(:));
    blue_field = interp1(x, blue_val, norm_field(:));
    color_field = zeros(width * 3, height);
    color_field(1:3:end, :) = reshape(red_field, width, height);
    color_field(2:3:end, :) = reshape(green_field, width, height);
    color_field(3:3:end, :) = reshape(blue_field, width, height);
end

function key_callback(hObject, eventdata, handles)
    f = getappdata(hObject, 'frame');
    f_next = f;
    key = eventdata.Key;
    if strcmp(key, 'rightarrow')
        f_next = f + 1;
    elseif strcmp(key, 'leftarrow')
        f_next = f - 1;
    elseif strcmp(key, 'pageup')
        f_next = f + 10;
    elseif strcmp(key, 'pagedown')
        f_next = f - 10;
    end
    if f ~= f_next
        display_call(hObject, f_next);
    end
end