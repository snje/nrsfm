This package contains five scripts:
  * nrsfm_dataset_reconstruction.m -- Utility for easy evaluation and file export on the dataset
  * nrsfm_score.m -- Compares your 3D reconstruction with the supplied ground truth frame. Useful for getting a feeling for your performance
  * icp.m -- Required by nrsfm_score.m
  * box_truncate.m -- Required by nrsfm_score.m
  * flatten.m -- Required by nrsfm_score.m
  * transform.m -- Required by nrsfm_score.m
  * truncated_search.m -- Required by nrsfm_score.m

The first script, nrsfm_dataset_reconstruction.m, is a utility script which helps in running your NRSfM algorithm on the dataset. It handles loading both sequences and visibility files, supply it to your algorithm and exporting the estimated 3D reconstruction into the file structured specified at http://nrsfm2017.compute.dtu.dk/dataset.

If you want to get your NRSfM reconstruction evaluated use the nrsfm_score.m script. Instruction are given in the script comments. The remaining three scripts are merely used by nrsfm_score.m and should just be included in the directory.

If you have any comments, questions or errors to report just write this email:
  * snje@dtu.dk
