function [f] = error_heatmap( score, type, varargin)
    parser = inputParser;
    %parser.KeepUnmatched = true;
    addOptional(parser, 'camera_model', 'nope', @isstr);
    addOptional(parser, 'frame', 1, @isnumeric);
    
    parse(parser, varargin{:});
    
    valid = strcmp(score(:, 5), type);
    camera_model = parser.Results.camera_model;
    frame = parser.Results.frame;
    if strcmp(camera_model, 'perspective') || strcmp(camera_model, 'orthogonal') 
       valid = valid & strcmp(score(:, 4), camera_model);
    end
    score = score(valid, :);
    
    M = [];

    for i = 1:length(score)
        s = score{i, 7};
        if sum(sum(~isnan(s))) && sum(sum(s ~= 0 ))
           v = sum(s);
           M = [M; v / norm(v, Inf)];
        end
    end
    Q = get_groundtruth(type);
    f = figure;
    scatter3(Q(frame * 3 + 1, :), Q(frame * 3 + 2, :), Q(frame * 3 + 3, :), 10, mean(M));
    axis equal;
    % colorbar;
    caxis([0 1]);
end

function [Q] = get_groundtruth(type)
    type2path = struct();
    base_path = '/repositories/nrsfm/analysis/groundtruth/';
    type2path.tearing = fullfile(base_path, 'tearing.txt');
    type2path.articulated = fullfile(base_path, 'articulated.txt');
    type2path.paper = fullfile(base_path, 'paper.txt');
    type2path.stretch = fullfile(base_path, 'stretch.txt');
    type2path.balloon = fullfile(base_path, 'balloon.txt');
    
    gt_path = type2path.(type);
    Q = dlmread(gt_path);
end

