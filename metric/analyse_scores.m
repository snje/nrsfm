clear all;
close all;

%load('/media/eco3d/nrsfm/challenge/evaluation.mat');
load('/home/snje/Data/evaluation.mat');

proposed = [cell2mat(score(:, 1))];%; cell2mat(occ_score(:, 1))];
uncor = [cell2mat(score_uncor(:, 1))];%; cell2mat(occ_score_uncor(:, 1))];
pro_proj = score(:, 4);
uncor_proj = score_uncor(:, 4);
plot(uncor);
hold on;
plot(proposed);
hold off;
alpha(0.5);
legend('Released', 'Proposed', 'Location', 'North');
proj_type = 'perspective';
valid = ~isnan(proposed) & ~isnan(uncor);
corrcoef([proposed(valid) uncor(valid)])

figure;
x = proposed(valid);
y = uncor(valid);
p = polyfit(x, y, 1);
scatter(x, y);
hold on;
plot(x, polyval(p, x));
legend('Data', 'Linear Regression', 'Location', 'SouthEast');
xlabel('Proposed score');
ylabel('Released score');

figure;
outlier_adjust = [{'tearing', 405}; {'articulated', 207}; {'balloon', 51}; {'paper', 40}; {'stretch', 40}];
outlier_uncor = cell2mat(score_uncor(valid, 2));
scene_uncor = score_uncor(valid, 5);
for i = 1:size(outlier_adjust, 1)
    s = outlier_adjust{i, 2};
    m = strcmp(outlier_adjust(i, 1), scene_uncor);
    outlier_uncor(m) = outlier_uncor(m) * s;
end
scatter(cell2mat(score(valid, 2)), outlier_uncor);
xlim([0, 8000]);
ylim([0, 8000]);
xlabel('Proposed outliers');
ylabel('Released outliers');
corrcoef(cell2mat(score(valid, 2)), outlier_uncor)


[p_pro, tbl_pro, model_pro] = anovan(cell2mat(score(valid, 1)), {score(valid, 3)}, 'display', 'off');
%[model_pro.coeffnames num2cell(model_pro.coeffs)]
[p_rel, tbl_rel, model_rel] = anovan(cell2mat(score_uncor(valid, 1)), {score_uncor(valid, 3)}, 'display', 'off');
%[model_rel.coeffnames num2cell(model_rel.coeffs)]
print_table([model_pro.coeffs model_rel.coeffs], {'%.2f'}, {'Proposed' 'Released'}, format_name(model_pro.coeffnames), 'printMode', 'latex')

% Now do scoring based on means
names = unique(score(:, 3));
s_mean = [];
u_mean = [];
for i = 1:numel(names)
   proj_type = 'orthogonal';
   
   valid = strcmp(score(:, 3), names{i}) & strcmp(score(:, 4), proj_type);
   %m = mean(cell2mat(score(valid, 1)), 'omitnan');
   m = rms(cell2mat(score(valid, 1)), 'omitnan');
   s_mean = [s_mean; m];
   
   uncor_valid = strcmp(score_uncor(:, 3), names{i}) & strcmp(score_uncor(:, 4), proj_type);
   %m = mean(cell2mat(score_uncor(uncor_valid, 1)), 'omitnan');
   m = rms(cell2mat(score_uncor(uncor_valid, 1)), 'omitnan');
   u_mean = [u_mean; m];
end

invalid = isnan(s_mean) | isnan(u_mean);
s_mean = s_mean(~invalid);
u_mean = u_mean(~invalid);
names(invalid) = [];

print_table([s_mean u_mean], {'%.2f'}, {'Proposed', 'Released'}, format_name(names), 'printMode', 'latex');

[~, Is] = sort(s_mean);
[~, Iu] = sort(u_mean);

%print_table(format_name([names(Is) s_mean(Is)]), {'%.2f'}, {'Proposed', 'Released'}, 'printMode', 'latex');
print_table([format_name(names(Iu)) num2cell(u_mean(Iu))], {'%s', '%.2f'}, {'Method', 'Mean RMS'}, 'printMode', 'latex');