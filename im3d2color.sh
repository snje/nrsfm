#!/bin/bash

in_dir=$1
out_dir=$2

cmd=$(dirname $0)/im3d2color.py
color_cmd="python /home/snje/Code/color_balance/color_balance.py -c /home/snje/Code/color_balance/calibration.pkl"

do_move() {
  path=$1
  fname_ext=$(basename $path)
  fname="${fname_ext%.*}"
  out_path=$out_dir/$fname.png
  $cmd $path $out_path
  echo "huh"
  echo $color_cmd
  $color_cmd $out_path $out_path
}

mkdir -p $out_dir

export cmd
export color_cmd
export out_dir
export -f do_move

in_files=$(ls $in_dir/*.npz)
echo $in_files | xargs --max-procs=1 -n 1 bash -c 'do_move "$@"' _
