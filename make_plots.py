import numpy as np
import pandas as pd
from scipy.io import loadmat
import matplotlib.pyplot as plt
import matplotlib.image as mplimg
import seaborn as sns
import sys
import os

mat = loadmat('./evaluation_v2.mat')
a = mat['score']
df = pd.DataFrame(a)
df.head(5)

methods_lut = {
    'BALM_v0.1.csv': 'BALM',
    'Spatial-temporal-Multi-body-NRSFM.csv': 'MultiBody',
    'vladislav.csv': 'ScalableSurface',
    'CSF2_CVPR11_v1.00.csv': 'CSF2',
    'CSF_PAMI_v1.00.csv': 'CSF',
    'Concensus.csv': 'Concensus',
    'EM_PPCA_v1.0.csv': 'EM PPCA',
    'KSTA_v1.00.csv': 'KSTA',
    'NRSFM_Yuchao_Hongdong.csv': 'SPFM',
    'PTA_v1.0.csv': 'PTA',
    'RIKs_v1.00.csv': 'RIKS',
    'SoftInextensibility.csv': 'SoftInext',
    'bundle_adjustment_nlopti_TOCHECK.csv': 'Bundle',
    'bundle_adjustment_nlopti_TOCHECK': 'Bundle',
    'compressible-sfm.csv': 'Compressible',
    'metric_projection.csv': 'MetricProj',
    'lrigid-0.2.0.csv': 'RigidTriangle',
    'MDH.csv': 'MDH'
}
inv_methods_lut = {v: k for k, v in methods_lut.items()}

paths_lut = {
    'circle': 'Circle',
    'semi_circle': 'Half Circle',
    'flyby': 'Flyby',
    'line': 'Line',
    'tricky': 'Tricky',
    'zigzag': 'Zigzag'
}
inv_paths_lut = {v: k for k, v in paths_lut.items()}


animatronic_lut = {
    'tearing': 'Tearing',
    'articulated': 'Articulated',
    'stretch': 'Stretching',
    'balloon': 'Deflation',
    'paper': 'Bending'
}
inv_animatronic_lut = {v: k for k, v in animatronic_lut.items()}


df = df.applymap(lambda x: x[0])
df[0] = df[0].apply(lambda x: x[0])
df[1] = df[1].apply(lambda x: x[0])
df[2] = df[2].apply(lambda x: methods_lut[x])
df[4] = df[4].apply(lambda x: animatronic_lut[x])
df[5] = df[5].apply(lambda x: paths_lut[x])
df.head(5)


# Paths
df_group_path = df.groupby([2,5])
df_group_path_0_mean = df_group_path[0].mean()
x_path = df_group_path_0_mean.reset_index()
x_path_pivot = x_path.pivot(2,5,0)
x_path_pivot['mean'] = x_path.groupby(2).mean()
x_path_pivot.sort_values('mean', inplace=True)
mean_path = x_path.groupby(5).mean().sort_values(0).transpose()
x_path_pivot = x_path_pivot.reindex_axis(mean_path.columns, axis=1)

sns.set(font_scale=1.3, rc={'font.weight':'bold'})
sns.set_context(rc={"font.size":16,"ytick.labelsize":20,"xtick.labelsize":16}) 
f, ax = plt.subplots(figsize=(10, 8))
cmap = sns.cubehelix_palette(light=1, as_cmap=True)
sns.heatmap(data=x_path_pivot, annot=True, fmt=".0f", linewidth=.1, ax=ax, cmap=cmap, robust=True, cbar=False)
f.tight_layout()
ax.set(xlabel='', ylabel='')
f.savefig('camera_path_performance_heatmap.pdf')

titles = x_path_pivot.keys()
images = []
for index, name in enumerate(titles):
    images.append(mplimg.imread(os.path.join("./paths", inv_paths_lut[name] + '.png')))

xl, yl, xh, yh=np.array(ax.get_position()).ravel()
w=xh-xl
h=yh-yl
xp=xl+w*0.07 #if replace '0' label, can also be calculated systematically using xlim()
size=0.12

for stride, img in enumerate(images):
    ax1=f.add_axes([(xp-size*0.5) + stride*size*1, 0.00, size, size], facecolor='b')
    ax1.axison = False
    imgplot = ax1.imshow(img, interpolation='none', cmap='gray')
f.subplots_adjust(bottom=0.18, left=0.23)
f.savefig('camera_path_performance_heatmap_images.pdf')


## Animatronics
df_group_animatronic = df.groupby([2,4])
df_group_animatronic_0_mean = df_group_animatronic[0].mean()
x_animatronic = df_group_animatronic_0_mean.reset_index()
x_animatronic_pivot = x_animatronic.pivot(2,4,0)
x_animatronic_pivot['mean'] = x_animatronic.groupby(2).mean()
x_animatronic_pivot.sort_values('mean', inplace=True)
mean_animatronic = x_animatronic.groupby(4).mean().sort_values(0).transpose()
x_animatronic_pivot = x_animatronic_pivot.reindex_axis(mean_animatronic.columns, axis=1)


sns.set(font_scale=1.3, rc={'font.weight':'bold'})
sns.set_context(rc={"font.size":16,"ytick.labelsize":20,"xtick.labelsize":16}) 
f, ax = plt.subplots(figsize=(10, 8))
cmap = sns.cubehelix_palette(light=1, as_cmap=True)
sns.heatmap(data=x_animatronic_pivot, annot=True, fmt=".0f", linewidth=.1, ax=ax, cmap=cmap, robust=True, cbar=False)
f.tight_layout()
ax.set(xlabel='', ylabel='')
f.savefig('camera_animatronic_performance_heatmap.pdf')

titles = x_animatronic_pivot.keys()
images = []
for index, name in enumerate(titles):
    images.append(mplimg.imread(os.path.join("./animatronics", inv_animatronic_lut[name] + '.png')))

xl, yl, xh, yh=np.array(ax.get_position()).ravel()
w=xh-xl
h=yh-yl
xp=xl+w*0.05 #if replace '0' label, can also be calculated systematically using xlim()
size=0.12

for stride, img in enumerate(images):
    ax1=f.add_axes([(xp-size*0.5) + stride*size*1.25, 0.00, size, size], facecolor='b')
    ax1.axison = False
    imgplot = ax1.imshow(img, interpolation='none', cmap='gray')
f.subplots_adjust(bottom=0.18, left=0.23)
f.savefig('camera_animatronic_performance_heatmap_images.pdf')
