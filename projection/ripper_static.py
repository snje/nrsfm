#!/usr/bin/env python
import numpy as np

import projection

pos = np.array([
    [7.50420000e+02, 4.30000000e+02, 6.69820000e+02],
    #[1.34999000e+03,   4.40000000e+02,   4.39990000e+02],
    [5.61000000e-01, -5.61000000e-01, 4.30000000e-01, 4.30000000e-01],
    #[3.26000000e-01,  -3.27000000e-01,   6.27000000e-01, 6.27000000e-01]
])

def generate_path(t):
    dz = 400 *(1 + np.sin(t * np.pi * 2)) * 0
    p = pos[0] + np.array([-100, 0, -400 + dz])
    return [p, pos[1]]

projection.render(generate_path)
