#!/usr/bin/env python
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib.tri import Triangulation
from scipy.spatial import ConvexHull
from scipy.spatial import Delaunay
import numpy as np
import plyfile
import sys
import tool

if len(sys.argv) < 2:
    print "Please provide trace path"
    sys.exit(1)

W = np.loadtxt(sys.argv[1])
triangles = np.loadtxt(sys.argv[2]).T.astype("uint32")

XYZ = W[:3, :].T

tool.ray_triangle(XYZ, XYZ, triangles, orthogonal = True)
