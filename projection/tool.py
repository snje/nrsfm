import numpy as np
import math
import trimesh
from mpl_toolkits.mplot3d import Axes3D
#import matplotlib.pyplot as plt
#from glumpy import glm

#trimesh.constants.tol.zero = -1

retpath = "/ret.npy"
cammatpath = "/cammat.npy"
distpath = "/dist.npy"
rvecspath = "/rvecs.npy"
tvecspath = "/tvecs.npy"

def savesinglecalibration(dirpath, ret, cammat, dist, rvecs, tvecs):
  def nonesavechecker(p, a):
    if a != None:
      np.save(p, a)

  nonesavechecker(dirpath + retpath, ret)
  nonesavechecker(dirpath + cammatpath, cammat)
  nonesavechecker(dirpath + distpath, dist)
  nonesavechecker(dirpath + rvecspath, rvecs)
  nonesavechecker(dirpath + tvecspath, tvecs)

def loadsinglecalibration(dirpath):
  def noneloader(path):
    tmppath = dirpath + path
    try:
      a = np.load(tmppath)
      return a
    except IOError:
      #print "Failed to load", tmppath
      return None

  return noneloader(retpath), noneloader(cammatpath), noneloader(distpath), noneloader(rvecspath), noneloader(tvecspath)

Rpath = "/./R.npy"
Tpath = "/./T.npy"
Epath = "/./E.npy"
Fpath = "/./F.npy"

def savestereocalibration(dirpath, ret, R, T, E, F):
  def nonesavechecker(p, a):
    tmppath = dirpath + p
    if a != None:
      np.save(tmppath, a)

  nonesavechecker(retpath, ret)
  nonesavechecker(Rpath, R)
  nonesavechecker(Tpath, T)
  nonesavechecker(Epath, E)
  nonesavechecker(Fpath, F)

def loadstereocalibration(dirpath):
  def noneloader(path):
    tmppath = dirpath + path
    try:
      a = np.load(tmppath)
      return a
    except IOError:
      print "Failed to load", tmppath
      return None

  return noneloader(retpath), noneloader(Rpath), noneloader(Tpath), noneloader(Epath), noneloader(Fpath)


lRpath = "/lR.npy"
rRpath = "/rR.npy"
lPpath = "/lP.npy"
rPpath = "/rP.npy"
Qpath = "/Q.npy"

def savestereorectify(dirpath, lR, rR, lP, rP, Q):
  def nonesavechecker(p, a):
    tmppath = dirpath + p
    if a != None:
      np.save(tmppath, a)

  nonesavechecker(lRpath, lR)
  nonesavechecker(rRpath, rR)
  nonesavechecker(lPpath, lP)
  nonesavechecker(rPpath, rP)
  nonesavechecker(Qpath, Q)

def loadstereorectify(dirpath):
  def noneloader(path):
    tmppath = dirpath + path
    try:
      a = np.load(tmppath)
      return a
    except IOError:
      print "Failed to load", tmppath
      return None

  return noneloader(lRpath), noneloader(rRpath), noneloader(lPpath), noneloader(rPpath), noneloader(Qpath)

Rpath = "/R.npy"
Ppath = "/P.npy"
Validpath = "/valid.npy"

def saverectification(dir, R, P, valid):
  def nonesavechecker(p, a):
    tmppath = dir + p
    if a != None:
      np.save(tmppath, a)

  nonesavechecker(Rpath, R)
  nonesavechecker(Ppath, P)
  nonesavechecker(Validpath, valid)

def loadrectification(dir):
  def noneloader(path):
    tmppath = dir + path
    try:
      a = np.load(tmppath)
      return a
    except IOError:
      print "Failed to load", tmppath
      return None
  return noneloader(Rpath), noneloader(Ppath), noneloader(Validpath)


def adjust_intrinsic(A, height):
    A = np.copy(A)
    A[:, 1] *= -1
    A[:, 2] *= -1
    a = np.mat(np.eye(3))
    b = np.mat(np.eye(3))
    a[1, 2] = height
    b[1, 1] = -1
    return a * b * np.mat(A)

# http://ksimek.github.io/2013/06/03/calibrated_cameras_in_opengl/
def calculate_projection(A, width, height, znear, zfar):
    A = adjust_intrinsic(A, height)
    a = znear + zfar
    b = znear * zfar
    persp = np.mat([
        [A[0, 0],   A[0, 1],    A[0, 2],    0],
        [A[1, 0],   A[1, 1],    A[1, 2],    0],
        [0,         0,          a,          b],
        [0,         0,          -1,         0]
    ])
    ndc = np.mat([
        [2.0 / width, 0, 0, -1],
        [0, -2.0 / height, 0, 1],
        [0, 0, -2.0 / (zfar - znear), -float(zfar + znear) / (zfar - znear)],
        [0, 0, 0, 1]
    ])
    #print persp
    #print ndc
    #print ndc * persp
    return ndc * persp

def glstyle_projection(points, view, A, d, (width, height), (znear, zfar)):
    projection = calculate_projection(A, width, height, znear, zfar)
    XYZ = np.mat(view) * np.mat(np.vstack((points, np.ones(points.shape[1]))))
    XYZ = np.asarray(XYZ)
    x, y, z, w = XYZ[0, :], XYZ[1, :], XYZ[2, :], XYZ[3, :]
    x, y = x / z, y / z
    r = x * x + y * y
    k1, k2, k3 = d[0, 0], d[0, 1], d[0, 4]
    x = x * (1 + k1 * r + k2 * np.power(r, 2) + k3 * np.power(r, 3))
    y = y * (1 + k1 * r + k2 * np.power(r, 2) + k3 * np.power(r, 3))
    xyz_cor = np.mat(np.vstack((x * z, y * z, z, w)))
    clip_xyz = np.mat(projection) * xyz_cor

    clip_xyz = clip_xyz / clip_xyz[3, :]
    clip_xyz[1, :] *= -1
    x = (clip_xyz[0, :] + 1) * width * 0.5
    y = (clip_xyz[1, :] + 1) * height * 0.5

    return np.asarray(np.vstack((x, y)))

def ortho_projection(points, view):
    XYZ = np.mat(view) * np.mat(np.vstack((points, np.ones(points.shape[1]))))
    x, y = XYZ[0, :], XYZ[1, :]
    return np.asarray(np.vstack((x, y)))

def rotate(M, angle, x, y, z, point=None):
    """Rotation about a vector

    Parameters
    ----------
    M : array
        Original transformation (4x4).
    angle : float
        Specifies the angle of rotation, in degrees.
    x : float
        X coordinate of the angle of rotation vector.
    y : float | None
        Y coordinate of the angle of rotation vector.
    z : float | None
        Z coordinate of the angle of rotation vector.

    Returns
    -------
    M : array
        Updated transformation (4x4). Note that this function operates
        in-place.
    """
    angle = math.pi * angle / 180
    c, s = math.cos(angle), math.sin(angle)
    n = math.sqrt(x * x + y * y + z * z)
    x /= n
    y /= n
    z /= n
    cx, cy, cz = (1 - c) * x, (1 - c) * y, (1 - c) * z
    R = np.array([[cx * x + c, cy * x - z * s, cz * x + y * s, 0],
                  [cx * y + z * s, cy * y + c, cz * y - x * s, 0],
                  [cx * z - y * s, cy * z + x * s, cz * z + c, 0],
                  [0, 0, 0, 1]], dtype=M.dtype).T
    M[...] = np.dot(M, R)
    return M

def translate(M, x, y=None, z=None):
    """Translate by an offset (x, y, z) .

    Parameters
    ----------
    M : array
        Original transformation (4x4).
    x : float
        X coordinate of a translation vector.
    y : float | None
        Y coordinate of translation vector. If None, `x` will be used.
    z : float | None
        Z coordinate of translation vector. If None, `x` will be used.

    Returns
    -------
    M : array
        Updated transformation (4x4). Note that this function operates
        in-place.
    """
    y = x if y is None else y
    z = x if z is None else z
    T = np.array([[1.0, 0.0, 0.0, x],
                  [0.0, 1.0, 0.0, y],
                  [0.0, 0.0, 1.0, z],
                  [0.0, 0.0, 0.0, 1.0]], dtype=M.dtype).T
    M[...] = np.dot(M, T)
    return M


def view_transform(theta, phi, radius, center):
    d = np.mat([0, 0, radius, 1])
    newview = np.eye(4)
    rotate(newview, theta, 0, 0, 1)
    rotate(newview, phi, 0, 1, 0)
    d = np.mat(newview) * np.mat(d).T
    d = d[:3]

    view = np.eye(4)
    c = center
    translate(view, -c[0] - d[0], -c[1] - d[1], -c[2] - d[2])
    rotate(view, theta, 0, 0, 1)
    rotate(view, phi, 0, 1, 0)
    rotate(view, -90, 0, 0, 1)
    return view

def ray_triangle(positions, vertices, triangles, orthogonal = False):
    vec_triangles = triangles.T.reshape(-1)
    vec_vertices = vertices[vec_triangles]
    vert_triangles = vec_vertices.reshape(-1, 3, 3)

    #print triangles[:, 0]
    #print vert_triangles[0, :, :]
    #print triangles[:, 1]
    #print vert_triangles[1, :, :]
    #print vec_triangles

    def perspective_dir(p):
        return -p / np.linalg.norm(p, axis = 1).reshape(-1, 1)

    def orthogonal_dir(p):
        d = np.copy(p)
        d[:, 0] = 0
        d[:, 1] = 0
        d[:, 2] = 1
        return d

    D = orthogonal_dir(positions) if orthogonal else perspective_dir(positions)

    def ray_triangles(i):
        all_col = trimesh.ray.ray_triangle_cpu.ray_triangles(
            vert_triangles, positions[i, :], D[i,:]
        )
        return reduce(lambda a, b: a or b, all_col)

    collision = np.array(map(ray_triangles, np.arange(D.shape[0])))
    return (1 - collision).astype('bool')

def plot_scatter3(W, f, occ):
    fig = plt.figure()
    ax = fig.gca(projection='3d')

    xmin = np.min(W[0::3, :])
    xmax = np.max(W[0::3, :])
    ymin = np.min(W[1::3, :])
    ymax = np.max(W[1::3, :])
    zmin = np.min(W[2::3, :])
    zmax = np.max(W[2::3, :])
    size = max(xmax - xmin, ymax - ymin, zmax - zmin)

    ax.set_xlim3d(xmin, xmin + size)
    ax.set_ylim3d(ymin, ymin + size)
    ax.set_zlim3d(zmin, zmin + size)
    #ax.axis('equal')
    #occ = occ != 0
    ax.scatter(W[f * 3, occ], W[f * 3 + 1, occ], W[f * 3 + 2, occ], color = 'r')
    nocc = occ == False

    plt.hold(True)

    ax.scatter(W[f * 3, nocc], W[f * 3 + 1, nocc], W[f * 3 + 2, nocc], color = 'b')
    ax.set_title('Frame %i' % f)

    plt.show()


class Recorder:
    def __init__(self):
        self.pose = []
        self.theta = []
        self.phi = []
        self.radius = []
        self.center = []

    def capture(self, state):
        self.pose += [state.get_pose()]
        self.theta += [state.get_theta()]
        self.phi += [state.get_phi()]
        self.radius += [state.get_radius()]
        self.center += [state.get_center()]

    def write(self, path):
        np.savez(
            path, pose = self.pose, theta = self.theta, phi = self.phi,
            radius = self.radius, center = self.center
        )
    @staticmethod
    def read(path):
        r = Recorder()
        data = np.load(path)
        r.pose = data['pose']
        r.theta = data['theta']
        r.phi = data['phi']
        r.radius = data['radius']
        r.center = data['center']
        return r
