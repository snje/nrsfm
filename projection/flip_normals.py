#!/usr/bin/env python
import numpy as np
import sys
from optparse import OptionParser

parser = OptionParser()

parser.add_option("-x", "--x", dest = "x", help = "x coordinates for sensor")
parser.add_option("-y", "--y", dest = "y", help = "y coordinates for sensor")
parser.add_option("-z", "--z", dest = "z", help = "z coordinates for sensor")
parser.add_option(
    "-o", "--output", dest = "output", help = "Sets output directory",
    metavar = "DIR"
)

(options, args) = parser.parse_args()

if len(args) < 2:
    print "Please supply points and normals"
    sys.exit(-1)

P = np.loadtxt(args[0]).T
N = np.loadtxt(args[1]).T

x = float(options.x or "0")
y = float(options.y or "0")
z = float(options.z or "0")

path = options.output or "flip_normals.txt"

frames = P.shape[1] / 3
points = P.shape[0]

P = P.reshape(points, frames, 3)
N = N.reshape(points, frames, 3)

D = np.zeros((points, frames, 3))
D[:, :, 0] = x
D[:, :, 1] = y
D[:, :, 2] = z

D = D - P
DdN = np.sum(np.multiply(D, N), axis = 2)

# Flip normals
N[DdN < 0, :] *= -1

N = N.reshape(points, -1).T

np.savetxt(path, N)
