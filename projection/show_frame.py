#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt

import sys

if len(sys.argv) < 2:
    print 'Please provide file [and visibility]'

path = sys.argv[1]
f = 0

W = np.loadtxt(path)
frames = W.shape[0] / 2
points = W.shape[1]

W[1::2, :] = -W[1::2, :]

V = np.ones((frames * 2, points))
if len(sys.argv) > 2:
    V = np.loadtxt(sys.argv[2]).astype("bool")

X = W[0::2, :]
Y = W[1::2, :]
xmin = np.min(X)
xmax = np.max(X)
ymin = np.min(Y)
ymax = np.max(Y)
size = max(xmax - xmin, ymax - ymin)

V = V.astype("bool")
if not f < frames:
    print "Requested frame {0} exceeded size {1}" % (f, frames)

def draw_frame(ax, f):
    #ax.axhspan(xmin, xmax, facecolor='0.5', alpha=0.1)
    #ax.axvspan(ymin, ymax, facecolor='0.5', alpha=0.1)
    plt.xlim([xmin, xmin + size])
    plt.ylim([ymin, ymin + size])
    vx = V[f * 2, :]
    vy = V[f * 2 + 1, :]
    ax.scatter(W[f * 2, vx], W[f * 2 + 1, vy], c = 'b')
    ax.scatter(W[f * 2, ~vx], W[f * 2 + 1, ~vy], c = 'r')

def press(event):
    sys.stdout.flush()
    if event.key == 'x':
        pass
    global f
    f_next = f
    if event.key == 'left':
        #ax.clear()
        #draw_frame(ax, 0)
        #visible = xl.get_visible()
        #xl.set_visible(not visible)
        #fig.canvas.draw()
        f_next = max(0, f_next - 1)
    if event.key == 'right':
        f_next = min(frames - 1, f_next + 1)
    if f != f_next:
        ax.clear()
        draw_frame(ax, f_next)
        fig.canvas.draw()
        f = f_next

fig, ax = plt.subplots()
draw_frame(ax, f)

fig.canvas.mpl_connect('key_press_event', press)


plt.show()
