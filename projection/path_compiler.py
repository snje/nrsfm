#!/usr/bin/env python
import numpy as np
import tool

import projection
import sys

parser = projection.get_argparser()
parser.add_option(
    "-p", "--path", dest = "path",
    help = "Path to the recorder pathing file",
    metavar = "file"
)
(options, args) = parser.parse_args()

if options.path is None:
    print "Please provide recorder pating file"
    sys.exit(-2)

record = tool.Recorder.read(options.path)

pose = np.array(record.pose)
theta = np.array(record.theta)
phi = np.array(record.phi)
center = np.array(record.center)
radius = np.array(record.radius)

order = np.argsort(pose)

pose = pose[order]
theta = theta[order]
phi = phi[order]
center = center[order, :]
radius = radius[order]

print pose, theta

def __interpolate(p):
    #print p, pose
    t = np.interp(p, pose, theta)
    p = np.interp(p, pose, phi)
    x = np.interp(p, pose, center[:, 0])
    y = np.interp(p, pose, center[:, 1])
    z = np.interp(p, pose, center[:, 2])
    r = np.interp(p, pose, radius)
    full = tool.view_transform(t, p, r, np.array([x, y, z])).T
    rot = tool.view_transform(t, p, 0, np.array([0, 0, 0])).T
    return full, rot

def generate_path(p):
    f, r = __interpolate(p)
    return np.mat(f), np.mat(r)


projection.set_direct_projection()
projection.render(generate_path)
