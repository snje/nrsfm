#!/usr/bin/env python

import numpy as np
from glumpy import app, gl, gloo, glm
import util
import structuredlight as stl
import sys
import os
import glob
import re
import glfw
import transformations as tf
import concurrent.futures as futures
import tool
import console

def _load_shader():
    base_path = os.path.dirname(sys.argv[0])
    base_path = "." if base_path == "" else base_path
    f = open(base_path + '/shader/point.glsl')
    s = f.read()
    f.close()
    vertex_str = "#define __VERTEX__\n" + s
    fragment_str = "#define __FRAGMENT__\n" + s
    return vertex_str, fragment_str


class State:
    def __init__(self, A, d, trace_3d):
        print trace_3d.shape
        self.__shader_str = _load_shader()
        self.__render_limit__ = 1e6

        self.pose = 0
        self.trace_3d = trace_3d
        self.A = A
        self.d = d
        self.theta = 0
        self.phi = 90
        self.r = 500
        #self.center = np.zeros(3)
        self.center = np.mean(trace_3d[:3, :], axis = 1)
        print self.center
        #self.center = np.mean(
        #    self.view_frames[self.view_frames.keys()[0]][self.pose], axis = 1
        #)


        self.trace_program = self.__create_program__(trace_3d.shape[1])
        t_colors = np.random.uniform(0.1, 1.00, (trace_3d.shape[1], 4))
        t_colors[:, 3] = 1
        self.trace_program['bg_color'] = t_colors
        self.trace_program['radius'] = 1

        self.marker_program = self.__create_program__(1)
        self.marker_program['radius'] = 2
        self.marker_program['bg_color'] = np.array([1, 1, 0, 1])
        self.marker_program['position'] = self.center


        self.update_view_transform()
        self.set_pose(0)

    def set_theta(self, t):
        self.theta = t
    def get_theta(self):
        return self.theta
    def set_phi(self, p):
        self.phi = max(0, min(180, p))
    def get_phi(self):
        return self.phi
    def set_radius(self, r):
        self.r = max(2, r)
    def get_radius(self):
        return self.r
    def set_center(self, c):
        self.center = c
        self.marker_program['position'] = self.center
    def get_center(self):
        return self.center

    def __create_program__(self, n):
        (v, f) = self.__shader_str
        p = gloo.Program(v, f, count = n)
        p['radius'] = 0
        p['linewidth'] = 0.01
        p['antialias'] = 1.0
        print self.d
        p['distortion'][:2] = self.d[0, :2]
        p['distortion'][2] = self.d[0, 4]
        width, height, znear, zfar = 3376, 2704, 1, 3000.0
        p['projection'] = tool.calculate_projection(
            self.A, width, height, znear, zfar
        ).T # Transpose due to column major nonsense
        p['view'] = np.eye(4)
        p['model'] = np.eye(4)
        return p

    def update_view_transform(self):
        T = self.get_view_transform()
        self.trace_program['view'] = T
        self.marker_program['view'] = T

    def max_pose(self):
        return self.trace_3d.shape[0] / 3

    def set_pose(self, p):
        m = self.max_pose()
        if p >= self.max_pose():
            return self.set_pose(p - m)
        if p < 0:
            return self.set_pose(p + m)
        self.pose = p
        if p * 3 < self.trace_3d.shape[0]:
            tf = self.trace_3d[(p) * 3:(p + 1) * 3, :]
            self.trace_program['position'] = tf.T

    def get_pose(self):
        return self.pose

    def _execute_load(self, p):
        def _action_co_(key, path):
            load_job = self.__io_pool__.submit(stl.im3dread, path)
            while not load_job.done():
                yield False
            (im3d, _, im) = load_job.result()
            im3d = im3d.reshape(-1, 3).astype("float32")
            im = im.reshape(-1, 3).astype("float32")
            im = im / 255.0
            m = ~np.isnan(im3d)
            p3d = im3d[m[:, 0], :]
            c3d = im[m[:, 0], :]

            subsampling = int(np.ceil(p3d.shape[0] / self.__render_limit__))
            vp = self.view_programs[key]
            sub_points = p3d[::subsampling, :]
            render_count = p3d[::subsampling, :].shape[0]
            vp['position'][:render_count, :] = sub_points
            colors = np.hstack((c3d, np.ones((c3d.shape[0], 1))))
            colors[:,3] = 1
            vp['bg_color'][:render_count, :] = colors[::subsampling, :]
            vp['radius'][:render_count] = 0.01
            vp['radius'][render_count:] = 0
            while True:
                yield True



        load_jobs = map(lambda (k, paths): _action_co_(k, paths[p]), self.view_frames.items())
        while not reduce(lambda a, b: a and b, map(lambda a: a.next(), load_jobs)):
            yield True
        yield False

    def update_io(self):
        __current = -1
        while True:
            if __current != self.pose:
                __current = self.pose
                i = self.pose
                #e = self._execute_load(self.pose)
                #while e.next():
                #    yield
            else:
                yield

    def get_view_transform(self):
        return tool.view_transform(self.theta, self.phi, self.r, self.center)

    def draw(self):
        gl.glEnable(gl.GL_DEPTH_TEST)
        #map(lambda p: p.draw(gl.GL_POINTS), self.view_programs.values())
        gl.glDisable(gl.GL_DEPTH_TEST)
        self.trace_program.draw(gl.GL_POINTS)
        self.marker_program.draw(gl.GL_POINTS)



d = app.parser.get_default()
d.add_argument("gt_path")
d.add_argument(
    "-c", "--calibration", dest = "cali", metavar = "path",
    help = "Path to calibration directory"
)
arg = d.parse_args()

if arg.cali is None:
    print "Please supply calibration"
    sys.exit(1)

lret, lA, ld, _, _ = util.loadsinglecalibration(arg.cali)

trace_3d = np.loadtxt(arg.gt_path)

#d.add_argument("-c", "--calibration", dest = "calibration", metavar = "path")
window = app.Window(width=800, height=640, color=(1,1,1,1), context = "stuff")
window.set_position(0, 0)
# This is a bit of a hack that hides and locks the cursor
glfw.set_input_mode(
    glfw.get_current_context(), glfw.CURSOR, glfw.CURSOR_DISABLED
)

state = State(lA, ld, trace_3d)
u = state.update_io()

recorder = tool.Recorder()

@window.event
def on_draw(dt):
    u.next()
    window.clear()
    state.update_view_transform()
    state.draw()

@window.event
def on_mouse_motion(x, y, dx, dy):
    state.set_theta(state.get_theta() - dx * 0.1)
    state.set_phi(state.get_phi() - dy * 0.1)

@window.event
def on_mouse_scroll(x, y, dx, dy):
    state.set_radius(state.get_radius() - dy * 10)

def _handle_move_characters(key):
    dp = np.array([0, 0, 0])
    if key == "q":
        dp[0] += 10
    if key == "a":
        dp[0] -= 10
    if key == "w":
        dp[1] += 10
    if key == "s":
        dp[1] -= 10
    if key == "e":
        dp[2] += 10
    if key == "d":
        dp[2] -= 10
    state.set_center(state.get_center() + dp)

def _handle_next_characters(text):
    dp = 0
    if text == "z":
        dp -= 1
    elif text == "x":
        dp += 1
    state.set_pose(state.get_pose() + dp)

def _handle_capture(text):
    if text != ' ':
        return
    print "captured"
    recorder.capture(state)


@window.event
def on_character(text):
    _handle_next_characters(text)
    _handle_move_characters(text)
    _handle_capture(text)

app.run(interactive=True)
