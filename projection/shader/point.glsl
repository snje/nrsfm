#version 120

varying vec4  v_bg_color;

#ifdef __VERTEX__
attribute vec4  bg_color;
attribute vec3  position;
attribute float radius;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform vec3 distortion;
uniform float linewidth;
uniform float antialias;

void main() {
    v_bg_color = bg_color;
    gl_Position = projection * view * model * vec4(position, 1.0);
    if (radius > 0) {
        gl_PointSize = 2 * (radius + linewidth + 1.5*antialias);
    } else {
        gl_PointSize = 0;
    }

    vec4 XYZ = view * model * vec4(position, 1.0);
    vec2 XY = XYZ.xy / XYZ.z;
    float k1 = distortion.x;
    float k2 = distortion.y;
    float k3 = distortion.z;
    float r = XY.x * XY.x + XY.y * XY.y;
    vec2 cor_XY = vec2(
        XY.x * (1 + k1 * r + k2 * pow(r, 2) + k3 * pow(r, 3)),
        XY.y * (1 + k1 * r + k2 * pow(r, 2) + k3 * pow(r, 3))
    );
    //gl_Position = projection * view * model * vec4(position, 1);
    gl_Position = projection * vec4(cor_XY * XYZ.z, XYZ.zw);
}

#endif

#ifdef __FRAGMENT__
void main() {
    gl_FragColor = v_bg_color;
}
#endif
