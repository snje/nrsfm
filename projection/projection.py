#!/usr/bin/env python
import numpy as np
import sys
import os
from optparse import OptionParser
import transformations as trans
import tool

parser = OptionParser()

parser.add_option(
    "-v", "--verbose", action = "store_true", dest="verbose",
    default = False, help="Makes program more talkative"
)
parser.add_option(
    "-t", "--orthogonal", action = "store_true", dest="orthogonal",
    default = False, help="Makes program more talkative"
)
parser.add_option(
    "-o", "--output", dest = "output", help = "Sets output directory",
    metavar = "DIR"
)
parser.add_option(
    "-c", "--calibration", dest = "calibration",
    help = "Path to the calibration directory", metavar = "FILE"
)
parser.add_option(
    "-m", "--mesh", dest = "mesh",
    help = "Path to the mesh file", metavar = "FILE"
)
parser.add_option(
    "-n", "--normal", dest = "normal",
    help = "Path to the mesh file", metavar = "FILE"
)
parser.add_option(
    "-i", "--visibility", dest = "visibility",
    help = "Path to visibility output file (requires mesh input)",
    metavar = "FILE"
)
parser.add_option(
    "-d", "--draw", dest = "draw",
    help = "Renders resulting trace and outputs to directory",
    metavar = "DIR"
)
parser.add_option(
    "-g", "--groundtruth", dest = "groundtruth",
    help = "Optional path for export groundtruth file",
    metavar = "DIR"
)
parser.add_option(
    "-s", "--timescale", dest = "timescale",
    help = "Step scaling for time steps", metavar = "DIR"
)

l2c = np.mat([
    [0, 0, 1, 0],
    [1, 0, 0, 0],
    [0, 1, 0, 0],
    [0, 0, 0, 1],
])
def quat2mat(t, q):
    M = trans.quaternion_matrix(q)
    M[0:3, 3] = np.array(t)
    return np.mat(M)

def view2disk(pose, views, projected_trace, color, size, map1, map2, draw_dir):
    proj_t2d = projected_trace
    cir_im = np.zeros((size[1], size[0], 3), dtype = "uint8")
    for j in xrange(proj_t2d.shape[0]):
        x = int(proj_t2d[j, 0])
        y = int(proj_t2d[j, 1])
        #print x, y
        cv2.circle(cir_im, (x, y), 8, color[j, :], 2)

    cir_im = cv2.remap(cir_im, map1, map2, cv2.INTER_LINEAR)
    cv2.imwrite((draw_dir + "/{0}.png").format(pose), cir_im)

def adjust_intrinsic(A, height):
    A[:, 1] *= -1
    A[:, 2] *= -1
    a = np.mat(np.eye(3))
    b = np.mat(np.eye(3))
    a[1, 2] = height
    b[1, 1] = -1
    return a * b * np.mat(A)

# http://ksimek.github.io/2013/06/03/calibrated_cameras_in_opengl/
def calculate_projection(A, width, height, znear, zfar):
    A = adjust_intrinsic(A, height)
    a = znear + zfar
    b = znear * zfar
    persp = np.mat([
        [A[0, 0],   A[0, 1],    A[0, 2],    0],
        [A[1, 0],   A[1, 1],    A[1, 2],    0],
        [0,         0,          a,          b],
        [0,         0,          -1,         0]
    ])
    ndc = np.mat([
        [2.0 / width, 0, 0, -1],
        [0, -2.0 / height, 0, 1],
        [0, 0, -2.0 / (zfar - znear), -float(zfar + znear) / (zfar - znear)],
        [0, 0, 0, 1]
    ])
    #print persp
    #print ndc
    #print ndc * persp
    return ndc * persp

def glstyle_projection(points, view, A, d, (width, height), (znear, zfar)):
    projection = calculate_projection(np.copy(A), width, height, znear, zfar)
    XYZ = np.mat(view) * np.mat(np.vstack((points, np.ones(points.shape[1]))))
    XYZ = np.asarray(XYZ)
    x, y, z, w = XYZ[0, :], XYZ[1, :], XYZ[2, :], XYZ[3, :]
    x, y = x / z, y / z
    r = x * x + y * y
    k1, k2, k3 = d[0, 0], d[0, 1], d[0, 4]
    x = x * (1 + k1 * r + k2 * np.power(r, 2) + k3 * np.power(r, 3))
    y = y * (1 + k1 * r + k2 * np.power(r, 2) + k3 * np.power(r, 3))
    xyz_cor = np.mat(np.vstack((x * z, y * z, z, w)))
    clip_xyz = np.mat(projection) * xyz_cor

    clip_xyz = clip_xyz / clip_xyz[3, :]
    clip_xyz[1, :] *= -1
    x = (clip_xyz[0, :] + 1) * width * 0.5
    y = (clip_xyz[1, :] + 1) * height * 0.5

    return np.asarray(np.vstack((x, y)))




def view2nothing(pose, views, projected_trace, color, d1, d2,d3, d4):
    pass


def update_progress(verbose, end_val, bar_length=20):
    if verbose:
        for i in xrange(end_val / 5 + 1):
            sys.stdout.write('\r')
        # the exact output you're looking for:
            sys.stdout.write("[%-20s] %d%%" % ('='*i, end_val))
            sys.stdout.flush()

pos = [
    [7.50420000e+02, 4.30000000e+02, 6.69820000e+02],
    #[1.34999000e+03,   4.40000000e+02,   4.39990000e+02],
    [5.61000000e-01, -5.61000000e-01, 4.30000000e-01, 4.30000000e-01],
    #[3.26000000e-01,  -3.27000000e-01,   6.27000000e-01, 6.27000000e-01]
]



def get_argparser():
    return parser

#ld = 10 * ld
def setup(fun):
    global lA
    global ld
    lA, ld = fun(lA, ld)

direct_projection = False
def set_direct_projection():
    global direct_projection
    direct_projection = True

def render(path_generator):
    (options, args) = parser.parse_args()

    if len(args) == 0:
        sys.exit(0)

    data_dir = os.path.dirname(args[0])
    output_path = options.output or "./trace.txt"

    mesh = None
    if options.mesh is not None:
        mesh = np.loadtxt(options.mesh).astype("int32")
    #if options.visibility and mesh is None:
    #    print "Please supply path to mesh file if you want visibility output"
    #    sys.exit(2)
    normals = None
    if options.normal is not None:
        normals = np.loadtxt(options.normal)

    if options.calibration is None:
        print "Please supply internal camera calibration"
        sys.exit(1)
    if options.draw is not None:
        global cv2
        import cv2

    timescale = int("1" or options.timescale)
    draw_dir = options.draw
    calipath = options.calibration
    lret, lA, ld, _, _ = tool.loadsinglecalibration(calipath + "/left/")
    rret, rA, rd, _, _ = tool.loadsinglecalibration(calipath + "/right/")
    sret, sR, sT, _, _ = tool.loadstereocalibration(calipath + "/stereo/")
    size = (3376, 2704)
    map1, map2 = np.eye(2), np.eye(2)
    # Suppress distortion
    ld = ld * 0
    rd = rd * 0

    if options.draw is not None:
        lR, rR, lP, rP, Q, lvalid, rvalid = cv2.stereoRectify(
            lA, ld, rA, rd, size, sR, sT, flags = 0
        )
        map1, map2 = cv2.initUndistortRectifyMap(
            lA, ld, lR, lP, size, cv2.CV_32FC1
        )
    #t2l = np.load(options.handeye)

    visualizer = view2disk if draw_dir is not None else view2nothing

    traces = map(np.loadtxt, args)
    trace3d = np.hstack(traces)
    #print trace3d
    #trace3d = trace3d[::-1, :]
    color = np.random.randint(0,255,(trace3d.shape[1],3))
    update_progress(options.verbose, 0)
    poses = trace3d.shape[0] / 3
    views = len(args)

    def perspective_proj(t3d, view):
        return glstyle_projection(t3d, view, lA, ld, size, (1, 3000)).T
    def orthogonal_proj(t3d, view):
        return tool.ortho_projection(t3d, view).T

    projection_method = orthogonal_proj if options.orthogonal else perspective_proj

    full_trace = []
    full_occlusion = []
    print "all the poses", poses
    for ii in xrange(poses):
        t = ii / timescale
        #dz = 400 *(1 + np.sin(t * np.pi * 2)) * 0
        #p = pos[0] + np.array([-100, 0, -400 + dz])
        pos = path_generator(t)
        P, R = None, None
        if not direct_projection:
            T = quat2mat(pos[0], pos[1]) * quat2mat(t2l[0], t2l[1]) * l2c
            P = np.linalg.inv(T)
        else:
            P, R = path_generator(ii)

        view = P
        P = P[0:3, :]

        t3d = trace3d[ii * 3:ii * 3 + 3, :]
        #proj_t2d = glstyle_projection(t3d, view, lA, ld, size, (1, 3000)).T
        proj_t2d = projection_method(t3d, view)
        #print np.mean(t3d, axis = 1)
    #    t3d = t3d[::-1, :]
        #proj_t3d = P * np.vstack((t3d, np.ones(t3d.shape[1])))
        full_trace.append(proj_t2d.T)
        occluded = np.ones(t3d.shape[1], dtype = "bool")
        if mesh is not None:
            XYZ = np.mat(view) * np.mat(np.vstack((t3d, np.ones(t3d.shape[1]))))
            XYZ = np.asarray(XYZ)
            XYZ = XYZ[:3, :]
            _occluded = tool.ray_triangle(XYZ.T, XYZ.T, mesh, options.orthogonal)
            occluded = np.bitwise_and(occluded, _occluded)
            #tool.plot_scatter3(XYZ, 0, occluded)
            #full_occlusion += [occluded]
        if normals is not None:
            #frame_normal = R[0:3, 0:3] * np.mat(normals[ii * 3:ii * 3 + 3, :])
            N = np.mat(np.vstack((normals[ii * 3:ii * 3 + 3, :], np.zeros(t3d.shape[1]))))
            #print(N[2, 0])
            N = np.linalg.inv(np.mat(view)).T * N
            frame_normal = np.asarray(N[:3, :])
            XYZ = np.zeros(t3d.shape)
            XYZ[2, :] = -1
            if not options.orthogonal:
                XYZ = np.mat(view) * np.mat(np.vstack((t3d, np.ones(t3d.shape[1]))))
                XYZ = np.asarray(XYZ)
                XYZ = XYZ[:3, :]
                XYZ /= np.linalg.norm(XYZ, axis = 0)
            _occluded = np.sum(-XYZ * frame_normal, axis = 0) >= 0.001
            occluded = np.bitwise_and(occluded, _occluded)

        full_occlusion += [occluded]
        # Add double for both coordinates.
        full_occlusion += [occluded]
        visualizer(ii, views, proj_t2d, color, size, map1, map2, draw_dir)
        #update_progress(options.verbose, ii * 100 / (poses - 1))

    full_trace = np.vstack(full_trace)
    np.savetxt(output_path, full_trace)
    if options.groundtruth:
        np.savetxt(options.groundtruth, trace3d)
    print ""
    # HACK CODE
    if options.visibility is not None:
        np.savetxt(options.visibility, np.array(full_occlusion))

# Utilities
def scale_distortion(s):
    def _action(lA, ld):
        return lA, ld * s
    return _action
