#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt

import sys

if len(sys.argv) < 3:
    print 'please provide input and output file'
    sys.exit(-1)

input_path = sys.argv[1]
output_path = sys.argv[2]

W = np.loadtxt(input_path)

MD = np.loadtxt(sys.argv[3]) if len(sys.argv) > 3 else np.ones(W.shape)
MD = MD.astype("bool")

X = W[0::2, :]
Y = W[1::2, :]
xmin = np.min(X)
xmax = np.max(X)
ymin = np.min(Y)
ymax = np.max(Y)
size = max(xmax - xmin, ymax - ymin)

frames = W.shape[0] / 2

for f in xrange(frames):
    #XY = W[frames * 2: frames * 2 + 1, :]
    fig, ax = plt.subplots()
    plt.xlim([xmin, xmin + size])
    plt.ylim([-ymax, -ymax + size])
    ax.scatter(W[f * 2, ~MD[f * 2, :]], -W[f * 2 + 1, ~MD[f * 2 + 1, :]], c = 'r')
    ax.scatter(W[f * 2, MD[f * 2, :]], -W[f * 2 + 1, MD[f * 2 + 1, :]], c = 'b')
    #plt.show()
    #fig.patch.set_visible(False)
    ax.axis('off')
    n = str(f)
    plt.savefig('{0}/{1}.png'.format(output_path, n.zfill(3)))
    plt.close()
