#!/usr/bin/env python

import numpy as np
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d import proj3d
import matplotlib.pyplot as plt

import sys

if len(sys.argv) < 2:
    print 'Please supply input file and output directory'
    sys.exit(-1)

path = sys.argv[1]
#output_path = sys.argv[2]
#f = int(sys.argv[2]) if len(sys.argv) > 2 else 0
f = 0

W = np.loadtxt(path)
frames = W.shape[0] / 3



xmin = np.min(W[0::3, :])
xmax = np.max(W[0::3, :])
ymin = np.min(W[1::3, :])
ymax = np.max(W[1::3, :])
zmin = np.min(W[2::3, :])
zmax = np.max(W[2::3, :])

size = max(xmax - xmin, ymax - ymin, zmax - zmin)

N = None
if len(sys.argv) > 3:
    N = np.loadtxt(sys.argv[3])

if not f < frames:
    print "Requested frame {0} exceeded size {1}" % (f, frames)

def draw_frame(ax, f):
    ax.set_xlim3d(xmin, xmin + size)
    ax.set_ylim3d(ymin, ymin + size)
    ax.set_zlim3d(zmin, zmin + size)
    #ax.axis('equal')
    X, Y, Z = W[f * 3, :], W[f * 3 + 1, :], W[f * 3 + 2, :]
    ax.scatter(X, Y, Z, c = 'r')
    if N is not None:
        nX, nY, nZ = N[f * 3, :], N[f * 3 + 1, :], N[f * 3 + 2, :]
        ax.quiver(X, Y, Z, nX, nY, nZ, length = size * 0.1, pivot = 'tail')

    #for i in xrange(X.shape[0]):
        #x, y, _ = proj3d.proj_transform(X[i], Y[i], Z[i], ax.get_proj())
        #ax.annotate(str(i), (x, y))
    ax.set_title('Frame %i' % f)

def press(event):
    sys.stdout.flush()
    if event.key == 'x':
        pass
    global f
    f_next = f
    if event.key == 'left' or event.key == 'a':
        f_next = max(0, f_next - 1)
    if event.key == 'right' or event.key == 'd':
        f_next = min(frames - 1, f_next + 1)
    if event.key == 'pageup':
        f_next = min(frames - 1, f_next + 10)
    if event.key == 'pagedown':
        f_next = f_next = max(0, f_next - 10)
    if f != f_next:
        ax.clear()
        draw_frame(ax, f_next)
        fig.canvas.draw()
        f = f_next

fig = plt.figure()
ax = fig.gca(projection='3d')
#ax.set_aspect('equal', adjustable='box')

draw_frame(ax, f)

fig.canvas.mpl_connect('key_press_event', press)

print (ax.azim, ax.elev)

plt.show()
"""
azim, elev = ax.azim, ax.elev
fig = plt.figure()
ax = fig.gca(projection='3d')
ax.view_init(azim = azim, elev = elev)

for f in xrange(frames):
    ax.clear()
    draw_frame(ax, f)
    n = str(f)
    plt.savefig('{0}/{1}.png'.format(output_path, n.zfill(3)))
"""
#plt.show()
