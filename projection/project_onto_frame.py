#!/usr/bin/env python
import numpy as np
import cv2
import matplotlib.pyplot as plt
import sys
import os
from optparse import OptionParser
import transformations as trans
import tool
import structuredlight as stl

parser = OptionParser()
parser.add_option(
    "-f", "--frame", dest = "frame", help="Which frame to use in the track.",
    default = "0"
)
parser.add_option(
    "-p", "--pose", dest = "pose", help="Which pose to use in the track.",
    default = "0"
)
parser.add_option(
    "-c", "--calibration", dest = "calibration",
    default = "/media/eco3d/nrsfm/calibration/",
    help="Path to the calibration directory"
)
parser.add_option(
    "-a", "--path", dest = "path",
    help = "path file for the recording"
)
parser.add_option(
    "-o", "--output", dest = "output", help = "output path",
    default = "./output.png"
)

(options, args) = parser.parse_args()

if len(args) < 2:
    print "Please provide the track file and image file in that order"
    sys.exit(-1)

if options.calibration is None:
    print "Please provide calibration path"
    sys.exit(-2)

if options.path is None:
    print "Please provide path file"
    sys.exit(-3)

Q = np.loadtxt(args[0])
_, _, im = stl.im3dread(args[1])
_, lA, ld, _, _ = tool.loadsinglecalibration(options.calibration + "/left/")
_, rA, rd, _, _ = tool.loadsinglecalibration(options.calibration + "/right/")
_, sR, sT, _, _ = tool.loadstereocalibration(options.calibration + "/stereo/")
l2t = np.load(options.calibration + "/left2tool.npy")
path = np.load(options.path)

p = int(options.pose)
f = int(options.frame)

def pose2matrix(position, quat):
    T = trans.quaternion_matrix(quat)
    print(position)
    T[3, :3] = position
    return np.matrix(T)

#print path
XYZ = np.vstack([Q[:3, :], np.ones((1, Q.shape[1]))]).T

#pos = np.zeros((1, 3))
#rot = np.zeros((1, 3))
P = pose2matrix(path[f, 0, :3], path[f, 0, 3:])
H = pose2matrix(l2t[0], l2t[1])

#T = np.linalg.mat(np.linalg.inv(T))
print l2t

fig = plt.figure()
ax = fig.gca(projection='3d')
ax.scatter(XYZ[:, 0], XYZ[:, 1], XYZ[:, 2], c = 'r')
plt.show()


#(im_points, _) = cv2.projectPoints(XYZ, pos, rot, lA, ld)
#print im_points[0, :, :]
#print im_points
#for i in xrange(im_points.shape[0]):
#    print im_points[i, 0, :], 20, (255, 0, 0)
#    cv2.circle(im, im_points[i, 0, :], 20, (255, 0, 0))
    
#cv2.imwrite(options.output, im)
