#!/usr/bin/env python
import numpy as np
import tool

import projection
import sys

record = tool.Recorder.read("/tmp/test.npz")

pose = np.array(record.pose)
theta = np.array(record.theta)
phi = np.array(record.phi)
center = np.array(record.center)
radius = np.array(record.radius)

order = np.argsort(pose)

pose = pose[order]
theta = theta[order]
phi = phi[order]
center = center[order, :]
radius = radius[order]

def __interpolate(p):
    t = np.interp(p, pose, theta)
    p = np.interp(p, pose, phi)
    x = np.interp(p, pose, center[:, 0])
    y = np.interp(p, pose, center[:, 1])
    z = np.interp(p, pose, center[:, 2])
    r = np.interp(p, pose, radius)
    return tool.view_transform(t, p, r, np.array([x, y, z])).T

def generate_path(p):
    return np.mat(__interpolate(p))


projection.set_direct_projection()
projection.render(generate_path)
