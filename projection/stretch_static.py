#!/usr/bin/env python
import numpy as np

import projection

pos = np.array([
    [1.19992000e+03,   2.80000000e-01,   4.20070000e+02],
    [4.40000000e-02,   4.40000000e-02,  -9.98000000e-01, 2.00000000e-03],
])

def generate_path(t):
    dz = 400 *(1 + np.sin(t * np.pi * 2)) * 0
    p = pos[0] #+ np.array([-100, 0, -400 + dz])
    return [p, pos[1]]

projection.render(generate_path)
