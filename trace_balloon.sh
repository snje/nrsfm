#!/bin/bash

program=$0
export track_script="$(dirname $0)/sparseflow.py"
export kalman_script="$(dirname $0)/kalman_filter.py"

SHORT=hvd:
LONG=help,verbose,debug:

# -temporarily store output to be able to check for errors
# -activate advanced mode getopt quoting e.g. via “--options”
# -pass arguments only via   -- "$@"   to separate them correctly
PARSED=`getopt --options $SHORT --longoptions $LONG --name "$0" -- "$@"`
if [[ $? != 0 ]]; then
    # e.g. $? == 1
    #  then getopt has complained about wrong arguments to stdout
    exit 2
fi
# use eval with "$PARSED" to properly handle the quoting
eval set -- "$PARSED"

function show_help {
  #echo "Program which scans a folder structure and reconstrucs using PSI algorithm"
  printf "Usage: $(basename $1) [options] [args]\n"
  printf "\nOptions:\n"
  printf "\t -h, --help \t show help message and exit\n"
  printf "\t -v, --verbose \t set script to verbose\n"
}

CMD_ARG=""
# now enjoy the options in order and nicely split until we see --
while true; do
    case "$1" in
        -h|--help)
            show_help $program
            exit 0
            ;;
        -v|--verbose)
            CMD_ARG="$CMD_ARG -v"
            shift
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
    esac
done

function _do_stuff() {
  images=$(ls -1vd $1/pose*_view$2.npz)
  mask=$1/mask_view$2.png
  tmp_out=/tmp/$RANDOM.txt
  $track_script $CMD_ARG -m $mask -o $tmp_out $images #-d /home/snje/Data/debug/
  out=$1/trace3d_view$2.txt
  $kalman_script $tmp_out $out
}
export -f _do_stuff
export CMD_ARG
export script
parallel -j3 -u _do_stuff ::: $@ ::: $(seq 0 0)
