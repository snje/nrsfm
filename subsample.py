#!/usr/bin/env python
import numpy as np
import sys

if len(sys.argv) < 3:
    print "Please supply input and output path"
    sys.exit(-1)

P = np.loadtxt(sys.argv[1])

s = 2
if len(sys.argv) >= 4:
    s = int(sys.argv[3])

print "Running with subsampling", s

prev = P.shape
P = P[:, ::s]
post = P.shape
print "Reduced shape from", prev, "->", post

np.savetxt(sys.argv[2], P)
