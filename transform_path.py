#!/usr/bin/env python
import numpy as np
import sys
from optparse import OptionParser


parser = OptionParser()

parser.add_option(
    "-p", "--path", dest="path", help="File path for the world space pathing"
)

(options, args) = parser.parse_args()


if len(sys.argv) < 3:
    print "Please provide input and output path"
    sys.exit(-1)
