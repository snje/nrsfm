import numpy as np
import sys

in_path = sys.argv[1]
out_path = sys.argv[2]

G = np.loadtxt(in_path)

frames = G.shape[0] / 3

gt_frame = int(frames / 2)

out_path = out_path + '/gt_frame_' + str(gt_frame) + '.txt'

F = G[gt_frame * 3:gt_frame * 3 + 3, :]

np.savetxt(out_path, F)
