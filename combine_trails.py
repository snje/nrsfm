#!/usr/bin/env python
from optparse import OptionParser
import numpy as np
import os
import sys

parser = OptionParser()

parser.add_option(
    "-o", "--output", dest = "output", help = "Sets output directory",
    metavar = "DIR"
)

(options, args) = parser.parse_args()

output_path = options.output or "./output.txt"

P = map(np.loadtxt, args)
T = np.hstack(P)
np.savetxt(output_path, T)
