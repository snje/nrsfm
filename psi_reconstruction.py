#!/usr/bin/env python
import numpy as np
import cv2
import sys
import os
from optparse import OptionParser

import structuredlight as stl
import util


parser = OptionParser()

parser.add_option(
    "-v", "--verbose", action = "store_true", dest="verbose",
    default = False, help="Makes program more talkative"
)
parser.add_option(
    "-o", "--output", dest = "output", help = "Sets output directory",
    metavar = "DIR"
)
parser.add_option(
    "-c", "--calibration", dest = "calibration",
    help = "Path to the calibration directory", metavar = "FILE"
)
parser.add_option(
    "-p", "--periods", dest = "periods",
    help = "Number of periods used in the fine-grained phase sequence",
    metavar = "NUM"
)
parser.add_option(
    "-l",  "--color", dest = "color",
    help = "Color image that gives the color mapping for the reconstruction",
    metavar = "FILE"
)

(options, args) = parser.parse_args()

if len(args) == 0:
    print "Please supply images for reconstruction"
    sys.exit(-1)

if options.calibration is None:
    print "Please supply internal camera calibration"
    sys.exit(-1)

calipath = options.calibration

lret, lA, ld, _, _ = util.loadsinglecalibration(calipath + "/left/")
rret, rA, rd, _, _ = util.loadsinglecalibration(calipath + "/right/")
sret, sR, sT, _, _ = util.loadstereocalibration(calipath + "/stereo/")

periods = int(options.periods or "16")

count = len(args) / 2

def load_image(path):
    return cv2.imread(path, 1)

lseq = map(load_image, args[:count])
rseq = map(load_image, args[count:])

cmap = None
if options.color is not None:
    cmap = cv2.imread(options.color, 1)
else:
    cmap = lseq[-1].astype("float32") + lseq[-2].astype("float32")\
            + lseq[-3].astype("float32")
    cmap = (cmap / 3).astype("uint8")

def _to_gray(im):
    return cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)

lseq = map(_to_gray, lseq)
rseq = map(_to_gray, rseq)


def reconstruction(lseq, rseq, lA, ld, rA, rd, sR, st):
    return stl.phaseshift(lseq, rseq, lA, ld, rA, rd, sR, st, periods = periods)

im3d, cmap = stl.generate_im3d(
    reconstruction, cmap, lseq, rseq, lA, ld, rA, rd, sR, sT
)

output_dir = options.output or "./im3d.npz"
stl.im3dwrite(output_dir, im3d, colorim = cmap)
