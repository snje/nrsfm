#! /usr/bin/env python

import pypcd
import cv2
import numpy as np
import structuredlight as stl
import sys
import util
import matplotlib.pyplot as plt
#import segmentation

if len(sys.argv) < 3:
    print "Please supply input and output path"
    sys.exit(1)

p, phase, c = stl.im3dread(sys.argv[1])
"""
#util.valueplot(p[:, :, 2])
#util.valueplot(phase)

bg_im3d, _, _ = stl.im3dread("/home/snje/Data/calibration/ringsted_070616/bg.npz")
#for i in xrange(3):
#    bg_im3d[:, :, i] = cv2.medianBlur(bg_im3d[:, :, i], 5)
bg_depth = np.linalg.norm(bg_im3d, axis = 2)
bg_mask = np.isnan(bg_depth)
#util.valueplot(bg_depth)
#util.show()
im3d = segmentation.segment_meat(c, p, bg_depth)
"""

def _balance(im):
    v = im.reshape(-1)
    v_sort = np.sort(v)
    p4 = int(0.04 * len(v))
    vmin = v_sort[p4]
    vmax = v_sort[-p4]
    im = np.clip(im, vmin, vmax)
    return cv2.normalize(im, alpha = 0, beta = 255, norm_type = cv2.NORM_MINMAX)

def balance(im):
    bim = np.copy(im)
    for i in xrange(3):
        bim[:, :, i] = _balance(bim[:, :, i])
    return bim

c = balance(c)

#p = p / 1000.0
x = p[:, :, 0]
y = p[:, :, 1]
z = p[:, :, 2]

print c.shape
size = c.shape[0:2]
r = c[:, :, 2]
g = c[:, :, 1]
b = c[:, :, 0]
color_str = np.concatenate(
    (c, 255 * np.ones((size[0], size[1], 1), dtype = "uint8")), axis = 2
)
color_str = color_str.reshape(-1)
color = np.fromstring(color_str, dtype = "float32", count = size[0] * size[1])
color = color.reshape(size[0], size[1])
print color.shape
s = 0.3

x = cv2.medianBlur(x, 5)
y = cv2.medianBlur(y, 5)
z = cv2.medianBlur(z, 5)

x = cv2.resize(x, (0, 0), fx = s, fy = s)
y = cv2.resize(y, (0, 0), fx = s, fy = s)
z = cv2.resize(z, (0, 0), fx = s, fy = s)
color = cv2.resize(color, (0, 0), fx = s, fy = s, interpolation = cv2.INTER_NEAREST)



subp = cv2.merge([x, y, z, color])
P = pypcd.make_xyz_rgb_point_cloud(subp.reshape(-1, 4))
P.width = subp.shape[1]
P.height = subp.shape[0]

def print_limit(a):
    print np.min(a[~np.isnan(a)]), np.max(a[~np.isnan(a)])

#util.valueplot(x)
#util.valueplot(y)
#util.valueplot(z)
#util.show()
print "xlimit"
print_limit(x)
print "ylimit"
print_limit(y)
print "zlimit"
print_limit(z)
print P.width, P.height, P.points
print P.check_sanity()
pypcd.save_point_cloud_bin_compressed(P, sys.argv[2])
