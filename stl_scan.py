#!/usr/bin/env python
import sys
import os
import numpy as np
from optparse import OptionParser
import re
import time
import cv2
import concurrent.futures as futures

r50path = "/home/snje/repositories/robotsuite/branch/r50"
trunkpath = "/home/snje/repositories/robotsuite/trunk/"

sys.path.append(r50path + "/control")
sys.path.append(trunkpath + "/utilities")
sys.path.append(trunkpath + "/analysis")

import robotpath
import abb
import projector as projector_lib
import camera
import util
import structuredlight as stl
import camerautils
from subprocess import Popen, PIPE
import light as light_ctrl

parser = OptionParser()

parser.add_option(
  "-o", "--output", dest="output",
  help="Output file in which to store the point cloud and color map",
  metavar="FILE"
)
parser.add_option(
  "-c", "--calibration", dest="cali", help="Calibration directory",
  metavar="FILE"
)
parser.add_option(
  "-e", "--handeye", dest="handeye", help="Path to handeye calibration",
  metavar="FILE"
)
parser.add_option(
    "-d", "--debug", dest = "debug",
    help = "Directory to which debug images will be dumped", metavar = "DIR"
)

(options, args) = parser.parse_args()

if options.cali is None:
    print "please supply calibration path"
    sys.exit(1)

output = options.output or "./scan.npz"

def safe_makedir(path):
    if not os.path.exists(path):
        os.makedirs(path)

calipath = options.cali
lret, lA, ld, _, _ = util.loadsinglecalibration(calipath + "/left/")
rret, rA, rd, _, _ = util.loadsinglecalibration(calipath + "/right/")
sret, sR, sT, _, _ = util.loadstereocalibration(calipath + "/stereo/")

handeye = None
if options.handeye is not None:
    handeye = np.load(options.handeye)

# declare all hardware handles
lproc = None
rproc = None
lcam = None
rcam = None
proj = None
robot = None

def do_exit():
    if lcam is not None:
        lcam.close()
        time.sleep(1)
    if rcam is not None:
        rcam.close
        time.sleep(1)
    if lproc is not None:
        lproc.kill()
    if rproc is not None:
        rproc.kill()
    if robot is not None:
        robot.close()
    #sys.exit(0)

# Setup all hardware
try:
    robot = abb.Robot(ip='192.168.125.1')
    robot.set_speed(speed=[50, 10, 10, 10])
except:
    print "Robot initialization failed"
    do_exit()
    sys.exit(1)

# Setup camera
camera_left_serial  = 14194507
camera_right_serial = 14253982
gain = 0
shutter = 120

def setup_camera(logpath, serial, port, gain, shutter):
    log = open("/tmp/" + logpath, "w")
    camera_path = r50path + "/drivers/CameraDaemon/CameraDaemon"
    proc = Popen(
        [camera_path, str(port)], stdout=log, stderr=log
    )
    cam = camera.Camera(port=port, url="tcp://localhost")
    cam.connect(serial)
    cam.init()
    cam.set_gain(gain)
    cam.set_shuttertime(shutter)
    return proc, cam

try:
    lproc, lcam = setup_camera(
        "leftlog.txt", camera_left_serial, 1337, gain, shutter
    )
    rproc, rcam = setup_camera(
        "rightlog.txt", camera_right_serial, 1338, gain, shutter
    )
except:
    print "Camera init failed"
    do_exit()
    sys.exit(1)

proj = projector_lib.Projector(screen=1)
os.system("xrandr --output HDMI3 --gamma {0}:{0}:{0}".format(1.7))
#light.all_on()

period = 16
shift = 9
patterns = stl.phaseshiftsequence(1280, period, shift)

def bayer2color(im):
    return cv2.cvtColor(im, cv2.COLOR_BAYER_BG2BGR)

def bayer2gray(im):
    im = bayer2color(im)
    # White blaance
    #im[:, :, 0] *= 2
    #im[:, :, 2] *= 2
    im = im[:, :, 0] / 4 + im[:, :, 1] / 2 + im[:, :, 2] / 4
    return im

def write_disk(im3d, cmap, rmap):
    stl.im3dwrite(
        output, im3d, colorim = cmap, normalim = rmap
    )
    #stl.pcdwrite(
    #    output + "/pose{0}_view{1}.pcd".format(j, i), im3d, cmap
    #)

iopool = futures.ThreadPoolExecutor(max_workers = 1)

def do_reconstruction(seq, cmap, rmap, pose):
    seq.left = map(bayer2gray, seq.left)
    seq.right = map(bayer2gray, seq.right)
    im3d, cmap = stl.generate_im3d(
        stl.phaseshift, bayer2color(cmap), seq.left, seq.right, lA, ld, rA,
        rd, sR, sT
    )
    if handeye is not None:
        im3d = stl.im3d_to_world_space(im3d, pose, handeye)
    return iopool.submit(write_disk, im3d, cmap, rmap), im3d, cmap



threads = 3
pool = futures.ThreadPoolExecutor(max_workers = threads)

bpool = futures.ThreadPoolExecutor(max_workers = 1)
proj.black_screen()
seq = camerautils.shoot_stl(lcam, rcam, proj, patterns)
proj.white_screen()
cam_pos = robot.get_cartesian()
time.sleep(0.3)
cmap = lcam.single_shot()
rmap = rcam.single_shot()
#job = pool.submit(do_reconstruction, seq, cmap)
do_exit()

if options.debug:
    for i, im, in enumerate(seq.left):
        cv2.imwrite(options.debug + "/left_{0}.png".format(i), im)
    for i, im, in enumerate(seq.right):
        cv2.imwrite(options.debug + "/right_{0}.png".format(i), im)

io_job, im3d, cmap = do_reconstruction(seq, cmap, rmap, cam_pos)

#io_job, im3d, cmap = job.result()
util.valueplot(im3d[:, :, 2])
#util.valueplot(phase)
#util.imshow(cmap)
io_job.result()
util.show()
