#!/usr/bin/env python
import sys
import os
import numpy as np
from optparse import OptionParser
import re
import time
import cv2
import concurrent.futures as futures
from printrun.printcore import printcore


r50path = "/home/snje/repositories/robotsuite/branch/r50"
trunkpath = "/home/snje/repositories/robotsuite/trunk/"

sys.path.append(r50path + "/control")
sys.path.append(trunkpath + "/utilities")
sys.path.append(trunkpath + "/analysis")

import robotpath
import abb
import projector as projector_lib
import camera
import util
import structuredlight as stl
import camerautils
from subprocess import Popen, PIPE
import light as light_ctrl

parser = OptionParser()

parser.add_option(
  "-o", "--output", dest="output",
  help="Output file in which to store the point cloud and color map",
  metavar="FILE"
)
parser.add_option(
  "-c", "--calibration", dest="cali", help="Calibration directory",
  metavar="FILE"
)
parser.add_option(
    "-e", "--handeye", dest = "handeye", help = "Specifies handeye calibration",
    metavar = "FILE"
)

(options, args) = parser.parse_args()

if options.cali is None:
    print "please supply calibration path"
    sys.exit(1)

output = options.output or "./"

def safe_makedir(path):
    if not os.path.exists(path):
        os.makedirs(path)

safe_makedir(output)
calipath = options.cali
lret, lA, ld, _, _ = util.loadsinglecalibration(calipath + "/left/")
rret, rA, rd, _, _ = util.loadsinglecalibration(calipath + "/right/")
sret, sR, sT, _, _ = util.loadstereocalibration(calipath + "/stereo/")

pos = [
    [330,700,494],
    [850,600,494],
    [1200,0,420],
    [900.0, -600.0, 430.0],
    [330,-700,300],
    [450.0, -500.0, 900.0],
]
quat = [
    [0.016,-0.793,0.609,0.021],
    [0.0, -0.574, 0.819, 0.0],
    [0.044,0.044,-0.998,0.002],
    [0.037, -0.537, -0.843, -0.02],
    [0.072,-0.803,-0.583,-0.099],
    [0.683, 0.183, 0.683, 0.183],
]
adjust = [
    [0, 0, 0],
    [0, 0, 0],
    [0, 0, 0],
    [0, 0, 0],
    [0, 1.5, .15],
    [0, 0, 0],
]

poses = 20
prog = 0
start_time = time.time()
def update_progress():
    global prog
    dt = time.time() - start_time
    ft = dt * 100 / prog if prog != 0 else 0
    for i in xrange(int(prog) / 5 + 1):
        sys.stdout.write('\r')
        # the exact output you're looking for:
        sys.stdout.write("[%-20s] %d%%" % ('='*i, 5*i))
        if ft != 0:
            h = ft / 3600
            m = (ft - h * 3600) / 60
            s = (ft - h * 3600 - m * 60)
            sys.stdout.write(", ETA = %dh %dm %ds %d %d" % (h, m, s, dt, ft))
        sys.stdout.flush()
    prog += float(100.0) / len(pos) / poses

update_progress()
handeye = None
if options.handeye is not None:
    handeye = np.load(options.handeye)

# declare all hardware handles
robot = None
lproc = None
rproc = None
lcam = None
rcam = None
proj = None
pbend = None
light = None
bender = None

def do_exit():
    if robot is not None:
        robot.close()
    if lcam is not None:
        lcam.close()
        time.sleep(1)
    if rcam is not None:
        rcam.close
        time.sleep(1)
    if lproc is not None:
        lproc.kill()
    if rproc is not None:
        rproc.kill()
    if bender is not None:
        bender.disconnect()
    #sys.exit(0)

# Setup all hardware
# Connect to bender
# This is done first as it needs a bit of time to get ready

def pressure_step(t):
    # Turn on lvale
    light.set_switch4('+')
    time.sleep(t)
    light.set_switch4('-')
    # Turn off value
    time.sleep(3)

# Setup robot
try:
    robot = abb.Robot(ip='192.168.125.1')
    robot.set_speed(speed=[75, 75, 75, 75])
except:
    print "Robot initialization failed"
    do_exit()
    sys.exit(1)
# Setup camera
camera_left_serial  = 14194507
camera_right_serial = 14253982
gain = 0
shutter = 120

def setup_camera(logpath, serial, port, gain, shutter):
    log = open(output + "/" + logpath, "w")
    camera_path = r50path + "/drivers/CameraDaemon/CameraDaemon"
    proc = Popen(
        [camera_path, str(port)], stdout=log, stderr=log
    )
    cam = camera.Camera(port=port, url="tcp://localhost")
    cam.connect(serial)
    cam.init()
    cam.set_gain(0)
    cam.set_shuttertime(shutter)
    return proc, cam

try:
    lproc, lcam = setup_camera(
        "leftlog.txt", camera_left_serial, 1337, gain, shutter
    )
    rproc, rcam = setup_camera(
        "rightlog.txt", camera_right_serial, 1338, gain, shutter
    )
except:
    print "Camera init failed"
    do_exit()
    sys.exit(1)

proj = projector_lib.Projector(screen=1)
light = light_ctrl.Light()
#light.all_on()
os.system("xrandr --output HDMI3 --gamma {0}:{0}:{0}".format(1.7))
period = 16
shift = 9
patterns = stl.phaseshiftsequence(1280, period, shift)

def bayer2color(im):
    return cv2.cvtColor(im, cv2.COLOR_BAYER_BG2BGR)

def bayer2gray(im):
    im = bayer2color(im)
    # White blaance
    #im[:, :, 0] *= 2
    #im[:, :, 2] *= 2
    im = im[:, :, 0] / 4 + im[:, :, 1] / 2 + im[:, :, 2] / 4
    return im

def write_disk(im3d, cmap, j, i):
    stl.im3dwrite(
        output + "/pose{0}_view{1}.npz".format(j, i), im3d, colorim = cmap
    )
    update_progress()
    #stl.pcdwrite(
    #    output + "/pose{0}_view{1}.pcd".format(j, i), im3d, cmap
    #)

iopool = futures.ThreadPoolExecutor(max_workers = 1)

def do_reconstruction(seq, cmap, pose, adjust, j, i):
    seq.left = map(bayer2gray, seq.left)
    seq.right = map(bayer2gray, seq.right)
    im3d, cmap = stl.generate_im3d(
        stl.phaseshift, bayer2color(cmap), seq.left, seq.right, lA, ld, rA,
        rd, sR, sT
    )
    if handeye is not None:
        im3d = stl.im3d_to_world_space(im3d, pose, handeye)
        im3d += np.array(adjust)
    return iopool.submit(write_disk, im3d, cmap, j, i), im3d, cmap



threads = 3
pool = futures.ThreadPoolExecutor(max_workers = threads)

bpool = futures.ThreadPoolExecutor(max_workers = 1)


views = zip(pos, quat)
path = np.zeros((poses, len(views), 7))
recon_job = []
for j in xrange(poses):
    step = bpool.submit(pressure_step, 15.0)
    robot.set_cartesian([pos[2], quat[2]])
    #bend.result()
    for i, (p, q) in enumerate(views):
        robot.set_cartesian([p, q])
        cam_pos = robot.get_cartesian()
        path[j, i, :] = np.array(cam_pos[0] + cam_pos[1])
        step.result()
        light.lights_off()
        seq = camerautils.shoot_stl(lcam, rcam, proj, patterns)
        light.lights_on()
        proj.white_screen()
        time.sleep(0.3)
        cmap = lcam.single_shot()
        job = pool.submit(do_reconstruction, seq, cmap, cam_pos, adjust[i / 2], j, i)
        recon_job.append(job)

    #views.reverse()
    #adjust.reverse()

np.save(output + "/path.npy", path)

light.lights_on()

do_exit()

# Wait for processing to finish
for job in recon_job:
    io_job, im3d, cmap = job.result()
    #util.valueplot(im3d[:, :, 2])
    #util.imshow(cmap)
    io_job.result()
#util.show()
print ""
