#!/bin/bash

getopt --test > /dev/null
if [[ $? != 4 ]]; then
    echo "I’m sorry, `getopt --test` failed in this environment."
    exit 1
fi

program=$0
script="$(dirname $0)/psi_reconstruction.py"

SHORT=hc:e:v:p: #dfo:v
LONG=help,calibration:,periods:,views:,poses: #debug,force,output:,verbose

# -temporarily store output to be able to check for errors
# -activate advanced mode getopt quoting e.g. via “--options”
# -pass arguments only via   -- "$@"   to separate them correctly
PARSED=`getopt --options $SHORT --longoptions $LONG --name "$0" -- "$@"`
if [[ $? != 0 ]]; then
    # e.g. $? == 1
    #  then getopt has complained about wrong arguments to stdout
    exit 2
fi
# use eval with "$PARSED" to properly handle the quoting
eval set -- "$PARSED"

function show_help {
  #echo "Program which scans a folder structure and reconstrucs using PSI algorithm"
  printf "Usage: $(basename $1) [options] [args]\n"
  printf "\nOptions:\n"
  printf "\t -h, --help \t show help message and exit\n"
  printf "\t -c, --calibration \t specify path to calibration data\n"
  printf "\t -v, --views \t specify number of sensor poses\n"
  printf "\t -p, --poses \t specify number of scene poses\n"
  printf "\t -e, --periods \t specify number fine-grained phase periods\n"
}

# now enjoy the options in order and nicely split until we see --
while true; do
    case "$1" in
        -h|--help)
            show_help $program
            exit 0
            ;;
        -c|--calibration)
            calibration="$2"
            shift 2
            ;;
        -v|--views)
            views="$2"
            shift 2
            ;;
        -p|--poses)
            poses="$2"
            shift 2
            ;;
        -e|--periods)
            periods="$2"
            shift 2
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
    esac
done

# handle non-option arguments
#echo $@
#echo $calibration
#echo $views
#echo $poses
#echo $periods


test "$calibration" == '' && (echo calibration must be supplied; exit 1)
test "$views" == '' && (echo sensor pose count must be supplied; exit 1)
test "$poses" == '' && (echo scene pose count must be supplied; exit 1)
#test "$periods" == '' && (echo scene pose count must be supplied; exit 1)
#parallel
function _do_reconstruct() {
  left_ims=$(ls -1vd $1/l_v$2_p$3_i*.png)
  right_ims=$(ls -1vd $1/r_v$2_p$3_i*.png)
  output="$1/v$2_p$3.npz"
  $script -o $output -c $calibration -p 16 $left_ims $right_ims
}
export -f _do_reconstruct
export script
export calibration

view_seq=$(seq -f "%03g" 1 $views)
pose_seq=$(seq -f "%03g" 1 $poses)
parallel _do_reconstruct ::: $@ ::: $view_seq ::: $pose_seq
